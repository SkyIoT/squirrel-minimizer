local Build = {};
Build.dynamicAsset <- true;
Build.frenchMeter <- false;
Build.meteringSegment <- true;
Build.modbusCommon <- true;
Build.modbusSerial <- false;
Build.modbusTcp <- true;
Build.modbusTcpBatteryMapped <- true;
Build.modbusTcpByd <- true;
Build.modbusTcpConnectedEnergy <- true;
Build.modbusTcpNec <- true;
Build.modbusTcpPowerOn <- false;
Build.modbusTcpTesla <- true;

    // Electric Imp Libraries
// Copyright (c) 2015 Electric Imp
// This file is licensed under the MIT License
// http://opensource.org/licenses/MIT

class Serializer {
    static version = [1,0,0];

    // Serialize a variable of any type into a blob
    static function serialize (obj, prefix = null) {
        // Take a guess at the initial size
        local b = blob(512);
        local header_len = 3;
        local prefix_len = (prefix == null) ? 0 : prefix.len();

        // Write the prefix plus dummy data for length and CRC
        if (prefix_len > 0) {
            foreach (ch in prefix) b.writen(ch, 'b');
        }

        // Write two bytes for length
        b.writen(0, 'w');   // 0x0000
        // Write 1 byte for CRC
        b.writen(0, 'b');   // 0x00

        // Serialise the object
        _serialize(b, obj);

        // Resize the blog as required
        local body_len = b.tell();
        b.resize(body_len);

        // Remove header and prefix from length of body
        body_len -= (header_len + prefix_len);

        // Write the body length and CRC
        b.seek(prefix_len);
        b.writen(body_len, 'w');
        b.writen(LRC8(b, header_len + prefix_len), 'b');

        // Move pointer to start of blob,
        // and return serialized object
        b.seek(0);
        return b;
    }

    // Deserialize a string into a variable
    static function deserialize (s, prefix = null) {
        // Read and check the prefix and header
        local prefix_len = (prefix == null) ? 0 : prefix.len();
        local header_len = 3;
        s.seek(0);

        local pfx = prefix_len > 0 ? s.readblob(prefix_len) : null;
        local len = s.readn('w');
        local crc = s.readn('b');

        if (s.len() != len+prefix_len+header_len) throw "Expected " + (len+prefix_len+header_len) + " bytes (got " + s.len() + " bytes)";
        // Check the prefix
        if (prefix != null && pfx.tostring() != prefix.tostring()) throw "Prefix mismatch";
        // Check the CRC
        local _crc = LRC8(s, prefix_len+header_len);
        if (crc != _crc) throw format("CRC err: 0x%02x != 0x%02x", crc, _crc);
        // Deserialise the rest
        return _deserialize(s, prefix_len+header_len).val;
    }

    static function sizeof(obj, prefix = null) {
        local size = 3; // header
        if (prefix != null) size += prefix.len();
        return size += _sizeof(obj);
    }

    static function _sizeof(obj) {
        switch (typeof obj) {
            case "integer":
                return format("%d", obj).len() + 3
            case "float":
                return 7;
            case "null":
            case "function": // Silently setting this to null
                return 1;
            case "bool":
                return 2;
            case "blob":
            case "string":
                return obj.len()+3;
            case "table":
            case "array":
                local size = 3;
                foreach ( k,v in obj ) {
                    size += _sizeof(k) + _sizeof(v)
                }
                return size;
            default:
                throw ("Can't serialize " + typeof obj);
        }
    }

    // Calculates an 8-bit CRC
    static function LRC8 (data, offset = 0) {
        local LRC = 0x00;
        for (local i = offset; i < data.len(); i++) {
            LRC = (LRC + data[i]) & 0xFF;
        }
        return ((LRC ^ 0xFF) + 1) & 0xFF;
    }

    static function _serialize (b, obj) {
        switch (typeof obj) {
            case "integer":
                return _write(b, 'i', format("%d", obj));
            case "float":
                // 'F' is for new floats, 'f' is for legacy floats
                local bl = blob(4);
                bl.writen(obj, 'f');
                return _write(b, 'F', bl);
            case "null":
            case "function": // Silently setting this to null
                return _write(b, 'n');
            case "bool":
                return _write(b, 'b', obj ? "\x01" : "\x00");
            case "blob":
                return _write(b, 'B', obj);
            case "string":
                return _write(b, 's', obj);
            case "table":
            case "array":
                local t = (typeof obj == "table") ? 't' : 'a';
                _write(b, t, obj.len());
                foreach ( k,v in obj ) {
                    _serialize(b, k);
                    _serialize(b, v);
                }
                return;
            default:
                throw ("Can't serialize " + typeof obj);
        }
    }

    static function _write(b, type, payload = null) {
        // Calculate the lengths
        local prefix_length = true;
        local payloadlen = 0;
        switch (type) {
            case 'n':
            case 'b':
                prefix_length = false;
                break;
            case 'a':
            case 't':
                payloadlen = payload;
                break;
            default:
                payloadlen = payload.len();
        }

        // Update the blob
        b.writen(type, 'b');
        if (prefix_length) {
            b.writen(payloadlen >> 8 & 0xFF, 'b');
            b.writen(payloadlen & 0xFF, 'b');
        }
        if (typeof payload == "string" || typeof payload == "blob") {
            foreach (ch in payload) {
                b.writen(ch, 'b');
            }
        }
    }

    static function _deserialize (s, p = 0) {
        for (local i = p; i < s.len(); i++) {
            local t = s[i];

            switch (t) {
                case 'n': // Null
                    return { val = null, len = 1 };
                case 'i': // Integer
                    local len = s[i+1] << 8 | s[i+2];
                    s.seek(i+3);
                    local val = s.readblob(len).tostring().tointeger();
                    return { val = val, len = 3+len };
                case 'F': // New Floats
                    local len = s[i+1] << 8 | s[i+2];
                    s.seek(i+3);
                    local val = s.readblob(len).readn('f');
                    return { val = val, len = 3+len };
                case 'f': // Legacy Float deserialization :(
                    local len = s[i+1] << 8 | s[i+2];
                    s.seek(i+3);
                    local val = s.readblob(len).tostring().tofloat();
                    return { val = val, len = 3+len };
                case 'b': // Bool
                    local val = s[i+1];
                    return { val = (val == 1), len = 2 };
                case 'B': // Blob
                    local len = s[i+1] << 8 | s[i+2];
                    local val = blob(len);
                    for (local j = 0; j < len; j++) {
                        val[j] = s[i+3+j];
                    }
                    return { val = val, len = 3+len };
                case 's': // String
                    local len = s[i+1] << 8 | s[i+2];
                    local val = "";
                    s.seek(i+3);
                    if (len > 0) {
                        val = s.readblob(len).tostring();
                    }
                    return { val = val, len = 3+len };
                case 't': // Table
                case 'a': // Array
                    local len = 0;
                    local nodes = s[i+1] << 8 | s[i+2];
                    i += 3;
                    local tab = null;

                    if (t == 'a') tab = [];
                    if (t == 't') tab = {};

                    for (local node = 0; node < nodes; node++) {

                        local k = _deserialize(s, i);
                        i += k.len;
                        len += k.len;

                        local v = _deserialize(s, i);
                        i += v.len;
                        len += v.len;

                        if (typeof tab == "array") tab.push(v.val);
                        else tab[k.val] <- v.val;
                    }
                    return { val = tab, len = len+3 };
                default:
                    throw format("Unknown type: 0x%02x at %d", t, i);
            }
        }
    }
}
// MIT License
//
// Copyright 2017 Electric Imp
//
// SPDX-License-Identifier: MIT
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
// EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES
// OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
// ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.

// Using `const`s instead of `static`s for performance
const SPIFLASHLOGGER_SECTOR_SIZE = 4096;        // Size of sectors
const SPIFLASHLOGGER_SECTOR_METADATA_SIZE = 6;      // Size of metadata at start of sectors
const SPIFLASHLOGGER_SECTOR_BODY_SIZE = 4090;   // Size of writeable memory / sector
const SPIFLASHLOGGER_CHUNK_SIZE = 256;          // Number of bytes we write / operation

const SPIFLASHLOGGER_OBJECT_MARKER = "\x00\xAA\xCC\x55";
const SPIFLASHLOGGER_OBJECT_MARKER_SIZE = 4;

const SPIFLASHLOGGER_OBJECT_HDR_SIZE = 7;       // SPIFLASHLOGGER_OBJECT_MARKER (4 bytes) + size (2 bytes) + crc (1 byte)
const SPIFLASHLOGGER_OBJECT_MIN_SIZE = 6;       // SPIFLASHLOGGER_OBJECT_MARKER (4 bytes) + size (2 bytes)

const SPIFLASHLOGGER_SECTOR_DIRTY = 0x00;       // Flag for dirty sectors
const SPIFLASHLOGGER_SECTOR_CLEAN = 0xFF;       // Flag for clean sectors


// The SPIFlashLogger creates a circular log system,
// allowing you to log any serializable object
// (table, array, string, blob, integer, float, boolean and null) to the SPIFlash.
// If the log system runs out of space in the SPIFlash,
// it begins overwriting the oldest logs.
class SPIFlashLogger {

    static VERSION = "2.2.0";

    _flash = null;      // hardware.spiflash or an object with an equivalent interface
    _serializer = null; // github.com/electricimp/serializer (or an object with an equivalent interface)

    _size = null;       // The size of the spiflash
    _start = null;      // First block to use for logging
    _end = null;        // Last block to use for logging
    _len = null;        // The length of the flash available (end-start)
    _sectors = 0;       // The number of sectors in _len
    _maxData = 0;      // The maximum data we can push at once

    _atSec = 0;        // Current sector we're writing to
    _atPos = 0;        // Current position we're writing to in the sector

    _map = null;        // Array of sector maps
    _enables = 0;       // Counting semaphore for _enable/_disable
    _nextSectorId = 1;   // The next sector we should write to

    // ------------------------ public API -------------------

    //
    // Constructor
    // Parameters:
    //      start        - spi flash start address for the logger
    //      end          - spi flash end address for the logger
    //      flash        - spi flash object see `hardware.spiflash`
    //                     (https://electricimp.com/docs/hardware/spiflash)
    //      serializer   - the serializer instance which is responsible for object
    //                     serialization and de-serialization
    //                     (for example: https://github.com/electricimp/Serializer)
    //
    constructor(start = null, end = null, flash = null, serializer = null) {
        // Set the SPIFlash, or try and set with hardware.spiflash
        try { _flash = flash ? flash : hardware.spiflash; }
        catch (e) { throw "Missing requirement (hardware.spiflash). For more information see: https://github.com/electricimp/spiflashlogger"; }

        // Set the serizlier, or try and set with Serializer
        try { _serializer = serializer ? serializer : Serializer; }
        catch (e) { throw "Missing requirement (Serializer). For more information see: https://github.com/electricimp/spiflashlogger"; }

        // Get the size of the flash
        _enable();
        _size = _flash.size();
        _disable();

        // Set the start/end values
        _start = (start != null) ? start : 0;
        _end = (end != null) ? end : _size;

        // Validate the start/end values
        if (_start >= _size) throw "Invalid start parameter (start must be < size of SPI flash";
        if (_end <= _start) throw "Invalid end parameter (end must be > start)";
        if (_end > _size) throw "Invalid end parameter (end must be <= size of SPI flash)";
        if (_start % SPIFLASHLOGGER_SECTOR_SIZE != 0) throw "Invalid start parameter (start must be at a sector boundary)";
        if (_end % SPIFLASHLOGGER_SECTOR_SIZE != 0) throw "Invalid end parameter (end must be at a sector boundary)";

        // Set the other utility properties
        _len = _end - _start;
        _sectors = _len / SPIFLASHLOGGER_SECTOR_SIZE;
        _maxData = _sectors * SPIFLASHLOGGER_SECTOR_BODY_SIZE;

        // Can compress this by eight by using bits instead of bytes
        _map = blob(_sectors);

        // Initialise the values by reading the metadata
        _init();
    }

    // This method returns a table with the following keys,
    // each of which gives access to an integer value:
    //   size    - The size of the SPIFlash in bytes
    //   len     - The number of bytes allocated to the logger
    //   start   - The first byte used by the logger
    //   end     - The last byte used by the logger
    //   sectors - The number of sectors allocated to the logger
    //   sectorSize - The size of sectors in bytes
    function dimensions() {
        return { "size": _size, "len": _len, "start": _start, "end": _end, "sectors": _sectors, "sector_size": SPIFLASHLOGGER_SECTOR_SIZE }
    }

    // Writes any serializable object to the memory allocated
    // for the SPIFlashLogger. If the memory is full,
    // the logger begins overwriting the oldest entries.
    // If the provided object can not be serialized,
    // the exception is thrown by the underlying serializer class.
    //
    // Parameters:
    //    object - an object to serialize and save
    function write(object) {
        // Check of the object will fit
        local objLength = _serializer.sizeof(object, SPIFLASHLOGGER_OBJECT_MARKER);
        if (objLength > _maxData) throw "Cannot store objects larger than alloted memory."

        // Serialize the object
        local obj = _serializer.serialize(object, SPIFLASHLOGGER_OBJECT_MARKER);

        _enable();

        // Write one sector at a time with the metadata attached
        local objPos = 0;
        local objRemaining = objLength;
        do {

            // How far are we from the end of the sector
            if (_atPos < SPIFLASHLOGGER_SECTOR_METADATA_SIZE) _atPos = SPIFLASHLOGGER_SECTOR_METADATA_SIZE;
            local secRemaining = SPIFLASHLOGGER_SECTOR_SIZE - _atPos;

            // We are too close to the end of the sector, skip to the next sector
            if (objPos == 0 && secRemaining < SPIFLASHLOGGER_OBJECT_MIN_SIZE) {
                secRemaining = SPIFLASHLOGGER_SECTOR_BODY_SIZE;
                _atSec = (_atSec + 1) % _sectors;
                _atPos = SPIFLASHLOGGER_SECTOR_METADATA_SIZE;
            }
            // Handle overflow use-case for a one-sector logger and multiple sectors logger
            // This check intended to prevent start code overwriting
            if ((_sectors == 1 && objPos == 0 && objRemaining > secRemaining)
                || (_sectors > 1
                    && _atPos > SPIFLASHLOGGER_SECTOR_METADATA_SIZE
                    && objRemaining - secRemaining > (_sectors - 1) * SPIFLASHLOGGER_SECTOR_BODY_SIZE)) {
                eraseAll(true);
                _atPos = SPIFLASHLOGGER_SECTOR_METADATA_SIZE;
                secRemaining = SPIFLASHLOGGER_SECTOR_BODY_SIZE;
            }

            // for a one sector only
            if (objRemaining < secRemaining) secRemaining = objRemaining;

            // Now write the data
            _write(obj, _atSec, _atPos, objPos, secRemaining);
            _map[_atSec] = SPIFLASHLOGGER_SECTOR_DIRTY;

            // Update the positions
            objPos += secRemaining;
            objRemaining -= secRemaining;
            _atPos += secRemaining;
            if (_atPos >= SPIFLASHLOGGER_SECTOR_SIZE) {
                _atSec = (_atSec + 1) % _sectors;
                _atPos = SPIFLASHLOGGER_SECTOR_METADATA_SIZE;
            }
        } while (objRemaining > 0);

        _disable();
    }

    // Reads objects from the logger synchronously,
    // returning a single log object for the specified *index*.
    //
    // Returns:
    // Deserialized object or null
    //    - the most recent object when `index == -1`,
    //    - the oldest object when `index == 1`,
    //    - *null* when the value of *index* is greater than the number of logs,
    //    - throws an exception when `index == 0`.
    //
    function readSync(index) {
        // Unexpected index value
        if (index == 0)
            throw "Invalid argument";

        // identify first sector to read
        local seekTo = math.abs(index);
        local count = 0;
        local i = 0;

        // _atSec - indicates the current write sector, therefore
        // for the index > 0 it is necessary to step forward (skip current sector)
        // to find first not empty sector
        // For the index < 0, it is possible to start couting from the current sector
        while (i < _sectors) {
            // convert sector index `i`, ordered by recency, to physical `sector`, ordered by position on disk
            local sector;
            if (index > 0) {
                sector = (_atSec + i + 1) % _sectors;
            } else {
                sector = (_atSec - i + _sectors) % _sectors;
            }

            ++i;

            // Read all objects start codes for current sector
            // and write them into the blob
            local objectsStartCodes = _getObjectsStartCodesForSector(sector);

            // If there is no start codes in the current sector
            // then switch to the next sector
            if (objectsStartCodes == null || objectsStartCodes.len() == 0)
                continue;

            // negative step, go backwards
            if (index < 0)
                objectsStartCodes.seek(-2, 'e');

            // Check that the number of start codes is enough
            // otherwise decrease the seekTo count on a number of objects
            // in the current sector and switch to the next sector
            if (seekTo > objectsStartCodes.len() / 2) {
              seekTo -= objectsStartCodes.len() / 2;
              continue;
            }

            // seek for an object start code
            if (objectsStartCodes.seek((seekTo - 1) * 2 * (index > 0 ? 1 : -1), 'c') == -1 || objectsStartCodes.eos() == 1) {
                // This code should never happen, because
                // the objectsStartCodes blob should have enough data
                // for seek to the seekTo position (check at the previous if-condition)
                server.error("Unexpected error");
                return null;
            }
            // Read the object address
            local addr = objectsStartCodes.readn('w');
            // Get the global object address on the spiflash
            local spiAddr = _start + sector * SPIFLASHLOGGER_SECTOR_SIZE + SPIFLASHLOGGER_SECTOR_METADATA_SIZE + addr;
            // Read the object by address and return the de-serialized value
            // return null in case of de-serialize object errors
            return _getObject(spiAddr);
        } // while

        return null;
    }

    // Synchronously returns the first object written to the log
    // that hasn't been erased (i.e. the oldest entry on flash).
    // If there are no logs on the flash, returns default.
    function first(defaultVal = null) {
        local data = this.readSync(1);
        return data == null ? defaultVal : data
    }

    // Synchronously returns the last object written to the log
    // that hasn't been erased (i.e. the newest entry on flash).
    // If there are no logs on the flash, returns default.
    function last(defaultVal = null) {
        local data = this.readSync(-1);
        return data == null ? defaultVal : data
    }

    // Reads objects from the logger asynchronously.
    //
    // This mehanism is intended for the asynchronous processing of each log object,
    // such as sending data to the agent and waiting for an acknowledgement.
    //
    // Parameters:
    //
    // onData   - Callback that provides the object which has been read
    //            from the logger
    //
    // onFinish - Callback that is called after the last object is provided
    //            (i.e. there are no more objects to return by the current read operation),
    //            or when the operation it terminated,
    //            or in case of an error The callback has no parameters.
    //
    // step     - The rate at which the read operation steps through the logged
    //            objects. Must not be 0. If it has a positive value the read
    //            operation starts from the oldest logged object.
    //            If it has a negative value, the read operation starts from the
    //            most recently written object and steps backwards. Defaule value: 1
    //
    // skip     - Skips the specified number of the logged objects at the start of
    //            the reading. Must not has a negative value. Default value: 0
    //
    function read(onData = null, onFinish = null, step = 1, skip = 0) {
        assert(typeof step == "integer" && step != 0);

        // skipped tracks how many entries we have skipped, in order to implement skip
        local skipped = math.abs(step) - skip - 1;
        local count = 0;

        // function to read one sector, optionally continuing to the next one
        local readSector;
        local objectsStartCodes = null;
        readSector = function(i) {

            if (i >= _sectors){
                if (onFinish != null) {
                    return onFinish()
                }
                return;
            };

            // convert sector index `i`, ordered by recency, to physical `sector`, ordered by position on disk
            local sector;
            if (step > 0) {
                sector = (_atSec + i + 1) % _sectors;
            } else {
                sector = (_atSec - i + _sectors) % _sectors;
            }

            objectsStartCodes = _getObjectsStartCodesForSector(sector);

            if (objectsStartCodes.len() == 0) {
                return imp.wakeup(0, function() {
                    readSector(i + 1);
                }.bindenv(this))
            };

            if (step < 0) {
                // negative step, go backwards
                // `skip` will take care of the magnitude of the steps
                objectsStartCodes.seek(-2, 'e');
            }


            local addr, spiAddr, obj, readObj, cont, seekTo;

            // Passed in to the read callback to be called as `next`
            cont = function(keepGoing = true) {
                if (keepGoing == false) {
                    // Clean up and exit
                    objectsStartCodes = obj = null;
                    if (onFinish != null) onFinish();
                } else if ((objectsStartCodes.seek(seekTo, 'c') == -1 || objectsStartCodes.eos() == 1)) {
                    //  ^ Try to seek to the next available object
                    // If we've exhausted all addresses found in this sector, move on to the next
                    return imp.wakeup(0, function() {
                        readSector(i + 1);
                    }.bindenv(this));
                } else {
                    // There are more objects to read, read the next one
                    return imp.wakeup(0, readObj.bindenv(this));
                }
            };

            readObj =  function() {

                if (++skipped == math.abs(step)) {
                    // We are not skipping this object, reset `skipped` count
                    skipped = 0;
                    // Get the address (offset from the end of this sectors meta)
                    addr = objectsStartCodes.readn('w');
                    // Calculate the raw spiflash address
                    spiAddr = _start + sector * SPIFLASHLOGGER_SECTOR_SIZE + SPIFLASHLOGGER_SECTOR_METADATA_SIZE + addr;
                    // Read the object
                    obj = _getObject(spiAddr);

                    // If we're moving backwards, do so, otherwise our blob cursor is
                    // already moved forward by reading
                    if (step < 0) seekTo = -4;
                    else seekTo = 0

                    return onData(obj, spiAddr, cont.bindenv(this));

                } else {
                    // We need to skip more
                    if (step < 0) seekTo = -2;
                    else seekTo = 2

                    // continue
                    return cont();

                }

            }.bindenv(this);

            imp.wakeup(0, readObj.bindenv(this));  // start reading objects in this sector

        }.bindenv(this)

        imp.wakeup(0, function() {
            readSector(0); // start reading sectors
        });
    }

    // Erases all dirty sectors, or an individual object
    // This method erases an object at SPIFlash address address by marking it erased.
    // If address is not specified, it behaves as eraseAll() method with the default parameter.
    function erase(addr = null) {
        if (addr == null) return eraseAll();
        else return _eraseObject(addr);
    }

    // Erases the entire allocated SPIFlash area.
    // The optional force parameter is a Boolean value which defaults to false,
    // a value which will cause the method to erase only the sectors
    // written to by this library. You must pass in true
    // if you wish to erase the entire allocated SPIFlash area.
    // Parametes:
    //    force - force all sectors erase, otherwise only dirty
    function eraseAll(force = false) {
        for (local sector = 0; sector < _sectors; sector++) {
            if (force || _map[sector] == SPIFLASHLOGGER_SECTOR_DIRTY) {
                _erase(sector);
            }
        }
    }

    //
    // This method returns the current SPI flash pointer,
    // ie. where the SPIFlashLogger will perform the next write operation.
    // This information can be used along with the setPosition()
    // method to optimize SPIFlash memory usage between deep sleeps.
    //
    function getPosition() {
        // Return the current pointer (sector + offset)
        return _atSec * SPIFLASHLOGGER_SECTOR_SIZE + _atPos;
    }

    //
    // This method sets the current SPI flash pointer,
    // ie. where the SPIFlashLogger will perform the next read/write operation.
    // Setting the pointer can help optimize SPI flash memory usage
    // between deep sleeps, as it allows the SPIFlashLogger to be precise
    // to one byte rather 256 bytes (the size of a chunk).
    function setPosition(position) {
        // Grab the sector and offset from the position
        local sector = position / SPIFLASHLOGGER_SECTOR_SIZE;
        local offset = position % SPIFLASHLOGGER_SECTOR_SIZE;

        // Validate sector and position
        if (sector < 0 || sector >= _sectors) throw "Position out of range";

        // Set the current sector and position
        _atSec = sector;
        _atPos = offset;
    }

    //
    // Enable flash lock
    //
    function _enable() {
        // Check _enables then increment
        if (_enables == 0) {
            _flash.enable();
        }

        _enables += 1;
    }

    //
    // Disable flash lock
    //
    function _disable() {
        // Decrement _enables then check
        _enables -= 1;

        if (_enables == 0)  {
            _flash.disable();
        }
    }

    //
    // Increase sector id and check id overflow
    //
    function _getNextSectorId() {
        if (_nextSectorId <= 0)
            _nextSectorId = 1;
        return _nextSectorId++;
    }

    //
    // Gets the logged object at the specified position
    //
    function _getObject(pos, cb = null) {
        local requested_pos = pos;

        _enable();
        // Get the meta (for checking) and the object length (to know how much to read)
        local marker = _flash.read(pos, SPIFLASHLOGGER_OBJECT_MARKER_SIZE).tostring();
        local len = _flash.read(pos + SPIFLASHLOGGER_OBJECT_MARKER_SIZE, 2).readn('w');
        _disable();

        if (marker != SPIFLASHLOGGER_OBJECT_MARKER) {
            throw "Error, marker not found at " + pos;
        }

        local serialised = blob(SPIFLASHLOGGER_OBJECT_HDR_SIZE + len);

        local leftInObject;
        _enable();
        // while there is more object left, read as much as we can from each sector into `serialised`
        while (leftInObject = serialised.len() - serialised.tell()) {
            // Decide what to read
            local sectorStart = pos - (pos % SPIFLASHLOGGER_SECTOR_SIZE);
            local sectorEnd = sectorStart + SPIFLASHLOGGER_SECTOR_SIZE;// MINUS ONE?
            local leftInSector = sectorEnd - pos;

            // Read it
            local read;
            if (leftInObject < leftInSector) {
                read = _flash.read(pos, leftInObject);
                assert(read.len() == leftInObject);
            } else {
                read = _flash.read(pos, leftInSector);
                assert(read.len() == leftInSector);
            }

            serialised.writeblob(read);

            // Update remaining and position
            leftInObject -= read.len();

            pos += read.len();
            assert (pos <= sectorEnd);

            if (pos == _end) pos = _start + SPIFLASHLOGGER_SECTOR_METADATA_SIZE;
            else if (pos == sectorEnd) pos += SPIFLASHLOGGER_SECTOR_METADATA_SIZE;

        }
        _disable();

        // Try to deserialize the object
        local obj;
        try {
            obj = _serializer.deserialize(serialised, SPIFLASHLOGGER_OBJECT_MARKER);
        } catch (e) {
            server.error(format("Exception reading logger object address 0x%04x with length %d: %s", requested_pos, serialised.len(), e));
            obj = null;
        }

        if (cb) cb(obj);
        else return obj;
    }

    // Returns a blob of 16 bit address of starts of objects,
    // relative to sector body start
    //
    function _getObjectsStartCodesForSector(sector_idx) {
        local from = 0,        // index to search form
              addrs = blob(),  // addresses of starts of objects
              found;

        // Sector clean
        if (_map[sector_idx] != SPIFLASHLOGGER_SECTOR_DIRTY) return addrs;

        local dataStart = _start + sector_idx * SPIFLASHLOGGER_SECTOR_SIZE + SPIFLASHLOGGER_SECTOR_METADATA_SIZE;
        local readLength = SPIFLASHLOGGER_SECTOR_BODY_SIZE;
        _enable();
        local sectorData = _flash.read(dataStart, readLength).tostring();
        _disable();
        if (sectorData == null) return addrs;

        while ((found = sectorData.find(SPIFLASHLOGGER_OBJECT_MARKER, from)) != null) {
            // Found an object start, save the address
            addrs.writen(found, 'w');
            // Skip the one we just found the next time around
            from = found + 1;
        }

        addrs.seek(0);
        return addrs;
    }

    //
    // Write full object or object part
    //
    function _write(object, sector, pos, objectPos = 0, len = null) {
        if (len == null) len = object.len();

        // Prepare the new metadata
        local meta = blob(6);

        // Erase the sector(s) if it is dirty but not if we are appending
        local appending = (pos > SPIFLASHLOGGER_SECTOR_METADATA_SIZE);
        if (_map[sector] == SPIFLASHLOGGER_SECTOR_DIRTY && !appending) {
            // Prepare the next sector
            _erase(sector, sector+1, true);
            // Write a new sector id
            meta.writen(_getNextSectorId(), 'i');
        } else {
            // Make sure we have a valid sector id
            if (_getSectorMetadata(sector).id > 0) {
                meta.writen(0xFFFFFFFF, 'i');
            } else {
                meta.writen(_getNextSectorId(), 'i');
            }
        }

        // Write the new usage map, changing only the bit in this write
        local chunkMap = 0xFFFF;
        local bitStart = math.floor(1.0 * pos / SPIFLASHLOGGER_CHUNK_SIZE).tointeger();
        local bitFinish = math.ceil(1.0 * (pos+len) / SPIFLASHLOGGER_CHUNK_SIZE).tointeger();
        for (local bit = bitStart; bit < bitFinish; bit++) {
            local mod = 1 << bit;
            chunkMap = chunkMap ^ mod;
        }
        meta.writen(chunkMap, 'w');

        // Write the metadata and the data
        local start = _start + (sector * SPIFLASHLOGGER_SECTOR_SIZE);
        _enable();
        _flash.write(start, meta, SPIFLASH_POSTVERIFY);
        local res = _flash.write(start + pos, object, SPIFLASH_POSTVERIFY, objectPos, objectPos + len);
        _disable();

        if (res != 0) {
            throw format("Writing failed from object position %d of %d, to 0x%06x (meta), 0x%06x (body)", objectPos, len, start, start + pos)
            return null;
        } else {
            // server.log(format("Written to: 0x%06x (meta), 0x%06x (body) of: %d", start, start + pos, objectPos));
        }

        return len;
    }

    // Erases the marker to make an object invisible
    function _eraseObject(addr) {

        if (addr == null) return false;

        // Erase the marker for the entry we found
        _enable();
        local check = _flash.read(addr, SPIFLASHLOGGER_OBJECT_MARKER_SIZE);
        if (check.tostring() != SPIFLASHLOGGER_OBJECT_MARKER) {
            server.error("Object address invalid. No marker found.")
            _disable();
            return false;
        }
        local clear = blob(SPIFLASHLOGGER_OBJECT_MARKER_SIZE);
        local res = _flash.write(addr, clear, SPIFLASH_POSTVERIFY);
        _disable();

        if (res != 0) {
            server.error("Clearing object marker failed.");
            return false;
        }

        return true;

    }

    //
    // Read sector metadata
    //
    function _getSectorMetadata(sector) {
        // NOTE: Should we skip clean sectors automatically?
        _enable();
        local start = _start + (sector * SPIFLASHLOGGER_SECTOR_SIZE);
        local meta = _flash.read(start, SPIFLASHLOGGER_SECTOR_METADATA_SIZE);
        // Parse the meta data
        meta.seek(0);
        _disable();

        return { "id": meta.readn('i'), "map": meta.readn('w') };
    }

    //
    // Count the number of dirty chunks in sector
    //
    function _dirtyChunkCount(sector) {
        local map = _getSectorMetadata(sector).map;
        local count, mask;
        for (count = 0, mask = 0x0001; mask < 0x8000; mask = mask << 1) {
            if (!(map & mask)) count++;
            else break;
        }
        return count+1;// TODO: why was this plus one necessary?
    }

    //
    // Initialise logger in scope of the provided logger addresses
    //
    function _init() {
        local firstSector = {"id" : 0, "sec" : 0, "map": 0xFFFF}; // The smallest id
        local lastSector = {"id" : 0, "sec" : 0, "map": 0xFFFF};// The highest id
        // Read all the metadata
        _enable();
        for (local sector = 0; sector < _sectors; sector++) {
            // Hunt for the highest id and its map
            local meta = _getSectorMetadata(sector);

            if (meta.id > 0) {
                // identify last sector
                if (meta.id > lastSector.id)
                    lastSector = {"id" : meta.id, "sec" : sector, "map": meta.map};
                // identify first sector
                if (firstSector.id == 0 || meta.id < firstSector.id)
                    firstSector = {"id" : meta.id, "sec" : sector, "map": meta.map};

            } else {
                // This sector has no id, we are going to assume it is clean
                _map[sector] = SPIFLASHLOGGER_SECTOR_CLEAN;
            }
        }
        _disable();

        // handle ID overflow use-case
        if (lastSector.id - firstSector.id >= (0x7FFFFFFF - _sectors))
            lastSector = firstSector;

        _atPos = 0;
        _atSec = lastSector.sec;
        _nextSectorId = lastSector.id;
        // increase sector id
        _getNextSectorId();
        for (local bit = 1; bit <= 16; bit++) {
            local mod = 1 << bit;
            _atPos += (~lastSector.map & mod) ? SPIFLASHLOGGER_CHUNK_SIZE : 0;
        }
    }

    //
    //  Erase sector part
    //
    function _erase(startSector = null, endSector = null, preparing = false) {
        if (startSector == null) {
            startSector = 0;
            endSector = _sectors;
        }

        if (endSector == null) {
            endSector = startSector + 1;
        }

        if (startSector < 0 || endSector > _sectors) throw "Invalid format request";

        _enable();
        for (local sector = startSector; sector < endSector; sector++) {
            // Erase the requested sector
            _flash.erasesector(_start + (sector * SPIFLASHLOGGER_SECTOR_SIZE));
            // server.log(format("Erasing: %d (0x%04x)", sector, _start + (sector * SPIFLASHLOGGER_SECTOR_SIZE)));

            // Mark the sector as clean
            _map[sector] = SPIFLASHLOGGER_SECTOR_CLEAN;

            // Move the pointer on to the next sector
            if (!preparing && sector == _atSec) {
                _atSec = (_atSec + 1) % _sectors;
                _atPos = SPIFLASHLOGGER_SECTOR_METADATA_SIZE;
            }
        }
        _disable();
    }
}

// MIT License
//
// Copyright 2017 Electric Imp
//
// SPDX-License-Identifier: MIT
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
// EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES
// OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
// ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.

enum MODBUSRTU_SUB_FUNCTION_CODE {
    RETURN_QUERY_DATA = 0x0000,
    RESTART_COMMUNICATION_OPTION = 0x0001,
    RETURN_DIAGNOSTICS_REGISTER = 0x0002,
    CHANGE_ASCII_INPUT_DELIMITER = 0x0003,
    FORCE_LISTEN_ONLY_MODE = 0x0004,
    CLEAR_COUNTERS_AND_DIAGNOSTICS_REGISTER = 0x000A,
    RETURN_BUS_MESSAGE_COUNT = 0x000B,
    RETURN_BUS_COMMUNICATION_ERROR_COUNT = 0x000C,
    RETURN_BUS_EXCEPTION_ERROR_COUNT = 0x000D,
    RETURN_SLAVE_MESSAGE_COUNT = 0x000E,
    RETURN_SLAVE_NO_RESPONSE_COUNT = 0x000F,
    RETURN_SLAVE_NAK_COUNT = 0x0010,
    RETURN_SLAVE_BUSY_COUNT = 0x0011,
    RETURN_BUS_CHARACTER_OVERRUN_COUNT = 0x0012,
    CLEAR_OVERRUN_COUNTER_AND_FLAG = 0x0014
}

enum MODBUSRTU_EXCEPTION {
    ILLEGAL_FUNCTION = 0x01,
    ILLEGAL_DATA_ADDR = 0x02,
    ILLEGAL_DATA_VAL = 0x03,
    SLAVE_DEVICE_FAIL = 0x04,
    ACKNOWLEDGE = 0x05,
    SLAVE_DEVICE_BUSY = 0x06,
    NEGATIVE_ACKNOWLEDGE = 0x07,
    MEMORY_PARITY_ERROR = 0x08,
    RESPONSE_TIMEOUT = 0x50,
    INVALID_CRC = 0x51,
    INVALID_ARG_LENGTH = 0x52,
    INVALID_DEVICE_ADDR = 0x53,
    INVALID_TARGET_TYPE = 0x57,
    INVALID_VALUES = 0x58,
    INVALID_QUANTITY = 0x59,
}


enum MODBUSRTU_TARGET_TYPE {
    COIL,
    DISCRETE_INPUT,
    INPUT_REGISTER,
    HOLDING_REGISTER,
}

enum MODBUSRTU_READ_DEVICE_CODE {
    BASIC = 0x01,
    REGULAR = 0x02,
    EXTENDED = 0x03,
    SPECIFIC = 0x04,
}

enum MODBUSRTU_OBJECT_ID {
    VENDOR_NAME = 0x00,
    PRODUCT_CODE = 0x01,
    MAJOR_MINOR_REVISION = 0x02,
    VENDOR_URL = 0x03,
    PRODUCT_NAME = 0x04,
    MODEL_NAME = 0x05,
    USER_APPLICATION_NAME = 0x06,
}

class ModbusRTU {
    static VERSION = "1.0.1";
     // resLen and reqLen are the length of the PDU
    static FUNCTION_CODES = {
        readCoils = {
            fcode   = 0x01,
            reqLen  = 5,
            resLen  = function(n) {
                return 2 + math.ceil(n / 8.0);
            }
        },
        readInputs = {
            fcode   = 0x02,
            reqLen  = 5,
            resLen  = function(n) {
                return 2 + math.ceil(n / 8.0);
            }
        },
        readHoldingRegs = {
            fcode   = 0x03,
            reqLen  = 5,
            resLen  = function(n) {
                return 2 * n + 2;
            }
        },
        readInputRegs = {
            fcode   = 0x04,
            reqLen  = 5,
            resLen  = function(n) {
                return 2 * n + 2;
            }
        },
        writeSingleCoil = {
            fcode   = 0x05,
            reqLen  = 5,
            resLen  = 5
        },
        writeSingleReg = {
            fcode   = 0x06,
            reqLen  = 5,
            resLen  = 5
        },
        writeMultipleCoils = {
            fcode   = 0x0F,
            reqLen  = function(n) {
                return 6 + math.ceil(n / 8.0);
            },
            resLen  = 5
        },
        writeMultipleRegs = {
            fcode   = 0x10,
            reqLen  = function(n) {
                return 6 + n * 2;
            },
            resLen  = 5
        },
        readExceptionStatus = {
            fcode   = 0x07,
            reqLen  = 1,
            resLen  = 2
        },
        diagnostics = {
            fcode   = 0x08,
            reqLen  = function(n) {
                return 3 + n * 2;
            },
            resLen  = function(n) {
                return 3 + n * 2;
            }
        },
        reportSlaveID = {
            fcode   = 0x11,
            reqLen  = 1,
            resLen  = null
        },
        readDeviceIdentification = {
            fcode   = 0x2B,
            reqLen  = 4,
            resLen  = null
        },
        maskWriteRegister = {
            fcode   = 0x16,
            reqLen  = 7,
            resLen  = 7
        },
        readFIFOQueue = {
            fcode   = 0x18,
            reqLen  = 3,
            resLen  = function(n) {
                return 5 + n * 2;
            }
        },
        readWriteMultipleRegisters = {
            fcode   = 0x17,
            reqLen  = function(n) {
                return 10 + n * 2;
            },
            resLen  = function(n) {
                return 2 + n * 2;
            }
        }
    }

    //
    // function to create PDU for readWriteMultipleRegisters
    //
    static function createReadWriteMultipleRegistersPDU(readingStartAddress, readQuantity, writeStartAddress, writeQuantity, writeValue) {
        local readWriteMultipleRegisters = FUNCTION_CODES.readWriteMultipleRegisters;
        local PDU = blob(readWriteMultipleRegisters.reqLen(writeQuantity));
        PDU.writen(readWriteMultipleRegisters.fcode, 'b');
        PDU.writen(swap2(readingStartAddress), 'w');
        PDU.writen(swap2(readQuantity), 'w');
        PDU.writen(swap2(writeStartAddress), 'w');
        PDU.writen(swap2(writeQuantity), 'w');
        PDU.writen(writeValue.len(), 'b');
        PDU.writeblob(writeValue);
        return PDU;
    }

    //
    // function to create PDU for maskWriteRegister
    //
    static function createMaskWriteRegisterPDU(referenceAddress, AND_Mask, OR_Mask) {
        local maskWriteRegister = FUNCTION_CODES.maskWriteRegister;
        local PDU = blob(maskWriteRegister.reqLen);
        PDU.writen(maskWriteRegister.fcode, 'b');
        PDU.writen(swap2(referenceAddress), 'w');
        PDU.writen(swap2(AND_Mask), 'w');
        PDU.writen(swap2(OR_Mask), 'w');
        return PDU;
    }

    //
    // function to create PDU for reportSlaveID
    //
    static function createReportSlaveIdPDU() {
        local reportSlaveID = FUNCTION_CODES.reportSlaveID;
        local PDU = blob(reportSlaveID.reqLen);
        PDU.writen(reportSlaveID.fcode, 'b');
        return PDU;
    }

    //
    // function to create PDU for readDeviceIdentification
    //
    static function createReadDeviceIdentificationPDU(readDeviceIdCode, objectId) {
        const MEI_TYPE = 0x0E;
        local readDeviceIdentification = FUNCTION_CODES.readDeviceIdentification;
        local PDU = blob(readDeviceIdentification.reqLen);
        PDU.writen(readDeviceIdentification.fcode, 'b');
        PDU.writen(MEI_TYPE, 'b');
        PDU.writen(readDeviceIdCode, 'b');
        PDU.writen(objectId, 'b');
        return PDU;
    }

    //
    // function to create PDU for diagnostics
    //
    static function createDiagnosticsPDU(subFunctionCode, data) {
        local diagnostics = FUNCTION_CODES.diagnostics;
        local PDU = blob(diagnostics.reqLen(data.len() / 2));
        PDU.writen(diagnostics.fcode, 'b');
        PDU.writen(swap2(subFunctionCode), 'w');
        PDU.writeblob(data);
        return PDU;
    }

    //
    // function to create PDU for readExceptionStatus
    //
    static function createReadExceptionStatusPDU() {
        local readExceptionStatus = FUNCTION_CODES.readExceptionStatus;
        local PDU = blob(readExceptionStatus.reqLen);
        PDU.writen(readExceptionStatus.fcode, 'b');
        return PDU;
    }

    //
    // function to create PDU for read
    //
    static function createReadPDU(targetType, startingAddress, quantity) {
        local PDU = blob(targetType.reqLen);
        PDU.writen(targetType.fcode, 'b');
        PDU.writen(swap2(startingAddress), 'w');
        PDU.writen(swap2(quantity), 'w');
        return PDU;
    }

    //
    // function to create PDU for write
    //
    static function createWritePDU(targetType, startingAddress, numBytes, quantity, values) {
        local PDU = blob();
        PDU.writen(targetType.fcode, 'b');
        PDU.writen(swap2(startingAddress), 'w');
        if (quantity > 1) {
            PDU.writen(swap2(quantity), 'w');
            PDU.writen(numBytes, 'b');
        }
        PDU.writeblob(values);
        return PDU;
    }


    //
    // function to parse the incoming ADU
    //
    static function parse(params) {
        local PDU = params.PDU;
        local functionCode = PDU.readn('b');
        local expectedResType = params.expectedResType;
        local expectedResLen = params.expectedResLen;
        local result = false;
        if ((functionCode & 0x80) == 0x80) {
            // exception code
            throw PDU.readn('b');
        }
        if (expectedResLen && (PDU.len() < expectedResLen)) {
            return false;
        }
        if (functionCode != expectedResType){
            return -1;
        }
        switch (functionCode) {
            case FUNCTION_CODES.readExceptionStatus.fcode:
                return _readExceptionStatus(PDU);
            case FUNCTION_CODES.readDeviceIdentification.fcode:
                return _readDeviceIdentification(PDU);
            case FUNCTION_CODES.reportSlaveID.fcode:
                return _reportSlaveID(PDU);
            case FUNCTION_CODES.diagnostics.fcode:
                return _diagnostics(PDU, params.quantity);
            case FUNCTION_CODES.readCoils.fcode:
            case FUNCTION_CODES.readInputs.fcode:
            case FUNCTION_CODES.readHoldingRegs.fcode:
            case FUNCTION_CODES.readInputRegs.fcode:
            case FUNCTION_CODES.readWriteMultipleRegisters.fcode:
                return _readData(PDU, expectedResType, params.quantity);
            case FUNCTION_CODES.writeSingleCoil.fcode:
            case FUNCTION_CODES.writeSingleReg.fcode:
            case FUNCTION_CODES.writeMultipleCoils.fcode:
            case FUNCTION_CODES.writeMultipleRegs.fcode:
            case FUNCTION_CODES.maskWriteRegister.fcode:
                return _writeData(PDU);
        }
    }

    //
    // function to parse ADU for diagnostics
    //
    static function _diagnostics(PDU, quantity) {
        PDU.seek(3);
        local result = [];
        while (result.len() != quantity) {
            result.push(swap2(PDU.readn('w')));
        }
        return result;
    }

    //
    // function to parse ADU for readExceptionStatus
    //
    static function _readExceptionStatus(PDU) {
        PDU.seek(1);
        return PDU.readn('b');
    }

    //
    // function to parse ADU for write
    //
    static function _writeData(PDU) {
        return true;
    }

    //
    // function to parse ADU for read
    //
    static function _readData(PDU, expectedResType, quantity) {
        PDU.seek(2);
        local result = [];
        switch (expectedResType) {
            case FUNCTION_CODES.readCoils.fcode:
            case FUNCTION_CODES.readInputs.fcode:
                while (!PDU.eos()) {
                    local byte = PDU.readn('b');
                    local bitmask = 1;
                    for (local bit = 0; bit < 8; ++ bit) {
                        result.push((byte & (bitmask << bit)) != 0x00);
                        if (result.len() == quantity) {
                            // move the pointer to the end to break out of the while loop
                            PDU.seek(0, 'e');
                            break;
                        }
                    }
                }
                break;
            case FUNCTION_CODES.readWriteMultipleRegisters.fcode:
            case FUNCTION_CODES.readHoldingRegs.fcode:
            case FUNCTION_CODES.readInputRegs.fcode:
                while (result.len() != quantity) {
                    result.push(swap2(PDU.readn('w')));
                }
                break;
        }
        return result;
    }

    //
    // function to parse ADU for reportSlaveID
    //
    static function _reportSlaveID(PDU) {
        PDU.seek(1);
        local byteCount = PDU.readn('b');
        if (PDU.len() - PDU.tell() >= byteCount) {
             local results = {
                 slaveId      = PDU.readstring(byteCount - 1),
                 runIndicator = ((PDU.readn('b') == 0) ? false: true),
             };
             return results;
        }
        return false;
    }

    //
    // function to parse ADU for readDeviceIdentification
    //
    static function _readDeviceIdentification(PDU) {
         if (PDU.len() < 7) {
             // Not enough data for this function code
             return false;
         }
         PDU.seek(6);
         local objectCount = PDU.readn('b');
         local objects = {};
         while (objects.len() < objectCount) {
             if (PDU.len() - PDU.tell() < 2) {
                 // Not enough data
                 return false;
             }
             local currentObjectId = PDU.readn('b');
             local currentObjectLen = PDU.readn('b');
             if (PDU.len() - PDU.tell() < currentObjectLen) {
                 // Not enough data
                 return false;
             }
             objects[currentObjectId] <- PDU.readstring(currentObjectLen);
         }
         return objects;
     }
}
// MIT License
//
// Copyright 2017 Electric Imp
//
// SPDX-License-Identifier: MIT
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
// EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES
// OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
// ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.

class ModbusMaster {
    static VERSION = "1.0.1";
    _debug = null;

    //
    // Constructor for ModbusMaster
    //
    // @param  {bool} debug - the debug flag
    //
    constructor(debug) {
        _debug = debug;
    }

    //
    // This function reads the description of the type, the current status, and other information specific to a remote device.
    //
    // @param {function} callback - The function to be fired when it receives response regarding this request
    //
    function reportSlaveID(callback = null) {
        local properties = {
            expectedResLen = ModbusRTU.FUNCTION_CODES.reportSlaveID.resLen,
            expectedResType = ModbusRTU.FUNCTION_CODES.reportSlaveID.fcode,
            callback = callback
        };
        _send(ModbusRTU.createReportSlaveIdPDU(), properties);
    }

    //
    // This function reads the contents of eight Exception Status outputs in a remote device
    //
    // @param {function} callback - The function to be fired when it receives response regarding this request
    //
    function readExceptionStatus(callback = null) {
        local properties = {
            expectedResLen = ModbusRTU.FUNCTION_CODES.readExceptionStatus.resLen,
            expectedResType = ModbusRTU.FUNCTION_CODES.readExceptionStatus.fcode,
            callback = callback
        };
        _send(ModbusRTU.createReadExceptionStatusPDU(), properties);
    }

    //
    // This function provides a series of tests for checking the communication system between a client ( Master) device and a server ( Slave), or for checking various internal error conditions within a server.
    //
    // @param {integer} deviceAddress - The unique address that identifies a device
    // @param {integer} subFunctionCode - The address from which it begins reading values
    // @param {blob} data - The data field required by Modbus request
    // @param {function} callback - The function to be fired when it receives response regarding this request
    //
    function diagnostics(subFunctionCode, data, callback = null) {
        local quantity = data.len() / 2;
        local properties = {
            expectedResLen = ModbusRTU.FUNCTION_CODES.diagnostics.resLen(quantity),
            expectedResType = ModbusRTU.FUNCTION_CODES.diagnostics.fcode,
            quantity = quantity,
            callback = callback
        };
        _send(ModbusRTU.createDiagnosticsPDU(subFunctionCode, data), properties);
    }

    //
    // This function modifies the contents of a specified holding register using a combination of an AND mask, an OR mask, and the register's current contents. The function can be used to set or clear individual bits in the register.
    //
    // @param {integer} referenceAddress - The address of the holding register the value is written into
    // @param {integer} AND_mask - The AND mask
    // @param {integer} OR_mask - The OR mask
    // @param {function} callback - The function to be fired when it receives response regarding this request
    //
    function maskWriteRegister(referenceAddress, AND_Mask, OR_Mask, callback = null) {
        local properties = {
            expectedResLen = ModbusRTU.FUNCTION_CODES.maskWriteRegister.resLen,
            expectedResType = ModbusRTU.FUNCTION_CODES.maskWriteRegister.fcode,
            callback = callback
        };
        _send(ModbusRTU.createMaskWriteRegisterPDU(referenceAddress, AND_Mask, OR_Mask), properties);
    }

    //
    // This function performs a combination of one read operation and one write operation in a single MODBUS transaction. The write operation is performed before the read.
    //
    // @param {integer} readingStartAddress - The address from which it begins reading values
    // @param {integer} readQuantity - The number of consecutive addresses values are read from
    // @param {integer} writeStartAddress - The address from which it begins writing values
    // @param {integer} writeQuantity - The number of consecutive addresses values are written into
    // @param {blob} writeValue - The value written into the holding register
    // @param {function} callback - The function to be fired when it receives response regarding this request
    //
    function readWriteMultipleRegisters(readingStartAddress, readQuantity, writeStartAddress, writeQuantity, writeValue, callback = null) {
        writeValue = _processWriteRegistersValues(writeQuantity, writeValue);
        local properties = {
            expectedResLen = ModbusRTU.FUNCTION_CODES.readWriteMultipleRegisters.resLen(readQuantity),
            expectedResType = ModbusRTU.FUNCTION_CODES.readWriteMultipleRegisters.fcode,
            quantity = readQuantity,
            callback = callback
        };
        _send(ModbusRTU.createReadWriteMultipleRegistersPDU(readingStartAddress, readQuantity, writeStartAddress, writeQuantity, writeValue), properties);
    }

    //
    // This function allows reading the identification and additional information relative to the physical and functional description of a remote device, only.
    //
    // @param {enum} readDeviceIdCode - read device id code
    // @param {enum} objectId - object id
    // @param {function} callback - The function to be fired when it receives response regarding this request
    //
    function readDeviceIdentification(readDeviceIdCode, objectId, callback = null) {
        local properties = {
            expectedResLen = ModbusRTU.FUNCTION_CODES.readDeviceIdentification.resLen,
            expectedResType = ModbusRTU.FUNCTION_CODES.readDeviceIdentification.fcode,
            callback = callback
        };
        _send(ModbusRTU.createReadDeviceIdentificationPDU(readDeviceIdCode, objectId), properties);
    }

    //
    // This is the generic function to write values into coils or holding registers .
    //
    // @param {enum} targetType - The address from which it begins reading values
    // @param {integer} startingAddress - The address from which it begins writing values
    // @param {integer} quantity - The number of consecutive addresses the values are written into
    // @param {integer, Array[integer, Bool], Bool, blob} values - The values written into Coils or Registers
    // @param {function} callback - The function to be fired when it receives response regarding this request
    //
    function write(targetType, startingAddress, quantity, values, callback = null) {
        try {
            if (quantity < 1) {
                throw MODBUSRTU_EXCEPTION.INVALID_QUANTITY;
            }
            switch (targetType) {
                case MODBUSRTU_TARGET_TYPE.COIL:
                    return _writeCoils(startingAddress, quantity, values, callback);
                case MODBUSRTU_TARGET_TYPE.HOLDING_REGISTER:
                    return _writeRegisters(startingAddress, quantity, values, callback);
                default:
                    throw MODBUSRTU_EXCEPTION.INVALID_TARGET_TYPE;
            }
        } catch (error) {
            _callbackHandler(error, null, callback);
        }
    }

    //
    // This is the generic function to read values from a single coil, register or multiple coils, registers .
    //
    // @param {enum} targetType - The address from which it begins reading values
    // @param {integer} startingAddress - The address from which it begins reading values
    // @param {integer} quantity - The number of consecutive addresses the values are read from
    // @param {function} callback - The function to be fired when it receives response regarding this request
    //
    function read(targetType, startingAddress, quantity, callback = null) {
        try {
            local PDU = null;
            local resLen = null;
            local resType = null;
            if (quantity < 1) {
                throw MODBUSRTU_EXCEPTION.INVALID_QUANTITY;
            }
            switch (targetType) {
                case MODBUSRTU_TARGET_TYPE.COIL:
                    PDU = ModbusRTU.createReadPDU(ModbusRTU.FUNCTION_CODES.readCoils, startingAddress, quantity);
                    resLen = ModbusRTU.FUNCTION_CODES.readCoils.resLen(quantity);
                    resType = ModbusRTU.FUNCTION_CODES.readCoils.fcode;
                    break;
                case MODBUSRTU_TARGET_TYPE.DISCRETE_INPUT:
                    PDU = ModbusRTU.createReadPDU(ModbusRTU.FUNCTION_CODES.readInputs, startingAddress, quantity);
                    resLen = ModbusRTU.FUNCTION_CODES.readInputs.resLen(quantity);
                    resType = ModbusRTU.FUNCTION_CODES.readInputs.fcode;
                    break;
                case MODBUSRTU_TARGET_TYPE.HOLDING_REGISTER:
                    PDU = ModbusRTU.createReadPDU(ModbusRTU.FUNCTION_CODES.readHoldingRegs, startingAddress, quantity);
                    resLen = ModbusRTU.FUNCTION_CODES.readHoldingRegs.resLen(quantity);
                    resType = ModbusRTU.FUNCTION_CODES.readHoldingRegs.fcode;
                    break;
                case MODBUSRTU_TARGET_TYPE.INPUT_REGISTER:
                    PDU = ModbusRTU.createReadPDU(ModbusRTU.FUNCTION_CODES.readInputRegs, startingAddress, quantity);
                    resLen = ModbusRTU.FUNCTION_CODES.readInputRegs.resLen(quantity);
                    resType = ModbusRTU.FUNCTION_CODES.readInputRegs.fcode;
                    break;
                default:
                    throw MODBUSRTU_EXCEPTION.INVALID_TARGET_TYPE;
            }
            local properties = {
                expectedResLen = resLen,
                expectedResType = resType,
                quantity = quantity,
                callback = callback
            };
            _send(PDU, properties);
        } catch (error) {
            _callbackHandler(error, null, callback);
        }
    }

    //
    // construct a writeCoils request
    //
    function _writeCoils(startingAddress, quantity, values, callback) {
        local numBytes = math.ceil(quantity / 8.0);
        local writeValues = blob(numBytes);
        local functionType = (quantity == 1) ? ModbusRTU.FUNCTION_CODES.writeSingleCoil : ModbusRTU.FUNCTION_CODES.writeMultipleCoils;
        switch (typeof values) {
            case "integer":
                writeValues.writen(swap2(values), 'w');
                break;
            case "bool":
                writeValues.writen(swap2(values ? 0xFF00 : 0), 'w');
                break;
            case "blob":
                writeValues = values;
                break;
            case "array":
                if (quantity != values.len()) {
                    throw MODBUSRTU_EXCEPTION.INVALID_ARG_LENGTH;
                }
                local byte, bitshift;
                foreach (bit, val in values) {
                    byte = bit / 8;
                    bitshift = bit % 8;
                    writeValues[byte] = writeValues[byte] | ((val ? 1 : 0) << bitshift);
                }
                break;
            default:
                throw MODBUSRTU_EXCEPTION.INVALID_VALUES;
        }
        local properties = {
            callback = callback,
            expectedResType = functionType.fcode,
            expectedResLen = functionType.resLen,
            quantity = quantity
        };
        _send(ModbusRTU.createWritePDU(functionType, startingAddress, numBytes, quantity, writeValues), properties);
    }

    //
    // construct a writeRegisters request
    //
    function _writeRegisters(startingAddress, quantity, values, callback) {
        local numBytes = quantity * 2;
        local writeValues = _processWriteRegistersValues(quantity, values);
        local functionType = (quantity == 1) ? ModbusRTU.FUNCTION_CODES.writeSingleReg : ModbusRTU.FUNCTION_CODES.writeMultipleRegs;
        local properties = {
            callback = callback,
            expectedResType = functionType.fcode,
            expectedResLen = functionType.resLen,
            quantity = quantity
        };
        _send(ModbusRTU.createWritePDU(functionType, startingAddress, numBytes, quantity, writeValues), properties);
    }

    //
    // process the values written into the holding registers
    //
    function _processWriteRegistersValues(quantity, values) {
        local writeValues = blob();
        switch (typeof values) {
            case "integer":
                writeValues.writen(swap2(values), 'w');
                break;
            case "blob":
                writeValues = values;
                break;
            case "array":
                if (quantity != values.len()) {
                    throw MODBUSRTU_EXCEPTION.INVALID_ARG_LENGTH;
                }
                foreach (value in values) {
                    writeValues.writen(swap2(value), 'w');
                }
                break;
            default:
                throw MODBUSRTU_EXCEPTION.INVALID_VALUES;
        }
        return writeValues;
    }

    //
    // log the message
    //
    function _log(message, prefix = "") {
        if (_debug) {
            switch (typeof message) {
                case "blob":
                    local mes = prefix;
                    foreach (value in message) {
                        mes += format("%02X ", value);
                    }
                    return server.log(mes);
                default:
                    return server.log(message);
            }
        }
    }

    //
    // abstract function
    // send the ADU
    //
    function _send(PDU, properties);

    //
    // abstract function
    // fire the callback
    //
    function _callbackHandler(error, result, callback);

    //
    // abstract function
    // create an ADU
    //
    function _createADU(PDU);
}


// MIT License
//
// Copyright 2017 Electric Imp
//
// SPDX-License-Identifier: MIT
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
// EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES
// OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
// ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.

class ModbusTCPMaster extends ModbusMaster {
    static VERSION = "1.0.1";
    static MAX_TRANSACTION_COUNT = 65535;
    _transactions = null;
    _wiz = null;
    _transactionCount = null;
    _connection = null;
    _connectionSettings = null;
    _shouldRetry = null;
    _connectCallback = null;
    _reconnectCallback = null;

    //
    // Constructor for ModbusTCPMaster
    //
    // @param  {object} wiz - The W5500 object
    // @param  {bool} debug - false by default. If enabled, the outgoing and incoming ADU will be printed for debugging purpose
    //
    constructor(wiz, debug = false) {
        base.constructor(debug);
        _wiz = wiz;
        _transactionCount = 1;
        _transactions = {};
    }

    //
    // configure and open a TCP connection
    //
    // @param  {table} networkSettings - The network settings table. It entails sourceIP, subnet, gatewayIP
    // @param  {table} connectionSettings - The connection settings table. It entails device IP and port
    // @param  {function} connectCallback - The function to be fired when the connection is established
    // @param  {function} reconnectCallback - The function to be fired when the connection is reestablished
    //
    function connect(connectionSettings, connectCallback = null, reconnectCallback = null) {
        _shouldRetry = true;
        _connectCallback = connectCallback;
        _reconnectCallback = reconnectCallback;
        _connectionSettings = connectionSettings;
        _wiz.onReady(function() {
            local destIP = connectionSettings.destIP;
            local destPort = connectionSettings.destPort;
            _wiz.openConnection(destIP, destPort, _onConnect.bindenv(this));
        }.bindenv(this));
    }

    //
    // close the existing TCP connection
    //
    // @param  {function} callback - The function to be fired when the connection is closed
    //
    function disconnect(callback = null) {
        _shouldRetry = false;
        _connection.close(callback);
    }

    //
    // The callback function to be fired when the connection is established
    //
    function _onConnect(error, conn) {
        if (error) {
            return _callbackHandler(error, null, _connectCallback);
        }
        _connection = conn;
        _connection.onReceive(_parseADU.bindenv(this));
        _connection.onDisconnect(_onDisconnect.bindenv(this));
        _callbackHandler(null, conn, _connectCallback);
    }

    //
    // The callback function to be fired when the connection is dropped
    //
    function _onDisconnect(conn) {
        _connection = null;
        if (_shouldRetry) {
            if (_reconnectCallback != null) {
                _connectCallback = _reconnectCallback;
            }
            _wiz.openConnection(_connectionSettings.destIP, _connectionSettings.destPort, _onConnect.bindenv(this));
        }
    }

    //
    // The callback function to be fired it receives a packet
    //
    function _parseADU(error, ADU) {
        if (error) {
            return _callbackHandler(error, null, _connectCallback);
        }
        ADU.seek(0);
        local header = ADU.readblob(7);
        local transactionID = swap2(header.readn('w'));
        local PDU = ADU.readblob(ADU.len() - 7);
        local params = null;
        try {
            params = _transactions[transactionID];
        } catch (error) {
            return _callbackHandler(format("Error parsing the response, transactionID %d does not exist", transactionID), null, _connectCallback);
        }
        local callback = params.callback;
        params.PDU <- PDU;
        try {
            local result = ModbusRTU.parse(params);
            _callbackHandler(null, result, callback);
        } catch (error) {
            _callbackHandler(error, null, callback);
        }
        _transactions.rawdelete(transactionID);
        _log(ADU, "Incoming ADU: ");
    }

    //
    // create an ADU
    //
    function _createADU(PDU) {
        local ADU = blob();
        ADU.writen(swap2(_transactionCount), 'w');
        ADU.writen(swap2(0x0000), 'w');
        ADU.writen(swap2(PDU.len() + 1), 'w');
        ADU.writen(0x00, 'b');
        ADU.writeblob(PDU);
        return ADU;
    }

    //
    // send the ADU via Ethernet
    //
    function _send(PDU, properties) {
        _transactions[_transactionCount] <- properties;
        local ADU = _createADU(PDU);
        // with or without success in transmission of data, the transaction count would be advanced
        _transactionCount = (_transactionCount + 1) % MAX_TRANSACTION_COUNT;
        _connection.transmit(ADU, function(error) {
            if (error) {
                _callbackHandler(error, null, properties.callback);
            } else {
                _log(ADU, "Outgoing ADU: ");
            }
        }.bindenv(this));
    }

    //
    // fire the callback
    //
    function _callbackHandler(error, result, callback) {
        if (callback) {
            if (error) {
                callback(error, null);
            } else {
                callback(null, result);
            }
        }
    }
}
// MIT License
//
// Copyright 2016-2017 Electric Imp
//
// SPDX-License-Identifier: MIT
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
// EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES
// OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
// ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.


// BLOCK SELECT BITS
const W5500_COMMON_REGISTER = 0x00;
const W5500_S0_REGISTER = 0x08;
const W5500_S0_TX_BUFFER = 0x10;
const W5500_S0_RX_BUFFER = 0x18;
const W5500_S1_REGISTER = 0x28;
const W5500_S1_TX_BUFFER = 0x30;
const W5500_S1_RX_BUFFER = 0x38;
const W5500_S2_REGISTER = 0x48;
const W5500_S2_TX_BUFFER = 0x50;
const W5500_S2_RX_BUFFER = 0x58;
const W5500_S3_REGISTER = 0x68;
const W5500_S3_TX_BUFFER = 0x70;
const W5500_S3_RX_BUFFER = 0x78;
const W5500_S4_REGISTER = 0x88;
const W5500_S4_TX_BUFFER = 0x90;
const W5500_S4_RX_BUFFER = 0x98;
const W5500_S5_REGISTER = 0xA8;
const W5500_S5_TX_BUFFER = 0xB0;
const W5500_S5_RX_BUFFER = 0xB8;
const W5500_S6_REGISTER = 0xC8;
const W5500_S6_TX_BUFFER = 0xD0;
const W5500_S6_RX_BUFFER = 0xD8;
const W5500_S7_REGISTER = 0xE8;
const W5500_S7_TX_BUFFER = 0xF0;
const W5500_S7_RX_BUFFER = 0xF8;

// READ/WRITE BIT
const W5500_READ_COMMAND = 0x00;
const W5500_WRITE_COMMAND = 0x04;

// SPI OPPERATION MODE
const W5500_VARIABLE_DATA_LENGTH = 0x00;
const W5500_FIXED_DATA_LENGTH_1 = 0x01;
const W5500_FIXED_DATA_LENGTH_2 = 0x02;
const W5500_FIXED_DATA_LENGTH_4 = 0x03;

// COMMON REGISTERS OFFSET ADDRESSES
// MR
const W5500_MODE = 0x0000;

// GWR
const W5500_GATEWAY_ADDR_0 = 0x0001;
const W5500_GATEWAY_ADDR_1 = 0x0002;
const W5500_GATEWAY_ADDR_2 = 0x0003;
const W5500_GATEWAY_ADDR_3 = 0x0004;

// SUBR
const W5500_SUBNET_MASK_ADDR_0 = 0x0005;
const W5500_SUBNET_MASK_ADDR_1 = 0x0006;
const W5500_SUBNET_MASK_ADDR_2 = 0x0007;
const W5500_SUBNET_MASK_ADDR_3 = 0x0008;

// SHAR
const W5500_SOURCE_HW_ADDR_0 = 0x0009;
const W5500_SOURCE_HW_ADDR_1 = 0x000A;
const W5500_SOURCE_HW_ADDR_2 = 0x000B;
const W5500_SOURCE_HW_ADDR_3 = 0x000C;
const W5500_SOURCE_HW_ADDR_4 = 0x000D;
const W5500_SOURCE_HW_ADDR_5 = 0x000E;

// SIPR
const W5500_SOURCE_IP_ADDR_0 = 0x000F;
const W5500_SOURCE_IP_ADDR_1 = 0x0010;
const W5500_SOURCE_IP_ADDR_2 = 0x0011;
const W5500_SOURCE_IP_ADDR_3 = 0x0012;

// INTLEVEL
const W5500_INTERRUPT_LOW_LEVEL_TIMER_0 = 0x0013;
const W5500_INTERRUPT_LOW_LEVEL_TIMER_1 = 0x0014;

// IR
const W5500_INTERRUPT = 0x0015;
// IMR
const W5500_INTERRUPT_MASK = 0x0016;

// SIR
const W5500_SOCKET_INTERRUPT = 0x0017;
// SIMR
const W5500_SOCKET_INTERRUPT_MASK = 0x0018;

// RTR
const W5500_RETRY_TIME_0 = 0x0019;
const W5500_RETRY_TIME_1 = 0x001A;
// RCR
const W5500_RETRY_COUNT = 0x001B;

// PHYSICAL CONFIG
const W5500_PHYSICAL_CONFIG = 0x002E;

// VERSION
const W5500_CHIP_VERSION = 0x0039;


// SOCKET REGISTER OFFSET ADDRESSES
const W5500_SOCKET_MODE = 0x0000;
const W5500_SOCKET_COMMAND = 0x0001;
const W5500_SOCKET_N_INTERRUPT = 0x0002;
const W5500_SOCKET_STATUS = 0x0003;

const W5500_SOCKET_SOURCE_PORT_0 = 0x0004;
const W5500_SOCKET_SOURCE_PORT_1 = 0x0005;

const W5500_SOCKET_DEST_HW_ADDR_0 = 0x0006;
const W5500_SOCKET_DEST_HW_ADDR_1 = 0x0007;
const W5500_SOCKET_DEST_HW_ADDR_2 = 0x0008;
const W5500_SOCKET_DEST_HW_ADDR_3 = 0x0009;
const W5500_SOCKET_DEST_HW_ADDR_4 = 0x000A;
const W5500_SOCKET_DEST_HW_ADDR_5 = 0x000B;

const W5500_SOCKET_DEST_IP_ADDR_0 = 0x000C;
const W5500_SOCKET_DEST_IP_ADDR_1 = 0x000D;
const W5500_SOCKET_DEST_IP_ADDR_2 = 0x000E;
const W5500_SOCKET_DEST_IP_ADDR_3 = 0x000F;

const W5500_SOCKET_DEST_PORT_0 = 0x0010;
const W5500_SOCKET_DEST_PORT_1 = 0x0011;

const W5500_SOCKET_RX_BUFFER_SIZE = 0x001E;
const W5500_SOCKET_TX_BUFFER_SIZE = 0x001F;

// SOCKET TX FREE SIZE REGISTER (Sn_TX_FREE_SIZE)
const W5500_SOCKET_TX_SIZE_R1 = 0x0020;
const W5500_SOCKET_TX_SIZE_R2 = 0x0021;

// SOCKET TX READ POINTER
const W5500_SOCKET_TX_RP_R1 = 0x0022;
const W5500_SOCKET_TX_RP_R2 = 0x0023;

// SOCKET TX WRITE POINTER
const W5500_SOCKET_TX_WP_R1 = 0x0024;
const W5500_SOCKET_TX_WP_R2 = 0x0025;

// SOCKET RX RECEIVED SIZE REGISTER (Sn_RX_RSR)
const W5500_SOCKET_RX_SIZE_R1 = 0x0026;
const W5500_SOCKET_RX_SIZE_R2 = 0x0027;

// SOCKET RX READ POINTER (Sn_RX_RD)
const W5500_SOCKET_RX_RP_R1 = 0x0028;
const W5500_SOCKET_RX_RP_R2 = 0x0029;

// SOCKET N INTERRUPT MASK (Sn_IMR)
const W5500_SOCKET_N_INTERRUPT_MASK = 0x002C;

// MODES
const W5500_SW_RESET = 0x80;
const W5500_WAKE_ON_LAN = 0x20;
const W5500_PING_BLOCK = 0x10;
const W5500_PPPoE = 0x08;
const W5500_FORCE_ARP = 0x01;
const W5500_DEFAULT_MODE = 0x00;

// SOCKET MODES (Sn_MR)
const W5500_SOCKET_MODE_MULTI = 0x80;
const W5500_SOCKET_MODE_BROADCAST_BLOCKING = 0x40;
const W5500_SOCKET_MODE_NO_DELAY_ACK = 0x20;
const W5500_SOCKET_MODE_UNICAST_BLOCKING = 0x10;
const W5500_SOCKET_MODE_CLOSED = 0x00;
const W5500_SOCKET_MODE_TCP = 0x01;
const W5500_SOCKET_MODE_UDP = 0x02;
const W5500_SOCKET_MODE_MACRAW = 0x03;

// SOCKET COMMANDS (Sn_CR)
const W5500_SOCKET_OPEN = 0x01;
const W5500_SOCKET_LISTEN = 0x02;
const W5500_SOCKET_CONNECT = 0x04;
const W5500_SOCKET_DISCONNECT = 0x08;
const W5500_SOCKET_CLOSE = 0x10;
const W5500_SOCKET_SEND = 0x20;
const W5500_SOCKET_SEND_MAC = 0x21;
const W5500_SOCKET_SEND_KEEP = 0x22;
const W5500_SOCKET_RECEIVE = 0x40;

// SOCKET STATUS (Sn_SR)
const W5500_SOCKET_STATUS_CLOSED = 0x00;
const W5500_SOCKET_STATUS_INIT = 0x13;
const W5500_SOCKET_STATUS_LISTEN = 0x14;
const W5500_SOCKET_STATUS_ESTABLISHED = 0x17;
const W5500_SOCKET_STATUS_CLOSE_WAIT = 0x1C;
const W5500_SOCKET_STATUS_UDP = 0x22;
const W5500_SOCKET_STATUS_MACRAW = 0x42;
const W5500_SOCKET_STATUS_SYNSENT = 0x15;
const W5500_SOCKET_STATUS_SYNRECV = 0x16;
const W5500_SOCKET_STATUS_FIN_WAIT = 0x18;
const W5500_SOCKET_STATUS_CLOSING = 0x1A;
const W5500_SOCKET_STATUS_TIME_WAIT = 0x1B;
const W5500_SOCKET_STATUS_LAST_ACK = 0x1D;

// INTERRUPT TYPES
const W5500_CONFLICT_INT_TYPE = 0x80;
const W5500_UNREACH_INT_TYPE = 0x40;
const W5500_PPPoE_INT_TYPE = 0x20;
const W5500_MAGIC_PACKET_TYPE = 0x10;
const W5500_NONE_INT_TYPE = 0x00;

// SOCKET INTERRUPTS
const W5500_DISABLE_SOCKET_INTERRUPTS = 0x00;
const W5500_S0_INTERRUPT = 0x01;
const W5500_S1_INTERRUPT = 0x02;
const W5500_S2_INTERRUPT = 0x04;
const W5500_S3_INTERRUPT = 0x08;
const W5500_S4_INTERRUPT = 0x10;
const W5500_S5_INTERRUPT = 0x20;
const W5500_S6_INTERRUPT = 0x40;
const W5500_S7_INTERRUPT = 0x80;

// SOCKET INTERRUPT TYPES
const W5500_SEND_COMPLETE_INT_TYPE = 0x10;
const W5500_TIMEOUT_INT_TYPE = 0x08;
const W5500_DATA_RECEIVED_INT_TYPE = 0x04;
const W5500_DISCONNECTED_INT_TYPE = 0x02;
const W5500_CONNECTED_INT_TYPE = 0x01;
const W5500_ALL_INT_TYPES = 0x1F;

// Socket states
enum W5500_SOCKET_STATES {
    CLOSED,
    INIT,
    CONNECTING,
    LISTENING,
    ESTABLISHED,
    DISCONNECTING
}

// Error messages
const W5500_ERR_INVALID_PARAMETERS = "Provide 'ip', 'port', 'mode' and a callback";
const W5500_ERR_CANNOT_CONNECT_STILL_CLEANING = "Cannot open a connection. Still cleaning up";
const W5500_ERR_CANNOT_CONNECT_SOCKETS_IN_USE = "Cannot open a connection. All connection sockets in use";
const W5500_ERR_CANNOT_CONNECT_TIMEOUT = "Connection timeout";
const W5500_ERR_TRANSMIT_TIMEOUT = "Transmit timeout";
const W5500_ERR_RECEIVE_TIMEOUT = "Receive timeout";
const W5500_ERR_COMMAND_TIMEOUT = "Command timeout";
const W5500_ERR_NOT_CONNECTED = "Not connected";


// Miscellaneous constants
const W5500_CONNECT_TIMEOUT = 60;
const W5500_TRANSMIT_TIMEOUT = 8;
const W5500_COMMAND_TIMEOUT = 3;
const W5500_INTERRUPT_POLL_TIME_IDLE = 0.5;
const W5500_INTERRUPT_POLL_TIME_ACTIVE = 0.01;

// ==============================================================================
// CLASS: W5500
// ==============================================================================

class W5500 {

    static VERSION = "2.1.1";

    _driver = null;
    _isReady = false; // set to true once the driver is loaded and connection to chip made
    _readyCb = null; // callback for when isReady becomes true
    _networkSettings = null;

// TODO: implement this
    _autoRetry = false;


// ***************************************************************************
// Constructor
// Returns: null
// Parameters:
//      interruptPin - interrupt pin
//      spi - configured spi bus, chip supports spi mode 0 or 3
//      csPin(optional) -  chip select pin, pass in if not using imp005
//      resetPin(optional) - reset pin
//      autoRetry(optional) - not implemented yet.
//      setMac(optional) - set the MAC address of the chip to the imp's own
//                         MAC, with the last bit flipped.
// ***************************************************************************
    constructor(interruptPin, spi, csPin = null, resetPin = null, autoRetry = false, setMac = true) {

        // Initialise the driver
        _driver = W5500.Driver(interruptPin, spi, csPin, resetPin, setMac);
        _driver.init(function() {

            // Let the caller know the device is ready
            _isReady = true;
            if (_readyCb) imp.wakeup(0, _readyCb);

        }.bindenv(this));
    }


// ***************************************************************************
// reset, note this is blocking for 0.2s
// Returns: this
// Parameters:
//          sw(optional) - boolean if true forces a software reset
//          Note: Datasheet states that SW reset should not be used
//                 on the W5500.
// ***************************************************************************
    function reset(sw = false) {

        // Initialise the driver
        _isReady = false;
        _driver.reset(sw, function() {
            _driver.init(function() {

                // Let the caller know the device is ready
                _isReady = true;
                if (_readyCb) imp.wakeup(0, _readyCb);

            }.bindenv(this));
        }.bindenv(this));
    }



// ***************************************************************************
// onReady
// Returns: this
// Parameters:
//      cb - function to be called when the Wiznet device is initialised
// ***************************************************************************
    function onReady(cb) {

        local _cb = cb;
        if (_networkSettings != null) {
            // Slip the network settings into the _cb
            _cb = function() {
                if ("availableSockets" in _networkSettings) {
                    _driver.setNumberOfAvailableSockets(_networkSettings.availableSockets);
                }
                if ("sourceIP" in _networkSettings) {
                    configureNetworkSettings(_networkSettings.sourceIP, _networkSettings.subnetMask, _networkSettings.gatewayIP, _networkSettings.mac);
                }
                _networkSettings = null;
                cb();
            }.bindenv(this);
        }

        if (_isReady) imp.wakeup(0, _cb);
        else _readyCb = _cb;

        return this;
    }



// ***************************************************************************
// setNumberOfAvailableSockets - configures interrupts and memory for each connection
// Returns: number of actual connections configured
// Parameters:
//      numSockets - number of desired connections
// **************************************************************************
    function setNumberOfAvailableSockets(numSockets = null) {
        if (_isReady) {
            return _driver.setNumberOfAvailableSockets(numSockets);
        } else {
            if (_networkSettings == null) {
                _networkSettings = {};
            }
            _networkSettings.availableSockets <- numSockets;
        }
    }


// ***************************************************************************
// isPhysicallyConnected
// Returns: true if there is a cable plugged in
// Parameters:
//      none
// ***************************************************************************
    function isPhysicallyConnected() {
        return _driver.getPhysicalLinkStatus();
    }


// ***************************************************************************
// forceCloseAllSockets
// Returns: nothing
// Parameters:
//      none
// ***************************************************************************
    function forceCloseAllSockets() {
        return _driver.forceCloseAllSockets();
    }


// ***************************************************************************
// configureNetworkSettings
// Returns: this
// Parameters:
//      sourceIP    -   ip adress of the Wiznet
//      subnetMask  -   subnetMask for the Wiznet
//      gatewayIP   -   gatewayIP for the Wiznet
//      mac         -   mac adress for the wiznet
// ***************************************************************************
    function configureNetworkSettings(sourceIP, subnetMask = null, gatewayIP = null, mac = null) {
        if (_isReady) {
            if (gatewayIP) _driver.setGatewayAddr(gatewayIP);
            if (mac) _driver.setSourceHWAddr(mac);
            else _driver.setSourceHWAddr(imp.getmacaddress(), true);
            if (subnetMask) _driver.setSubnetMask(subnetMask);
            if (sourceIP) _driver.setSourceIP(sourceIP);
            _networkSettings = null;
        } else {
            if (_networkSettings == null) _networkSettings = {};
            _networkSettings.sourceIP <- sourceIP;
            _networkSettings.subnetMask <- subnetMask;
            _networkSettings.gatewayIP <- gatewayIP;
            _networkSettings.mac <- mac;
        }

        return this;
    }

// ***************************************************************************
// openConnection
// Returns: connection instance
// Parameters:
//      ip - the ip address of the destination
//      port - the port of the destination
//      mode - TCP or UDP
//      cb - function to be called when connection successfully
//                     established or a timeout has occurred
// ****************************************************************************
    function openConnection(ip, port, mode = W5500_SOCKET_MODE_TCP, cb = null) {
        if (!_isReady) throw "Wiznet driver not ready";

        if ((mode == null && cb == null) || (typeof mode != "function" && cb == null)) {
            if (cb) cb(W5500_ERR_INVALID_PARAMETERS, null);
            else throw W5500_ERR_INVALID_PARAMETERS;
            return;
        } else if (typeof mode == "function") {
            cb = mode;
            mode = W5500_SOCKET_MODE_TCP;
        }

        // Create a socket, a connection object & a handler
        _driver.openConnection(ip, port, mode, cb);
    }

// ***************************************************************************
// listen
// Returns: this
// Parameters:
//      sourcePort - the port to listen on
//      cb - function to be called when connection successfully established
// ****************************************************************************
    function listen(sourcePort, cb) {
        if (!_isReady) throw "Wiznet driver not ready";

        // Create a socket, a connection object & a handler
        _driver.listen(sourcePort, cb);
    }

// ***************************************************************************
// getNumSockets -
// Returns: returns the total number of sockets available
// Parameters:
//      none
// **************************************************************************
    function getNumSockets() {
        return _driver._noOfSockets;
    }


// ***************************************************************************
// getNumSocketsFree -
// Returns: returns the number of unused sockets
// Parameters:
//      none
// **************************************************************************
    function getNumSocketsFree() {
        return _driver._noOfSockets - _driver._connections.len();
    }


}


// ==============================================================================
// CLASS: W5500.Driver
// ==============================================================================

class W5500.Driver {

    // Chip can support 8 sockets
    static TOTAL_SUPPORTED_SOCKETS = 8;
static MAX_TX_MEM_BUFFER = 16;
static MAX_RX_MEM_BUFFER = 16;

// CLASS VARIABLES
_interruptPin = null;
_spi = null;
_cs = null;
_resetPin = null;
_setMac = null;

_connections = null; // open connections
_availableSockets = null; // array of sockets available
_maxNoOfSockets = 0; // max number of sockets wiznet may use
_noOfSockets = 0; // number of sockets
_socketMemory = null; // amount of memeory each socket has

// ***************************************************************************
// Constructor
// Returns: null
// Parameters:
//      interruptPin - interrupt pin
//      spi - configured spi, W5500 supports spi mode 0 or 3
//      cs(optional) - configured chip select pin
//      reset(optional) - configured reset pin
//      setMac(optional) - set the MAC address of the chip to the imp's own
//                         MAC, with the last bit flipped.
// ***************************************************************************
constructor(interruptPin, spi, cs = null, resetPin = null, setMac = true) {

_interruptPin = interruptPin;
_spi = spi;
_cs = cs;
_resetPin = resetPin;
_setMac = setMac;
_connections = {};
_availableSockets = [];

// Check the pins
if (resetPin) _resetPin.configure(DIGITAL_OUT, 1);

// TODO: When imp.info() is available, change this code.
local imp005 = ("spi0" in hardware);
if (_cs != null) {
_cs.configure(DIGITAL_OUT, 1);
} else if (!imp005) {
throw "You must pass in a chip select pin."
}

// Reset the hardware
reset();

}


// ***************************************************************************
// reset, note this is blocking for 0.2s
// Returns: this
// Parameters:
//          sw(optional) - boolean if true forces a software reset
//          Note: datasheet for W5500 states that software reset is
//                unreliable - don't use
// **************************************************************************
function reset(sw = false, cb = null) {

// Wrangle the parameters
if (typeof sw == "function") {
cb = sw;
sw = false;
}

// Stop the interrupt pin from firing
_interruptPin.configure(DIGITAL_IN);

// Clear the connections and sockets
_connections = {};
_availableSockets = [];

// Force disconnect/close all ports
forceCloseAllSockets();

if (cb) {
// Asynchronouse reset
if (sw || _resetPin == null) {
setMode(W5500_SW_RESET);
// Allow things to settle down again
imp.wakeup(1.0, function() {
// Reset the default memory allocation
_setMemDefaults();
cb();
}.bindenv(this));
} else {
// hold at least 500us after assert low
_resetPin.write(0);
imp.wakeup(0.3, function() {
_resetPin.write(1);
// Allow things to settle down again
imp.wakeup(1.0, function() {
// Reset the default memory allocation
_setMemDefaults();
cb();
}.bindenv(this));
}.bindenv(this));
}
} else {
// Synchronous reset
if (sw || _resetPin == null) {
setMode(W5500_SW_RESET);
// Allow things to settle down again
imp.sleep(1.0);
// Reset the default memory allocation
_setMemDefaults();
} else {
// hold at least 500us after assert low
_resetPin.write(0);
imp.sleep(0.3);
_resetPin.write(1);
// Allow things to settle down again
imp.sleep(1.0);
// Reset the default memory allocation
_setMemDefaults();
}

}

return this;
}



// ***************************************************************************
// init(cb) - initialises the device to basic defaults ready for use after boot or reset
// Returns: nothing
// Parameters:
//        cb - callback function
//
// ***************************************************************************
function init(cb) {

// Configure interrupts
setInterrupt(W5500_CONFLICT_INT_TYPE);
clearInterrupt();
clearSocketInterrupts();
_interruptPin.configure(DIGITAL_IN_PULLUP, handleInterrupt.bindenv(this));

// Set the default mac address
if (_setMac){
setSourceHWAddr(imp.getmacaddress(), true);
}

// Set the defaults
setNumberOfAvailableSockets(TOTAL_SUPPORTED_SOCKETS);

// Done
imp.wakeup(0, cb);

}


// ***************************************************************************
// forceCloseAllSockets, note this is blocking for 1.1s+
// Returns: this
// Parameters:
//          none
// **************************************************************************
function forceCloseAllSockets() {
_maxNoOfSockets = getTotalSupportedSockets();
for (local socket = 0; socket < _maxNoOfSockets; socket++) {
sendSocketCommand(socket, W5500_SOCKET_DISCONNECT);
}
imp.sleep(1);
for (local socket = 0; socket < _maxNoOfSockets; socket++) {
sendSocketCommand(socket, W5500_SOCKET_CLOSE);
}
imp.sleep(0.1);
}


// ***************************************************************************
// getPhysicalLinkStatus
// Returns: true or false
// Parameters:
//      none
// ***************************************************************************
function getPhysicalLinkStatus() {
local status = readReg(W5500_PHYSICAL_CONFIG, W5500_COMMON_REGISTER);
return (status & 0x01) == 0x01;
}


// ***************************************************************************
// openConnection
// Returns: this
// Parameters:
//      destIP - the ip address of the destination
//      destPort - the port of the destination
//      mode - TCP or UDP
//      sourcePort(optional) - the port to send from
//      cb - function to be called when connection successfully established
// ****************************************************************************
function openConnection(destIP, destPort, mode, sourcePort = null, cb = null) {

// shuffle oarameters around
if (typeof sourcePort == "function") {
cb = sourcePort;
sourcePort = null;
}

// check for available socket
if (_availableSockets.len() == 0) {
if (cb) return cb(W5500_ERR_CANNOT_CONNECT_SOCKETS_IN_USE, null);
else throw W5500_ERR_CANNOT_CONNECT_SOCKETS_IN_USE;
}

// Create the connection object and assign it to a spare socket
local socket = _availableSockets.pop();
_connections[socket] <- W5500.Connection(this, socket, destIP, destPort, mode);
if (typeof sourcePort == "integer") {
_connections[socket].setSourcePort(sourcePort);
}
_connections[socket].open(cb);

}


// ***************************************************************************
// listen
// Returns: this
// Parameters:
//      sourcePort - the port to listen on
//      cb - function to be called when connection successfully established
// ****************************************************************************
function listen(sourcePort, cb) {

// check for available socket
if (_availableSockets.len() == 0) {
return cb(W5500_ERR_CANNOT_CONNECT_SOCKETS_IN_USE, null);
}

local socket = _availableSockets.pop();

// Create the connection object and assign it to a spare socket
_connections[socket] <- W5500.Connection(this, socket, null, null, W5500_SOCKET_MODE_TCP);
_connections[socket].setSourcePort(sourcePort);
_connections[socket].listen(cb);

}


// ***************************************************************************
// closeConnection
// Returns: this
// Parameters:
//      socket              - the socket of the connection to close
//      cb(optional)        - function
// ***************************************************************************
function closeConnection(socket, cb = null) {

if (getSocketStatus(socket) == W5500_SOCKET_STATUS_ESTABLISHED) {
sendSocketCommand(socket, W5500_SOCKET_DISCONNECT);
} else {
sendSocketCommand(socket, W5500_SOCKET_CLOSE);
}

local started = hardware.millis();
waitForSocketStatus(socket, W5500_SOCKET_STATUS_CLOSED, function(success) {

// Wait a second from the start
local waitfor = 1000 - (hardware.millis() - started);
if (waitfor < 0) waitfor = 0;

imp.wakeup(waitfor / 1000.0, function() {

deregisterSocket(socket);
if (cb) cb();

}.bindenv(this));

}.bindenv(this));

return this;
}


// ***************************************************************************
// deregisterSocket
// Returns:
// Parameters:
//      socket - the socket of the connection to deregister
// ***************************************************************************
function deregisterSocket(socket) {
// Return this to the available pool
if (_availableSockets.find(socket) == null) {
_availableSockets.push(socket);
}
if (socket in _connections) {
_connections.rawdelete(socket);
}
}



// GETTERS AND SETTERS
// ---------------------------------------------

// ***************************************************************************
// setMemory
// Returns: this
// Parameters:
//      memory - an array of four integers with the desired transmit memory
//               allotment for each socket (supported values are 0, 1, 2, 4, 8, 16)
//      dir - a string containing the transmition direction, accepted values
//            are "tx" or "rx"
// ***************************************************************************
function setMemory(memory, dir) {
local memoryBufferSize = 16;
local addr = (dir == "rx") ? W5500_SOCKET_RX_BUFFER_SIZE : W5500_SOCKET_TX_BUFFER_SIZE;

local bits = 0x00;
local total = 0;

foreach (socket, mem_size in memory) {
local socket_mem = 0x00;
local bytes = 0;

// adjust memory size if total memory is used up
if (total + mem_size > memoryBufferSize) {
mem_size = memoryBufferSize - total;
if (mem_size < 0) mem_size = 0;
}

if (mem_size == memoryBufferSize) {
bytes = 16384;
} else if (mem_size >= 8) {
mem_size = 8;
bytes = 8192;
} else if (mem_size >= 4) {
mem_size = 4;
bytes = 4096;
} else if (mem_size >= 2) {
mem_size = 2;
bytes = 2048;
} else if (mem_size >= 1) {
mem_size = 1;
bytes = 1024;
} else {
mem_size = 0;
bytes = 0;
}

total += mem_size;
_socketMemory[dir][socket] = bytes;
local bsb = _getSocketRegBlockSelectBit(socket);
writeReg(addr, bsb, mem_size);
}
return this;
}


// ***************************************************************************
// getMemory
// Returns: the buffer size
// Parameters:
//      dir - a string containing the transmition direction, accepted values
//            are "tx" or "rx"
// ***************************************************************************
function getMemory(dir, socket) {
return _socketMemory[dir][socket];
}

// ***************************************************************************
// setMode
// Returns: this
// Parameters:
//      mode - select mode using MODE constants or-ed together
// ***************************************************************************
function setMode(mode) {
writeReg(W5500_MODE, W5500_COMMON_REGISTER, mode);
return this;
}

// ***************************************************************************
// setRetries
// Returns: this
// Parameters:
//      time - amount of time (in ms) for each attempt (default: 2000ms)
//      count - the number of retry attempts (default: 5x)
// ***************************************************************************
function setRetries(time = 3000, count = 5) {
local time_us = time * 1000 / 100;
writeReg(W5500_RETRY_TIME_0, W5500_COMMON_REGISTER, ((time_us & 0xFF00) >> 8));
writeReg(W5500_RETRY_TIME_1, W5500_COMMON_REGISTER, (time_us & 0x00FF));
writeReg(W5500_RETRY_COUNT, W5500_COMMON_REGISTER, count);
return this;
}

// ***************************************************************************
// setSocketMode
// Returns: this
// Parameters:
//      socket - select the socket using an integer 0-3
//      mode - select mode using W5500_SOCKET_MODE constant
// ***************************************************************************
function setSocketMode(socket, mode) {
local bsb = _getSocketRegBlockSelectBit(socket)
writeReg(W5500_SOCKET_MODE, bsb, mode);
return this;
}

// ***************************************************************************
// setGatewayAddr
// Returns: this
// Parameters:
//      addr - an array of four integers with the gateway IP address
// ***************************************************************************
function setGatewayAddr(addr) {
local addr = _addrToIP(addr);

// Check that the address is actually different
local old_addr = getGatewayAddr();
if (_areObjectsEqual(addr, old_addr)) return;

writeReg(W5500_GATEWAY_ADDR_0, W5500_COMMON_REGISTER, addr[0]);
writeReg(W5500_GATEWAY_ADDR_1, W5500_COMMON_REGISTER, addr[1]);
writeReg(W5500_GATEWAY_ADDR_2, W5500_COMMON_REGISTER, addr[2]);
writeReg(W5500_GATEWAY_ADDR_3, W5500_COMMON_REGISTER, addr[3]);
return this;
}

// ***************************************************************************
// getGatewayAddr
// Returns:  an array of four integers with the gateway IP address
// Parameters:
//        none
// **************************************************************************
function getGatewayAddr() {
local addr = array(4);
// server.log((hardware.millis() - started) + ": DBG setGatewayAddr: " + pformat(addr));
addr[0] = readReg(W5500_GATEWAY_ADDR_0, W5500_COMMON_REGISTER);
addr[1] = readReg(W5500_GATEWAY_ADDR_1, W5500_COMMON_REGISTER);
addr[2] = readReg(W5500_GATEWAY_ADDR_2, W5500_COMMON_REGISTER);
addr[3] = readReg(W5500_GATEWAY_ADDR_3, W5500_COMMON_REGISTER);
return addr;
}

// ***************************************************************************
// setSubnetMask
// Returns: this
// Parameters:
//      addr - an array of four integers with the subnet mask
// **************************************************************************
function setSubnetMask(addr) {
local addr = _addrToIP(addr);

// Check that the address is actually different
local old_addr = getSubnetMask();
if (_areObjectsEqual(addr, old_addr)) return;

writeReg(W5500_SUBNET_MASK_ADDR_0, W5500_COMMON_REGISTER, addr[0]);
writeReg(W5500_SUBNET_MASK_ADDR_1, W5500_COMMON_REGISTER, addr[1]);
writeReg(W5500_SUBNET_MASK_ADDR_2, W5500_COMMON_REGISTER, addr[2]);
writeReg(W5500_SUBNET_MASK_ADDR_3, W5500_COMMON_REGISTER, addr[3]);
return this;
}

// ***************************************************************************
// getSubnet
// Returns: an array of four integers with the subnet address
// Parameters:
//         none
// **************************************************************************
function getSubnetMask() {
local addr = array(4);
// server.log((hardware.millis() - started) + ": DBG setSubnet: " + pformat(addr));
addr[0] = readReg(W5500_SUBNET_MASK_ADDR_0, W5500_COMMON_REGISTER);
addr[1] = readReg(W5500_SUBNET_MASK_ADDR_1, W5500_COMMON_REGISTER);
addr[2] = readReg(W5500_SUBNET_MASK_ADDR_2, W5500_COMMON_REGISTER);
addr[3] = readReg(W5500_SUBNET_MASK_ADDR_3, W5500_COMMON_REGISTER);
return addr;
}

// ***************************************************************************
// setSourceHWAddr
// Returns: this
// Parameters:
//      addr - an array of 6 integers with the mac address for the
//             source hardware
// **************************************************************************
function setSourceHWAddr(addr, flip_public_bit = false) {

if (typeof addr == "string") {
// Convert from a string
addr = _addrToMac(addr);
}

// Flip the public bit if required
if (flip_public_bit) {
addr[0] = addr[0] | 0x02;
}

// Check that the address is actually different
local old_addr = getSourceHWAddr();
if (_areObjectsEqual(addr, old_addr)) return;

writeReg(W5500_SOURCE_HW_ADDR_0, W5500_COMMON_REGISTER, addr[0]);
writeReg(W5500_SOURCE_HW_ADDR_1, W5500_COMMON_REGISTER, addr[1]);
writeReg(W5500_SOURCE_HW_ADDR_2, W5500_COMMON_REGISTER, addr[2]);
writeReg(W5500_SOURCE_HW_ADDR_3, W5500_COMMON_REGISTER, addr[3]);
writeReg(W5500_SOURCE_HW_ADDR_4, W5500_COMMON_REGISTER, addr[4]);
writeReg(W5500_SOURCE_HW_ADDR_5, W5500_COMMON_REGISTER, addr[5]);
return this;
}

// ***************************************************************************
// getSourceHWAddr
// Returns: addr - an array of 6 integers with the mac address for the
//             source hardware
// Parameters:
//      none
// **************************************************************************
function getSourceHWAddr() {
// server.log((hardware.millis() - started) + ": DBG setSourceHWAddr: " + pformat(addr));
local addr = array(6);
addr[0] = readReg(W5500_SOURCE_HW_ADDR_0, W5500_COMMON_REGISTER);
addr[1] = readReg(W5500_SOURCE_HW_ADDR_1, W5500_COMMON_REGISTER);
addr[2] = readReg(W5500_SOURCE_HW_ADDR_2, W5500_COMMON_REGISTER);
addr[3] = readReg(W5500_SOURCE_HW_ADDR_3, W5500_COMMON_REGISTER);
addr[4] = readReg(W5500_SOURCE_HW_ADDR_4, W5500_COMMON_REGISTER);
addr[5] = readReg(W5500_SOURCE_HW_ADDR_5, W5500_COMMON_REGISTER);

return addr;
}


// ***************************************************************************
// setDestHWAddr
// Returns: this
// Parameters:
//      socket - select the socket using an integer 0-3
//      addr - an array of 6 integers with the mac address for the
//             destination hardware
// **************************************************************************
function setDestHWAddr(socket, addr) {
local bsb = _getSocketRegBlockSelectBit(socket);
writeReg(W5500_SOCKET_DEST_HW_ADDR_0, bsb, addr[0]);
writeReg(W5500_SOCKET_DEST_HW_ADDR_1, bsb, addr[1]);
writeReg(W5500_SOCKET_DEST_HW_ADDR_2, bsb, addr[2]);
writeReg(W5500_SOCKET_DEST_HW_ADDR_3, bsb, addr[3]);
writeReg(W5500_SOCKET_DEST_HW_ADDR_4, bsb, addr[4]);
writeReg(W5500_SOCKET_DEST_HW_ADDR_5, bsb, addr[5]);
return this;
}

// ***************************************************************************
// setSourceIP
// Returns: this
// Parameters:
//      addr - an array of 4 integers with the IP address for the
//             source hardware
// **************************************************************************
function setSourceIP(addr) {
local addr = _addrToIP(addr);

// Check that the address is actually different
local old_addr = getSourceIP();
if (_areObjectsEqual(addr, old_addr)) return;

writeReg(W5500_SOURCE_IP_ADDR_0, W5500_COMMON_REGISTER, addr[0]);
writeReg(W5500_SOURCE_IP_ADDR_1, W5500_COMMON_REGISTER, addr[1]);
writeReg(W5500_SOURCE_IP_ADDR_2, W5500_COMMON_REGISTER, addr[2]);
writeReg(W5500_SOURCE_IP_ADDR_3, W5500_COMMON_REGISTER, addr[3]);
return this;
}

// ***************************************************************************
// getSourceIP
// Returns: addr - an array of 4 integers with the IP address for the
//                 source hardware
// Parameters:
//          none
// **************************************************************************
function getSourceIP() {
local addr = array(4);
addr[0] = readReg(W5500_SOURCE_IP_ADDR_0, W5500_COMMON_REGISTER);
addr[1] = readReg(W5500_SOURCE_IP_ADDR_1, W5500_COMMON_REGISTER);
addr[2] = readReg(W5500_SOURCE_IP_ADDR_2, W5500_COMMON_REGISTER);
addr[3] = readReg(W5500_SOURCE_IP_ADDR_3, W5500_COMMON_REGISTER);
return addr;
}

// ***************************************************************************
// setSourcePort
// Returns: this
// Parameters:
//      socket - select the socket using an integer 0-3
//      addr   - an array of 2 integers with the port for the
//               source hardware (ex: for port 4242, addr = [0x10, 0x92])
// **************************************************************************
function setSourcePort(socket, port) {
// Convert the type
if (typeof port == "integer") {
local newport = array(2, 0);
newport[0] = (port & 0xFF00) >> 8;
newport[1] = (port & 0x00FF);
port = newport;
}

local bsb = _getSocketRegBlockSelectBit(socket);
writeReg(W5500_SOCKET_SOURCE_PORT_0, bsb, port[0]);
writeReg(W5500_SOCKET_SOURCE_PORT_1, bsb, port[1]);
return this;
}

// ***************************************************************************
// getDestIP
// Returns: addr
// Parameters:
//      socket - select the socket using an integer 0-3
// **************************************************************************
function getDestIP(socket) {

local bsb = _getSocketRegBlockSelectBit(socket);
local addr = array(4);
addr[0] = readReg(W5500_SOCKET_DEST_IP_ADDR_0, bsb);
addr[1] = readReg(W5500_SOCKET_DEST_IP_ADDR_1, bsb);
addr[2] = readReg(W5500_SOCKET_DEST_IP_ADDR_2, bsb);
addr[3] = readReg(W5500_SOCKET_DEST_IP_ADDR_3, bsb);
return addr;

}

// ***************************************************************************
// setDestIP
// Returns: this
// Parameters:
//      socket - select the socket using an integer 0-3
//      addr - an array of 4 integers with the IP address for the
//             destination hardware
// **************************************************************************
function setDestIP(socket, addr) {

// Convert the type
local addr = _addrToIP(addr);
local bsb = _getSocketRegBlockSelectBit(socket);
writeReg(W5500_SOCKET_DEST_IP_ADDR_0, bsb, addr[0]);
writeReg(W5500_SOCKET_DEST_IP_ADDR_1, bsb, addr[1]);
writeReg(W5500_SOCKET_DEST_IP_ADDR_2, bsb, addr[2]);
writeReg(W5500_SOCKET_DEST_IP_ADDR_3, bsb, addr[3]);

return this;
}

// ***************************************************************************
// getDestPort
// Returns: integer port number
// Parameters:
//      socket - select the socket using an integer 0-3
// **************************************************************************
function getDestPort(socket) {

local bsb = _getSocketRegBlockSelectBit(socket);
local port_hi = readReg(W5500_SOCKET_DEST_PORT_0, bsb) << 8;
local port_lo = readReg(W5500_SOCKET_DEST_PORT_1, bsb);
return port_hi + port_lo;
}

// ***************************************************************************
// setDestPort
// Returns: this
// Parameters:
//      socket - select the socket using an integer 0-3
//      addr - an array of 2 integers with the port for the
//             destination hardware
//             (ex: for port 4242, addr = [0x10, 0x92])
// **************************************************************************
function setDestPort(socket, port) {

// Convert the type
if (typeof port == "integer") {
local newport = array(2, 0);
newport[0] = (port & 0xFF00) >> 8;
newport[1] = (port & 0x00FF);
port = newport;
}

local bsb = _getSocketRegBlockSelectBit(socket);
writeReg(W5500_SOCKET_DEST_PORT_0, bsb, port[0]);
writeReg(W5500_SOCKET_DEST_PORT_1, bsb, port[1]);
return this;
}

// ***************************************************************************
// setRxReadPointer
// Returns: this
// Parameters:
//      socket - select the socket using an integer 0-3
//      value - new RX read pointer value
// **************************************************************************
function setRxReadPointer(socket, value) {
local rx_pointer_r1 = (value & 0xFF00) >> 8;
local rx_pointer_r2 = value & 0x00FF;
local bsb = _getSocketRegBlockSelectBit(socket);
writeReg(W5500_SOCKET_RX_RP_R1, bsb, rx_pointer_r1);
writeReg(W5500_SOCKET_RX_RP_R2, bsb, rx_pointer_r2);
return this;
}

// ***************************************************************************
// setTxWritePointer
// Returns: this
// Parameters:
//      socket - select the socket using an integer 0-3
//      value - new TX write pointer value
// **************************************************************************
function setTxWritePointer(socket, value) {
local tx_pointer_r1 = (value & 0xFF00) >> 8;
local tx_pointer_r2 = value & 0x00FF;
local bsb = _getSocketRegBlockSelectBit(socket);
writeReg(W5500_SOCKET_TX_WP_R1, bsb, tx_pointer_r1);
writeReg(W5500_SOCKET_TX_WP_R2, bsb, tx_pointer_r2);
return this;
}

// ***************************************************************************
// getRxReadPointer
// Returns: value of read pointer
// Parameters:
//      socket - select the socket using an integer 0-3
// **************************************************************************
function getRxReadPointer(socket) {
local bsb = _getSocketRegBlockSelectBit(socket);
local rx_pointer_pt1 = readReg(W5500_SOCKET_RX_RP_R1, bsb) << 8;
local rx_pointer_pt2 = readReg(W5500_SOCKET_RX_RP_R2, bsb);

return rx_pointer_pt1 | rx_pointer_pt2;
}

// ***************************************************************************
// getTxReadPointer
// Returns: value of read pointer
// Parameters:
//      socket - select the socket using an integer 0-3
// **************************************************************************
function getTxReadPointer(socket) {
local bsb = _getSocketRegBlockSelectBit(socket);
local tx_pointer_pt1 = readReg(W5500_SOCKET_TX_RP_R1, bsb) << 8;
local tx_pointer_pt2 = readReg(W5500_SOCKET_TX_RP_R2, bsb);

return tx_pointer_pt1 | tx_pointer_pt2;
}

// ***************************************************************************
// getRxDataSize
// Returns: value of received data size register
// Parameters:
//      socket - select the socket using an integer 0-3
// **************************************************************************
function getRxDataSize(socket) {
local bsb = _getSocketRegBlockSelectBit(socket);
local rx_pt1 = readReg(W5500_SOCKET_RX_SIZE_R1, bsb) << 8;
local rx_pt2 = readReg(W5500_SOCKET_RX_SIZE_R2, bsb);

return rx_pt1 | rx_pt2;
}

// ***************************************************************************
// getFreeTxDataSize
// Returns: value of TX data free size register
// Parameters:
//      socket - select the socket using an integer 0-3
// **************************************************************************
function getFreeTxDataSize(socket) {
local bsb = _getSocketRegBlockSelectBit(socket);
local tx_fd_pt1 = readReg(W5500_SOCKET_TX_SIZE_R1, bsb) << 8;
local tx_fd_pt2 = readReg(W5500_SOCKET_TX_SIZE_R2, bsb);

return tx_fd_pt1 | tx_fd_pt2;
}

// ***************************************************************************
// getSocketRXBufferSize
// Returns: value of RX data buffer size register
// Parameters:
//      socket - select the socket using an integer 0-3
// **************************************************************************
function getSocketRXBufferSize(socket) {
local bsb = _getSocketRegBlockSelectBit(socket);
return readReg(W5500_SOCKET_RX_BUFFER_SIZE, bsb);
}

// ***************************************************************************
// getSocketTXBufferSize
// Returns: value of TX data buffer size register
// Parameters:
//      socket - select the socket using an integer 0-3
// **************************************************************************
function getSocketTXBufferSize(socket) {
local bsb = _getSocketRegBlockSelectBit(socket);
return readReg(W5500_SOCKET_TX_BUFFER_SIZE, bsb);
}

// ***************************************************************************
// getChipVersion
// Returns: chip version (should be 0x04 for the W5500)
// Parameters: none
// **************************************************************************
function getChipVersion() {
return readReg(W5500_CHIP_VERSION, W5500_COMMON_REGISTER);
}

// ***************************************************************************
// getTotalSupportedSockets
// Returns: number of sockets the driver code currently supports
// Parameters: none
// **************************************************************************
function getTotalSupportedSockets() {
return TOTAL_SUPPORTED_SOCKETS;
}

// ***************************************************************************
// getMemoryInfo
// Returns: table with memory maximums and supported memory block sizes
// Parameters: none
// **************************************************************************
function getMemoryInfo() {
// mem_block_sizes - an array with supported values highest to lowest
return { "tx_max": MAX_TX_MEM_BUFFER, "rx_max": MAX_RX_MEM_BUFFER, "mem_block_sizes": [16, 8, 4, 2, 1, 0] }
}

// ***************************************************************************
// getSocketStatus
// Returns: integer with the connection status for the socket passed in
// Parameters:
//      socket - select the socket using an integer 0-3
// **************************************************************************
function getSocketStatus(socket) {
local bsb = _getSocketRegBlockSelectBit(socket);
local status = readReg(W5500_SOCKET_STATUS, bsb);
return status;
}

// ***************************************************************************
// waitForSocketStatus
// Returns: nothing
// Parameters:
//      socket - select the socket using an integer 0-3
//      state - the state we are waiting for
//      callback - the callback to execute when the state is reached
//      timeout - the number of seconds after which we will give up
// **************************************************************************
function waitForSocketStatus(socket, state, callback, timeout = 10) {
if (getSocketStatus(socket) == state) {
return callback(true);
} else if (timeout <= 0) {
return callback(false);
} else {
imp.wakeup(0.1, function() {
return waitForSocketStatus(socket, state, callback, timeout - 0.1);
}.bindenv(this))
}
}

// ***************************************************************************
// getSocketMode
// Returns: socketMode
// Parameters:
//      socket - select the socket using an integer 0-3
// **************************************************************************
function getSocketMode(socket) {
local bsb = _getSocketRegBlockSelectBit(socket)
local mode = readReg(W5500_SOCKET_MODE, bsb);
return mode;
}

// ***************************************************************************
// sendSocketCommand
// Returns: this
// Parameters:
//      socket - select the socket using an integer 0-3
//      command - select command using SOCKET_COMMANDS constant
// **************************************************************************
function sendSocketCommand(socket, command) {
local bsb = _getSocketRegBlockSelectBit(socket);
writeReg(W5500_SOCKET_COMMAND, bsb, command);
local started = hardware.millis();
while (readReg(W5500_SOCKET_COMMAND, bsb) != 0x00) {
// Check for timeouts here
if (hardware.millis() - started > (W5500_COMMAND_TIMEOUT * 1000)) throw W5500_ERR_COMMAND_TIMEOUT;
imp.sleep(0.001);
}

return this;
}

// ***************************************************************************
// setNumberOfAvailableSockets - configures interrupts and memory for each connection
// Returns: number of actual connections configured
// Parameters:
//      numSockets - number of desired connections
// **************************************************************************
function setNumberOfAvailableSockets(numSockets = null) {

// limit number of sockets to what driver can support
if (numSockets == null) numSockets = _maxNoOfSockets;
else if (numSockets < 1) numSockets = 1;
else if (numSockets > _maxNoOfSockets) numSockets = _maxNoOfSockets;

// Store this for later
_noOfSockets = numSockets;

// calculate max memory for number of sockets
local memoryInfo = getMemoryInfo();
local tx_mem = _getMaxMemValue(numSockets, memoryInfo.tx_max, memoryInfo.mem_block_sizes);
local rx_mem = _getMaxMemValue(numSockets, memoryInfo.rx_max, memoryInfo.mem_block_sizes);

// Enable interrupt for each connection
// Set max equal memory across sockets
local sockets = [], interrupts = 0, txmem = array(numSockets, 0), rxmem = array(numSockets, 0);
for (local c = 0; c < numSockets; ++c) {
sockets.insert(0, c);
interrupts = interrupts | (0x01 << c);
txmem[c] = tx_mem;
rxmem[c] = rx_mem;
}

setSocketInterrupt(interrupts);
setMemory(txmem, "tx");
setMemory(rxmem, "rx");

return _availableSockets = sockets;
}

// ***************************************************************************
// readRxData
// Returns: data from received data register
// Parameters:
//      socket - select the socket using an integer 0-3
//      maxlen - the maximum length of the buffer to receive
// **************************************************************************
function readRxData(socket, maxlen = null) {

// get the amount of data waiting for attention
local dataWaiting = getRxDataSize(socket);
if (dataWaiting == 0) return blob();
if (maxlen != null && dataWaiting > maxlen) dataWaiting = maxlen;

// get the buffer size to make sure we don't read more than we have space for
local bufferSize = getSocketRXBufferSize(socket);

// Get offset address
local src_ptr = getRxReadPointer(socket);

// use cntl_byte and src_ptr to get addr??
// read transmitted data here
local bsb = _getSocketRXBufferBlockSelectBit(socket);
local data = _readData(src_ptr, bsb, dataWaiting);

// increase Sn_RX_RD as length of len
src_ptr += dataWaiting;
setRxReadPointer(socket, src_ptr);

// set RECV command
sendSocketCommand(socket, W5500_SOCKET_RECEIVE);

return data;
}

// ***************************************************************************
// sendTxData
// Returns: length of data to transmit
// Parameters:
//      socket - select the socket using an integer 0-3
//      transmitData - data to be sent
// **************************************************************************
function sendTxData(socket, transmitData) {

local tx_length = transmitData.len();

// select TX memory
local cntl_byte = getSocketTXBufferSize(socket);
// Get offset address
local dst_ptr = getTxReadPointer(socket); // Sn_TX_RD;
//  get Block select bit for RX buffer
local bsb = _getSocketTXBufferBlockSelectBit(socket);

// write transmit data
_writeData(dst_ptr, bsb, transmitData);

// increase Sn_TX_WR as length of send_size
dst_ptr += tx_length; // adjust write pointer get first??
setTxWritePointer(socket, dst_ptr);

// set SEND command
sendSocketCommand(socket, W5500_SOCKET_SEND);

return tx_length;
}


// INTERRUPT FUNCTIONS
// ---------------------------------------------

// ***************************************************************************
// setInterrupt
// Returns: this
// Parameters:
//      type - select interrupt type using interrupt type
//             constants or-ed together
// **************************************************************************
function setInterrupt(type) {
writeReg(W5500_INTERRUPT_MASK, W5500_COMMON_REGISTER, type);
return this;
}

// ***************************************************************************
// clearInterrupt
// Returns: this
// Parameters:
//      type(optional) - clear specified interrupt type
//                               if nothing passed in clears all interrupts
// **************************************************************************
function clearInterrupt(type = 0xF0) {
writeReg(W5500_INTERRUPT, W5500_COMMON_REGISTER, type);
return this;
}

// ***************************************************************************
// setSocketInterrupt
// Returns: this
// Parameters:
//      socketInt - select the sockets to enable interrupts on using
//                  SOCKET INTERRUPTS constants or-ed together
//      type(optional) - select SOCKET INTERRUPT TYPES using constants
//              or-ed together, if type not passed in all type interrupts
//              will be cleared.
// **************************************************************************
function setSocketInterrupt(socketInt, type = W5500_ALL_INT_TYPES) {
writeReg(W5500_SOCKET_INTERRUPT_MASK, W5500_COMMON_REGISTER, socketInt);
// default enables all socket interrupt types
writeReg(W5500_SOCKET_N_INTERRUPT_MASK, W5500_COMMON_REGISTER, type);
return this;
}

// ***************************************************************************
// clearSocketInterrupt
// Returns: this
// Parameters:
//      socket : socket (selected by integer) to clear interrupt on
//      type(optional) - clear specified interrupt type
//                       if nothing passed in clears all interrupts
// **************************************************************************
function clearSocketInterrupt(socket, type = W5500_ALL_INT_TYPES) {
local bsb = _getSocketRegBlockSelectBit(socket);
writeReg(W5500_SOCKET_N_INTERRUPT, bsb, type);
return this;
}

// ***************************************************************************
// clearSocketInterrupts
// Returns: clears the Sockets Interrupt
// Parameters:
//      none
// **************************************************************************
function clearSocketInterrupts() {
for (local socket = 0; socket < _noOfSockets; socket++) {
clearSocketInterrupt(socket);
}
}


// ***************************************************************************
// handleInterrupt
// Returns: interrupt status table
// Parameters: none
// **************************************************************************
function handleInterrupt() {

// Handle the main interrupt
local status1 = readReg(W5500_INTERRUPT, W5500_COMMON_REGISTER);
local interrupt = {
"CONFLICT": status1 & W5500_CONFLICT_INT_TYPE ? true : false,
"UNREACH": status1 & W5500_UNREACH_INT_TYPE ? true : false,
"PPPoE": status1 & W5500_PPPoE_INT_TYPE ? true : false,
"MAGIC_PACKET": status1 & W5500_MAGIC_PACKET_TYPE ? true : false,
"REGISTER_VALUE": status1
};

if (interrupt.UNREACH) {
clearInterrupt(W5500_UNREACH_INT_TYPE);
}
if (interrupt.CONFLICT) {
clearInterrupt(W5500_CONFLICT_INT_TYPE);
handleConflictInt();
}

// Handle the socket interrupt
local status2 = readReg(W5500_SOCKET_INTERRUPT, W5500_COMMON_REGISTER);
if (status1 == 0x00 && status2 == 0x00) return; // server.error("BOTH INTERRUPTS EMPTY");

// Work out which socket(s) is/are interrupting
for (local socket_n = 0; socket_n < 8; ++socket_n) {
// Is this socket number flagged?
if ((status2 & (0x01 << socket_n)) == 0x00) continue;
// Now find the socket in our connection table
if (socket_n in _connections) {
// server.log(format("INTERRUPT ON SOCKET %d", socket_n));
_connections[socket_n].handleInterrupt(true);
}
}

}


// ***************************************************************************
// _handleConflictInt, logs conflict error message & clears interrupt
// Returns: null
// Parameters: none
// ***************************************************************************
function handleConflictInt() {
// TODO: Throw an error callback for the developer to capture this instead of just logging it
local addr = getSourceIP();
if (_areObjectsEqual(addr, [0, 0, 0, 0])) return;
server.error(format("Conflict with IP address: %d.%d.%d.%d", addr[0], addr[1], addr[2], addr[3]));
}


// ***************************************************************************
// getSocketInterruptTypeStatus
// Returns: socket interrupt status table
// Parameters:
//      socket - select the socket using an integer 0-7
// **************************************************************************
function getSocketInterruptTypeStatus(socket) {
local bsb = _getSocketRegBlockSelectBit(socket);
local status = readReg(W5500_SOCKET_N_INTERRUPT, bsb);
local intStatus = {
"SEND_COMPLETE": status & W5500_SEND_COMPLETE_INT_TYPE ? true : false,
"TIMEOUT": status & W5500_TIMEOUT_INT_TYPE ? true : false,
"DATA_RECEIVED": status & W5500_DATA_RECEIVED_INT_TYPE ? true : false,
"DISCONNECTED": status & W5500_DISCONNECTED_INT_TYPE ? true : false,
"CONNECTED": status & W5500_CONNECTED_INT_TYPE ? true : false,
"REGISTER_VALUE": status
};
return intStatus;
}

// SPI FUNCTIONS
// ---------------------------------------------

// ***************************************************************************
// readReg
// Returns: data stored at the specified register
// Parameters:
//      reg - register to read
// **************************************************************************
function readReg(addr, bsb) {
local b = blob();
local cp = bsb | W5500_READ_COMMAND | W5500_VARIABLE_DATA_LENGTH;
local res = blob();

(_cs) ? _cs.write(0): _spi.chipselect(1);

b.writen(addr >> 8, 'b');
b.writen(addr & 0xFF, 'b');
b.writen(cp, 'b');
b.writen(0x00, 'b');

res = _spi.writeread(b);

(_cs) ? _cs.write(1): _spi.chipselect(0);

return res[3];
}

// ***************************************************************************
// writeReg
// Returns: null
// Parameters:
//      reg - register to write to
//      value - data to write to register
// **************************************************************************
function writeReg(addr, bsb, value) {
local b = blob();
local cp = bsb | W5500_WRITE_COMMAND | W5500_VARIABLE_DATA_LENGTH;

(_cs) ? _cs.write(0): _spi.chipselect(1);

b.writen(addr >> 8, 'b');
b.writen(addr & 0xFF, 'b');
b.writen(cp, 'b');
b.writen(value, 'b');

_spi.write(b);

(_cs) ? _cs.write(1): _spi.chipselect(0);
}



// PRIVATE FUNCTIONS
// ---------------------------------------------

// ***************************************************************************
// _hexToInt
// Returns: an integer representation of the string passed in
// Parameters:
//      hexString - A string representing a hexadecimal number
// **************************************************************************
function _hexToInt(hexString) {
// Does the string start with '0x'? If so, remove it
if (hexString.slice(0, 2) == "0x") hexString = hexString.slice(2);

// Get the integer value of the remaining string
local intValue = 0;

foreach (character in hexString) {
local nibble = character - '0';
if (nibble > 9) nibble = ((nibble & 0x1F) - 7);
intValue = (intValue << 4) + nibble;
}

return intValue;
}

// ***************************************************************************
// _addrToIP
// Returns: an array of four numbers representing an ip address or false if it is a string for the DNS
// Parameters:
//      addr - a string or array representing an IP address
// **************************************************************************
function _addrToIP(addr) {

if (typeof addr == "string" && addr.len() > 0) {

try {
local parts = split(addr, ".");
if (parts.len() == 4) {
foreach (part in parts) {
local ipart = part.tointeger();
if (ipart.tostring() != part || ipart < 0 || ipart >= 256) {
return false;
}
}

return [parts[0].tointeger(), parts[1].tointeger(), parts[2].tointeger(), parts[3].tointeger()];
} else {
return false;
}
} catch (e) {
return false;
}

} else if (typeof addr == "blob" && addr.len() == 4) {
local parts = array();
addr.seek(0, 'b');
while (!addr.eos()) {
parts.push(addr.readn('b'))
}
return parts;
} else if (typeof addr == "array" && addr.len() == 4) {
return addr;
} else {
throw "Bad IP address";
}

}


// ***************************************************************************
// _addrToIP
// Returns: an array of four numbers representing an ip address or false if it is a string for the DNS
// Parameters:
//      addr - a string or array representing an IP address
// **************************************************************************
function _addrToMac(addr) {

if (typeof addr == "string" && addr.len() == 12) {
// Convert a string address to an array
local mac = [];
for (local i = 0; i < addr.len(); i += 2) {
local byte = addr.slice(i, i + 2);
mac.push(_hexToInt(byte));
}
return mac;
} else if (typeof addr == "blob" && addr.len() == 6) {
local parts = array();
addr.seek(0, 'b');
while (!addr.eos()) {
parts.push(addr.readn('b'))
}
return parts;
} else if (typeof addr == "array" && addr.len() == 6) {
return addr;
} else {
throw "Bad Mac address";
}
}


// ***************************************************************************
// _areObjectsEqual
// Returns: true if the two objects are identical. Currently only checks the one node deep
// Parameters:
//      obj1 - the first object
//      obj2 - the second object
// **************************************************************************
static function _areObjectsEqual(obj1, obj2) {
if (typeof obj1 != typeof obj2) return false;
if (typeof obj1 == "array" || typeof obj1 == "table" || typeof obj1 == "blob") {
if (obj1.len() != obj2.len()) return false;
foreach (k, v in obj1) {
if (!(k in obj2)) return false;
if (typeof obj1[k] != typeof obj2[k]) return false;
// NOTE: Should recurse here if not a scalar
if (obj1[k] != obj2[k]) return false;
}
return true;
} else {
return obj1 == obj2;
}
}


// ***************************************************************************
// _getSocketRegBlockSelectBit
// Returns: the socket register Control Phase Block Select Bit for the socket passed in
// Parameters:
//      socket - select the socket using an integer 0-3
// **************************************************************************
function _getSocketRegBlockSelectBit(socket) {
switch (socket) {
case 0:
return W5500_S0_REGISTER;
case 1:
return W5500_S1_REGISTER;
case 2:
return W5500_S2_REGISTER;
case 3:
return W5500_S3_REGISTER;
case 4:
return W5500_S4_REGISTER;
case 5:
return W5500_S5_REGISTER;
case 6:
return W5500_S6_REGISTER;
case 7:
return W5500_S7_REGISTER;
}
return null;
}

// ***************************************************************************
// _getSocketRXBufferBlockSelectBit
// Returns: the socket RX buffer Control Phase Block Select Bit for the socket passed in
// Parameters:
//      socket - select the socket using an integer 0-3
// **************************************************************************
function _getSocketRXBufferBlockSelectBit(socket) {
switch (socket) {
case 0:
return W5500_S0_RX_BUFFER;
case 1:
return W5500_S1_RX_BUFFER;
case 2:
return W5500_S2_RX_BUFFER;
case 3:
return W5500_S3_RX_BUFFER;
case 4:
return W5500_S4_RX_BUFFER;
case 5:
return W5500_S5_RX_BUFFER;
case 6:
return W5500_S6_RX_BUFFER;
case 7:
return W5500_S7_RX_BUFFER;
}
return null;
}

// ***************************************************************************
// _getSocketRXBufferBlockSelectBit
// Returns: the socket TX buffer Control Phase Block Select Bit for the socket passed in
// Parameters:
//      socket - select the socket using an integer 0-3
// **************************************************************************
function _getSocketTXBufferBlockSelectBit(socket) {
switch (socket) {
case 0:
return W5500_S0_TX_BUFFER;
case 1:
return W5500_S1_TX_BUFFER;
case 2:
return W5500_S2_TX_BUFFER;
case 3:
return W5500_S3_TX_BUFFER;
case 4:
return W5500_S4_TX_BUFFER;
case 5:
return W5500_S5_TX_BUFFER;
case 6:
return W5500_S6_TX_BUFFER;
case 7:
return W5500_S7_TX_BUFFER;
}
return null;
}


// ***************************************************************************
// _setMemDefaults, sets locally stored default socket memory info
// Returns: null
// Parameters: none
// **************************************************************************
function _setMemDefaults() {
_socketMemory = {
"rx": [2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048],
"tx": [2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048]
}
}


// ***************************************************************************
// _writeData, writes data to transmit memory buffer
// Returns: size of data written
// Parameters:
//      addr - offset address to start the data write
//      bsb - socket tx buffer block select bit
//      data - data to be written
// **************************************************************************
function _writeData(addr, bsb, data) {
foreach (item in data) {
writeReg(addr, bsb, item);
addr++;
}

return data.len();
}

// ***************************************************************************
// _readData
// Returns: blob containing data from memory buffer
// Parameters:
//      addr - offset address to start the read
//      bsb - block select bit for the socket rx buffer
//      size - size of data to be read
// **************************************************************************
function _readData(addr, bsb, size) {
local b = blob();

while (size != 0) {
b.writen(readReg(addr, bsb), 'b');
addr++;
size--;
}

return b
}

// ***************************************************************************
// _getMaxMemValue
// Returns: max memory value
// Parameters:
//      numValues - number of memory slots
//      max - max memory available
//      blockSizes - array of supported memory sizes
// **************************************************************************
function _getMaxMemValue(numValues, max, blockSizes) {
// make sure memory values are in desending order
blockSizes.sort();
blockSizes.reverse();

local memory = max / numValues;

foreach (value in blockSizes) {
if (memory >= value) {
memory = value;
break;
}
}
return memory;
}

}


// ==============================================================================
// CLASS: W5500.Connection
// ==============================================================================

class W5500.Connection {

_driver = null;
_socket = null;
_state = null;
_handlers = null;
_ip = null;
_port = null;
_mode = null;
_sourcePort = null;
_transmitQueue = null;
_transmitting = false;
_interrupt_timer = null;
_open_timer = null;
_debug = false;

// ***************************************************************************
// Constructor
// Returns: null
// Parameters:
//      driver  - the instance of the W5500.driver class
//      socket  - socket number of the connection (integer)
//      ip      - the ip address of the destination
//      port    - the port of the destination
//      mode    - TCP or UDP
// **************************************************************************
constructor(driver, socket, ip, port, mode) {
_driver = driver;
_socket = socket;
_state = W5500_SOCKET_STATES.CLOSED;
_ip = ip;
_port = port;
_mode = mode;
_handlers = { "receive": null, "connect": null, "close": null, "transmit": null };
_transmitQueue = [];

// Close the socket if it is still open
while (_driver.getSocketStatus(_socket) != W5500_SOCKET_STATUS_CLOSED) {
// NOTE: Worth putting a time limit on this loop
_driver.sendSocketCommand(_socket, W5500_SOCKET_CLOSE);
imp.sleep(0.1);
}

// Clear out any stagnant interrupts
_driver.clearSocketInterrupt(_socket);

}


// ***************************************************************************
// open
// Returns: this
// Parameters:
//      cb - callback to be called when the connection is opened
// **************************************************************************
function open(cb = null) {

// Set the socket mode and open the socket
_driver.setRetries(); // Default: 3000ms, 5x
_driver.setSocketMode(_socket, _mode);
_driver.sendSocketCommand(_socket, W5500_SOCKET_OPEN);

// Set the source port
if (_sourcePort == null) {
// create a random port between 1024-65535
_driver.setSourcePort(_socket, _returnRandomPort(1024, 65535));
} else {
_driver.setSourcePort(_socket, _sourcePort);
}

// Open the socket and setup the connection
_driver.setDestIP(_socket, _ip);
_driver.setDestPort(_socket, _port);
_driver.sendSocketCommand(_socket, W5500_SOCKET_CONNECT);

if (_mode == W5500_SOCKET_MODE_UDP) {
// UDP packet's don't actually connect, so we can call the callback immediately.
_state = W5500_SOCKET_STATES.ESTABLISHED
if (cb) cb(null, this);
} else {
// For all other modes we just prepare for a future connection
_state = W5500_SOCKET_STATES.CONNECTING;
if (cb) _handlers["connect"] <- cb;

// Setup a timeout timer
_open_timer = imp.wakeup(W5500_CONNECT_TIMEOUT, function() {
_open_timer = null;

local _connectionCallback = _getHandler("connect");
if (_connectionCallback) {
_handlers["connect"] <- null;
_connectionCallback(W5500_ERR_CANNOT_CONNECT_TIMEOUT, null);
}

}.bindenv(this))
}


// Start polling the interrupts
_interrupt_timer = imp.wakeup(W5500_INTERRUPT_POLL_TIME_IDLE, handleInterrupt.bindenv(this));

return this;
}


// ***************************************************************************
// listen
// Returns: sets the socket to listen mode
// Parameters:
//      cb - function to called when a connection is established
//
// **************************************************************************
function listen(cb) {

// Set the socket mode and open the socket
_driver.setSocketMode(_socket, W5500_SOCKET_MODE_TCP);
_driver.sendSocketCommand(_socket, W5500_SOCKET_OPEN);
_driver.setSourcePort(_socket, _sourcePort);
_driver.sendSocketCommand(_socket, W5500_SOCKET_LISTEN);

_state = W5500_SOCKET_STATES.LISTENING;

// Start polling the interrupts
_interrupt_timer = imp.wakeup(W5500_INTERRUPT_POLL_TIME_IDLE, handleInterrupt.bindenv(this));

// Setup the callback
_handlers["connect"] <-

function(err, conn) {
if (!err) {
_ip = _driver.getDestIP(_socket);
_port = _driver.getDestPort(_socket);
}
cb(err, conn);
}.bindenv(this);

return this;
}



// ***************************************************************************
// close
// Returns: close connection
// Parameters:
//      none
// **************************************************************************
function close(cb = null) {
if (cb) onClose(cb);
_state = W5500_SOCKET_STATES.DISCONNECTING;
_driver.closeConnection(_socket, _getHandler("close"));
_handlers = {};
if (_interrupt_timer) imp.cancelwakeup(_interrupt_timer);
}


// ***************************************************************************
// getIP
// Returns: the destination ip address
// Parameters:
//      none
// **************************************************************************
function getIP() {
return _ip;
}

// ***************************************************************************
// getPort
// Returns: the destination port
// Parameters:
//      none
// **************************************************************************
function getPort() {
return _port;
}

// ***************************************************************************
// onDisconnect
// Returns: this
// Parameters:
//      cb - function to called when disconnect interrupt triggered
// **************************************************************************
function onDisconnect(cb) {
_handlers["disconnect"] <- cb;
return this;
}

// ***************************************************************************
// onReceive
// Returns: this
// Parameters:
//      cb - function to called when data received interrupt triggered
// **************************************************************************
function onReceive(cb) {
_handlers["receive"] <- cb;
receive();
return this;
}

// ***************************************************************************
// onClose
// Returns: this
// Parameters:
//      cb - function to called when socket is deregistered
// **************************************************************************
function onClose(cb) {
_handlers["close"] <- cb;
return this;
}

// ***************************************************************************
// setSourcePort
// Returns: this
// Parameters:
//      port - source port
// **************************************************************************
function setSourcePort(port) {
_sourcePort = port;
return this;
}

// ***************************************************************************
// _getHandler
// Returns: handler function or null
// Parameters:
//      handlerName - name of handler
// **************************************************************************
function _getHandler(handlerName) {
return (handlerName in _handlers) ? _handlers[handlerName] : null;
}


// ***************************************************************************
// getSocket
// Returns: the socket id
// Parameters: none
// **************************************************************************
function getSocket() {
return _socket;
}


// ***************************************************************************
// dataWaiting
// Returns: boolean
// Parameters:
//      socket - connection socket
// **************************************************************************
function dataWaiting() {
return (_driver.getRxDataSize(_socket) == 0x00) ? false : true;
}

// ***************************************************************************
// isEstablished
// Returns: boolean
// Parameters:
//      none
// **************************************************************************
function isEstablished() {
return (_driver.getSocketStatus(_socket) == W5500_SOCKET_STATUS_ESTABLISHED) ? true : false;
}

// ***************************************************************************
// _handleSocketInt, handles/clears the specified socket interrupt
// Returns: null
// Parameters: socket the interrupt occurred on
// **************************************************************************
function handleInterrupt(skip_timer = false) {

local status = _driver.getSocketInterruptTypeStatus(_socket);

if (status.CONNECTED) {

// server.log("Connection established on socket " + _socket);
_driver.clearSocketInterrupt(_socket, W5500_CONNECTED_INT_TYPE);
_state = W5500_SOCKET_STATES.ESTABLISHED;

if (_open_timer) imp.cancelwakeup(_open_timer);
_open_timer = null;

skip_timer = true;

local _connectionCallback = _getHandler("connect");
if (_connectionCallback) {
_handlers["connect"] <- null;

imp.wakeup(0, function() {
_connectionCallback(null, this);
}.bindenv(this))
}
}

if (status.TIMEOUT) {

// server.log("Timeout occurred on socket " + _socket);
_driver.clearSocketInterrupt(_socket, W5500_TIMEOUT_INT_TYPE);
skip_timer = true;

if (_state == W5500_SOCKET_STATES.CONNECTING) {

if (_open_timer) imp.cancelwakeup(_open_timer);
_open_timer = null;

local _connectionCallback = _getHandler("connect");
if (_connectionCallback) {
_handlers["connect"] <- null;

imp.wakeup(0, function() {
_connectionCallback(W5500_ERR_CANNOT_CONNECT_TIMEOUT, null);
}.bindenv(this))
}

// Close this socket
close();

} else {

local _timeoutCallback = _getHandler("timeout");
if (_timeoutCallback) {
_handlers["timeout"]  <- null;
_handlers["transmit"] <- null; // clean the transmit callback as it timed out

imp.wakeup(0, function() {
_timeoutCallback(W5500_ERR_TRANSMIT_TIMEOUT);
}.bindenv(this))
} else {
// server.error("No timeout handler")
}
}
}

if (status.SEND_COMPLETE) {

// server.log("Send complete on socket " + _socket);
_driver.clearSocketInterrupt(_socket, W5500_SEND_COMPLETE_INT_TYPE);
skip_timer = true;

// call transmitting callback
local _transmitCallback = _getHandler("transmit");
if (_transmitCallback) {
_handlers["transmit"] <- null;

imp.wakeup(0, function() {
_transmitCallback(null);
}.bindenv(this))
}
}

if (status.DATA_RECEIVED) {

// server.log("Data received on socket " + _socket);
_driver.clearSocketInterrupt(_socket, W5500_DATA_RECEIVED_INT_TYPE);
receive(); // process incoming data

}

if (status.DISCONNECTED) {

// server.log("Connection disconnected on socket " + _socket);
_driver.clearSocketInterrupt(_socket, W5500_DISCONNECTED_INT_TYPE);
skip_timer = true;

if (_open_timer) imp.cancelwakeup(_open_timer);
_open_timer = null;

if (_state == W5500_SOCKET_STATES.CONNECTING) {

// call connected (failed) callback
local _connectionCallback = _getHandler("connect");
if (_connectionCallback) {
_handlers["connect"] <- null;

imp.wakeup(0, function() {
_connectionCallback("failed", null);
}.bindenv(this))
}

} else {

// call disconnected callback
local _disconnectCallback = _getHandler("disconnect");
if (_disconnectCallback) {
imp.wakeup(0, function() {
_disconnectCallback(null);
}.bindenv(this));
}

}

// Close the socket and remove interrupts
close();

}

// Scan the interrupt again very soon
if (_interrupt_timer) imp.cancelwakeup(_interrupt_timer);
if (_socket in _driver._connections) {
local timer_time = ("receive" in _handlers) ? W5500_INTERRUPT_POLL_TIME_ACTIVE : W5500_INTERRUPT_POLL_TIME_IDLE;
_interrupt_timer = imp.wakeup(timer_time, handleInterrupt.bindenv(this))
}

return status;

}



// ***************************************************************************
// transmit
// Returns: this
// Parameters:
//      transmitData - blob/string of data to transmit
//      cb(optional) - function to be called when data successfully
//                      sent or timeout has occurred
// **************************************************************************
function transmit(transmitData, cb = null) {

local _transmitNextInQueue, _transmit;

// Function to perform the transmission of one data packet broken into chunks
_transmit = function(transmitData, _cb = null) {

local __cb, transmit_timer, _transmitNextChunk;

// Prepare for a timeout
transmit_timer = imp.wakeup(W5500_TRANSMIT_TIMEOUT, function() {
__cb(W5500_ERR_TRANSMIT_TIMEOUT)
}.bindenv(this));

// Prepare a dummy callback to clear the timer before calling the real callback
__cb = function(err = null) {
if (transmit_timer) {
imp.cancelwakeup(transmit_timer);
transmit_timer = null;
}
if (_cb) {
imp.wakeup(0, function() {
_cb(err);
}.bindenv(this));
}
}.bindenv(this);


// Chunking data that is greater than buffer size. check transmission data size.
// If larger than socket transmit bufffer, break data into chunks before sending.
local txBufferSize = _driver.getMemory("tx", _socket);
local tx_length = transmitData.len();
local chunks = [];

if (tx_length > txBufferSize) {
local startPointer = 0;
local endPointer = txBufferSize;

// Loop over data and create chunks
while (endPointer < tx_length) {

chunks.push(transmitData.slice(startPointer, endPointer));
startPointer = endPointer;
endPointer += txBufferSize;

}

// Create final, partially full chunk
if (startPointer < tx_length) {
chunks.push(transmitData.slice(startPointer));
}
} else {
chunks.push(transmitData);
}


// Receive any waiting data first
local receiveHandler = _getHandler("receive");
if (receiveHandler && dataWaiting()) {
receiveHandler(null, _driver.readRxData(_socket));
}


// Finally, loop thru a queue of chunks
_transmitNextChunk = function(chunk) {

// Setup the temporary callback
_handlers["timeout"]  <-
_handlers["transmit"] <-

function(err) {
if (!err && chunks.len() > 0) {
// Send the next chunk
_transmitNextChunk(chunks.remove(0));
} else {
// All done.
__cb(err);
}
}.bindenv(this);

// Send the chunk
_driver.sendTxData(_socket, chunk);
}

// Send the first chunk
_transmitNextChunk(chunks.remove(0));
}


// Function to loop thru a queue of transmissions
_transmitNextInQueue = function() {
if (!_transmitting && _transmitQueue.len() > 0) {
_transmitting = true;
local task = _transmitQueue.remove(0);
_transmit(task.data, function(err) {
_transmitting = false;
imp.wakeup(0, _transmitNextInQueue.bindenv(this));
if (task.cb) task.cb(err);
}.bindenv(this));
}
}


// Check types are valid and convert otherwise
if (transmitData == null) {
throw "transmit() requires a string or blob";
} else if (typeof transmitData != "string") {
transmitData = transmitData.tostring();
}
if (typeof transmitData != "string") {
throw "transmit() requires a string or blob";
}

// Check the socket is ready
if (_state != W5500_SOCKET_STATES.ESTABLISHED) {
return cb("Connection not established");
}

// Append to the queue and start the transmission.
_transmitQueue.push({ "data": transmitData, "cb": cb });
_transmitNextInQueue();

return this;
}


// ***************************************************************************
// receive
// Returns: this
// Parameters:
//      cb(optional) - callback to pass receive data to
//                     (note: if callback is passed in it will superceede
//                     the callback set by setReceiveCallback, and will be
//                     used only for during this check for data)
// **************************************************************************
function receive(cb = null, timeout = 0) {

local receiveHandler = _getHandler("receive");
local callback = cb ? cb : receiveHandler;

if (cb) {

local timeout_timer = null;

// Temporarily take over the callback
onReceive(function(err, data) {
if (timeout_timer) imp.cancelwakeup(timeout_timer);
timeout_timer = null;
cb(err, data);
onReceive(receiveHandler);
}.bindenv(this));

// Set a timeout before putting the handler back
if (timeout > 0) {
timeout_timer = imp.wakeup(timeout, function() {
cb(W5500_ERR_RECEIVE_TIMEOUT, null);
onReceive(receiveHandler);
}.bindenv(this))
}

} else if (callback) {
// Handle a normal callback here
if (_state == W5500_SOCKET_STATES.ESTABLISHED) {
// local before = _driver.getRxDataSize(_socket);
local data = _driver.readRxData(_socket);
// local after = _driver.getRxDataSize(_socket);
if (data && data.len() > 0) {
// if (_debug) server.error(format("RECEIVE CALLBACK: %d - %d = %d", before, data.len(), after));
callback(null, data);
if (dataWaiting()) {
// _debug = true;
// server.error(format("DATA WAITING: %d - %d = %d", before, data.len(), _driver.getRxDataSize(_socket)));
imp.wakeup(0, receive.bindenv(this));
} else {
// _debug = false;
}
} else {
// if (_debug) server.error(format("RECEIVE STOP: %d - %d = %d", before, data.len(), after));
// _debug = false;
}
} else {
callback(W5500_ERR_NOT_CONNECTED, null);
}
}

return this;
}


// ***************************************************************************
// _returnRandomPort
// Returns: an array with a random port between min and max
// Parameters:
//      min - lowest possible port number
//      max - highest possible port number
// **************************************************************************
function _returnRandomPort(min, max) {
local port = (1.0 * math.rand() / RAND_MAX) * (max + 1 - min);
port = port.tointeger() + min;
return [(port >> 8) & 0xFF, port & 0xFF];
}

}

    // KiWi overrides of above Electric Imp libraries
class KiwiModbusTCPMaster extends ModbusTCPMaster {
    _connectInProgress = false;
    _disconnectOnConnect = false;
    _savedDisconnectCb = null;
    _id = 0;

    constructor(wiz, debug = false, slave = 0) {
        _id = slave;
        base.constructor(wiz,debug);
    }

    function connect(connectionSettings, connectCallback = null, reconnectCallback = null) {
        _connectInProgress = true;
        base.connect(connectionSettings, connectCallback, reconnectCallback);
    }

    function _onConnect(error, conn) {
        _connectInProgress = false;
        if (_disconnectOnConnect) {
            logger.log("Disconnecting on connection completed");
            _disconnectOnConnect = false;
            disconnect(_savedDisconnectCb);
        } else {
            base._onConnect(error, conn);
        }
    }

    function disconnect(callback = null) {
        _shouldRetry = false;
        if ("close" in _connection) {
            _connection.close(callback);
        } else {
            if (_connectInProgress) {
                // register a disconnect on connect complete
                logger.log("Registering a disconnect on connect completion");
                _disconnectOnConnect = true;
                _savedDisconnectCb = callback;
            } else if (callback) {
                callback();
            }
        }
    }

    function _send(PDU, properties) {
        _transactions[_transactionCount] <- properties;
        local ADU = _createADU(PDU);
        // with or without success in transmission of data, the transaction count would be advanced
        _transactionCount = (_transactionCount + 1) % MAX_TRANSACTION_COUNT;
        if ("transmit" in _connection) {
            _connection.transmit(ADU, function(error) {
                if (error) {
                    _callbackHandler(error, null, properties.callback);
                } else {
                    _log(ADU, "Outgoing ADU: ");
                }
            }.bindenv(this));
        } else {
            // don't attempt to transmit if connection no longer exists
            local error = "CONNECTION_NO_LONGER_ACTIVE";
            _callbackHandler(error, null, properties.callback);
        }
    }

    function _createADU(PDU) {
        //logger.log("KiwiModbusTCPMaster::_createADU:" + PDU);
        local ADU = blob();
        ADU.writen(swap2(_transactionCount), 'w');
        ADU.writen(swap2(0x0000), 'w');
        ADU.writen(swap2(PDU.len() + 1), 'w');
        ADU.writen(_id, 'b');
        ADU.writeblob(PDU);
        return ADU;
    }
}

class KiwiSPIFlashLogger extends SPIFlashLogger {

    //
    // Gets the logged object at the specified position
    //
    function _getObject(pos, cb = null) {
        local requested_pos = pos;

        _enable();
        // Get the meta (for checking) and the object length (to know how much to read)
        local marker = _flash.read(pos, SPIFLASHLOGGER_OBJECT_MARKER_SIZE).tostring();
        local len = _flash.read(pos + SPIFLASHLOGGER_OBJECT_MARKER_SIZE, 2).readn('w');
        _disable();

        if (marker != SPIFLASHLOGGER_OBJECT_MARKER) {
            logger.log("SPI flash logger: Ignoring object marker not found at " + pos);
            return null;
        }

        local serialised = blob(SPIFLASHLOGGER_OBJECT_HDR_SIZE + len);

        local leftInObject;
        _enable();
        // while there is more object left, read as much as we can from each sector into `serialised`
        while (leftInObject = serialised.len() - serialised.tell()) {
            // Decide what to read
            local sectorStart = pos - (pos % SPIFLASHLOGGER_SECTOR_SIZE);
            local sectorEnd = sectorStart + SPIFLASHLOGGER_SECTOR_SIZE;// MINUS ONE?
            local leftInSector = sectorEnd - pos;

            // Read it
            local read;
            if (leftInObject < leftInSector) {
                read = _flash.read(pos, leftInObject);
                assert(read.len() == leftInObject);
            } else {
                read = _flash.read(pos, leftInSector);
                assert(read.len() == leftInSector);
            }

            serialised.writeblob(read);

            // Update remaining and position
            leftInObject -= read.len();

            pos += read.len();
            assert (pos <= sectorEnd);

            if (pos == _end) pos = _start + SPIFLASHLOGGER_SECTOR_METADATA_SIZE;
            else if (pos == sectorEnd) pos += SPIFLASHLOGGER_SECTOR_METADATA_SIZE;

        }
        _disable();

        // Try to deserialize the object
        local obj;
        try {
            obj = _serializer.deserialize(serialised, SPIFLASHLOGGER_OBJECT_MARKER);
        } catch (e) {
            logger.error(format("Exception reading logger object address 0x%04x with length %d: %s", requested_pos, serialised.len(), e));
            obj = null;
        }

        if (cb) cb(obj);
        else return obj;
    }

// Reads objects from the logger asynchronously.
//
// This mehanism is intended for the asynchronous processing of each log object,
// such as sending data to the agent and waiting for an acknowledgement.
//
// Parameters:
//
// onData   - Callback that provides the object which has been read
//            from the logger
//
// onFinish - Callback that is called after the last object is provided
//            (i.e. there are no more objects to return by the current read operation),
//            or when the operation it terminated,
//            or in case of an error The callback has no parameters.
//
// step     - The rate at which the read operation steps through the logged
//            objects. Must not be 0. If it has a positive value the read
//            operation starts from the oldest logged object.
//            If it has a negative value, the read operation starts from the
//            most recently written object and steps backwards. Defaule value: 1
//
// skip     - Skips the specified number of the logged objects at the start of
//            the reading. Must not has a negative value. Default value: 0
//
    function read(onData = null, onFinish = null, step = 1, skip = 0) {
        assert(typeof step == "integer" && step != 0);

        // skipped tracks how many entries we have skipped, in order to implement skip
        local skipped = math.abs(step) - skip - 1;
        local count = 0;

        // function to read one sector, optionally continuing to the next one
        local readSector;
        local objectsStartCodes = null;
        readSector = function(i) {

            if (i >= _sectors){
                if (onFinish != null) {
                    return onFinish()
                }
                return;
            };

            // convert sector index `i`, ordered by recency, to physical `sector`, ordered by position on disk
            local sector;
            if (step > 0) {
                sector = (_atSec + i + 1) % _sectors;
            } else {
                sector = (_atSec - i + _sectors) % _sectors;
            }

            objectsStartCodes = _getObjectsStartCodesForSector(sector);

            if (objectsStartCodes.len() == 0) {
                return imp.wakeup(0, function() {
                    readSector(i + 1);
                }.bindenv(this))
            };

            if (step < 0) {
                // negative step, go backwards
                // `skip` will take care of the magnitude of the steps
                objectsStartCodes.seek(-2, 'e');
            }


            local addr, spiAddr, obj, readObj, cont, seekTo;

            // Passed in to the read callback to be called as `next`
            cont = function(keepGoing = true) {
                if (keepGoing == false) {
                    // Clean up and exit
                    objectsStartCodes = obj = null;
                    if (onFinish != null) onFinish();
                } else if ((objectsStartCodes.seek(seekTo, 'c') == -1 || objectsStartCodes.eos() == 1)) {
                    //  ^ Try to seek to the next available object
                    // If we've exhausted all addresses found in this sector, move on to the next
                    return imp.wakeup(0, function() {
                        readSector(i + 1);
                    }.bindenv(this));
                } else {
                    // There are more objects to read, read the next one
                    return imp.wakeup(0, readObj.bindenv(this));
                }
            };

            readObj =  function() {

                if (++skipped == math.abs(step)) {
                    // We are not skipping this object, reset `skipped` count
                    skipped = 0;
                    // Get the address (offset from the end of this sectors meta)
                    addr = objectsStartCodes.readn('w');
                    // Calculate the raw spiflash address
                    spiAddr = _start + sector * SPIFLASHLOGGER_SECTOR_SIZE + SPIFLASHLOGGER_SECTOR_METADATA_SIZE + addr;
                    // Read the object
                    obj = _getObject(spiAddr);

                    // If we're moving backwards, do so, otherwise our blob cursor is
                    // already moved forward by reading
                    if (step < 0) seekTo = -4;
                    else seekTo = 0

                    if (obj == null) {
                        // Skip this object as it has an illegal marker
                        // Illegal marker can occur if a sector has been overwritten between
                        // reading the getobjectstartcodes() and reading the object in getobject()
                        return cont();
                    }

                    return onData(obj, spiAddr, cont.bindenv(this));

                } else {
                    // We need to skip more
                    if (step < 0) seekTo = -2;
                    else seekTo = 2

                    // continue
                    return cont();

                }

            }.bindenv(this);

            imp.wakeup(0, readObj.bindenv(this));  // start reading objects in this sector

        }.bindenv(this)

        imp.wakeup(0, function() {
            readSector(0); // start reading sectors
        });
    }
}

// Drivers

// Buses

class I2cDriver {
    i2cBus    = null;
    writeAddr = null;
    readAddr  = null;

    constructor(bus, writeAddress, readAddress) {
        i2cBus = bus;
        writeAddr = writeAddress;
        readAddr = readAddress;
    }

    // Abstract function for concrete child class to implement
    function selfTest();

    function init() {
        // todo this is configuring the bus ...
        // i2c0 is shared between RGB and Humidity sensor so should it be set just once only ...
        // may be in startup sequence?
        i2cBus.configure(CLOCK_SPEED_400_KHZ);
    }

    function write(address, data) {
        local WriteData = blob(2);
        WriteData[0] = address;
        WriteData[1] = data;

        return i2cBus.write(writeAddr, WriteData.tostring());
    }

    function read(address) {
        local ReadData = blob(1);
        ReadData[0] = address;

        return i2cBus.read(readAddr, ReadData.tostring(), 1);
    }

    function readError() {
        return i2cBus.readerror();
    }

/*
    Notes:
     - Not all values are allowed, e.g. setting 0 to 10110000, because the 2nd bit isn't enabled, so you can't set the exec mode
     - Some values take a little while to set, so the chip fails to initialise sometimes
     - Some values change back to 00 after you set them, e.g. the EXEC mode bits in the ENABLE register

     // TODO - CAS - 09/09/2017 - These things:
     - Understand the blob nonsense, and see if we can do without
     - Remove unneeded initialisation sequence commands
     - Namespace the registers
     - Test whether unused constants are eliminated at build/upload time
     - Have a read-only endpoint
     - Include the results in the HTTP return values
     - Extract a generic i2c device, then add specialisations for the LP5521
     - Create a standard way of stubbing i2c interactions, as per Priya's modbus examples.
     */
    function setRegister(register, value) {
        local ReadOne = blob(1)
        local ReadTwo = blob(1)

        local ReadOne = read(register);
        write(register, value);
        local ReadTwo = read(register);
        server.log("Register [" + register + "] before: " + ReadOne[0] + " after: " + ReadTwo[0]);
    }
}


// Chips (depend on buses)
const LP5521_REG_ENABLE        = 0x00;
const LP5521_REG_OP_MODE       = 0x01;
const LP5521_REG_R_PWM         = 0x02;
const LP5521_REG_G_PWM         = 0x03;
const LP5521_REG_B_PWM         = 0x04;
const LP5521_REG_CONFIG        = 0x08;

const LP5521_MASTER_ENABLE   = 0x40; /* Chip master enable */
const LP5521_LOGARITHMIC_PWM = 0x80; /* Logarithmic PWM adjustment */
const LP5521_EXEC_THEN_HOLD  = 0x2A;

const LP5521_CP_MODE_AUTO = 0x18; /* Automatic mode selection */
const LP5521_CLK_INT      = 1;    /* Internal clock */

const LP5521_REG_LED_PWM_BASE  = 0x02;

enum LED_LIGHT {
    RED = 1,
    GREEN = 2,
    BLUE = 3,
    YELLOW = 4,
    TEAL = 5,
    PURPLE = 6,
    WHITE = 7
}

/* aka RGB Driver */
class LP5521 extends I2cDriver {
    static BLINK_INTERVAL = 0.5;
    static LED_R = 0;
    static LED_G = 1;
    static LED_B = 2;

    _ledState = 0;
    _ledColors = null;
    _timer = null;

    constructor(bus, writeAddress = 0x64, readAddress = 0x65) {
        base.constructor(bus, writeAddress, readAddress);
    }

    function selfTest() {
        base.init();

        local writeValue = LP5521_MASTER_ENABLE | LP5521_LOGARITHMIC_PWM;
        local writeResult = write(LP5521_REG_ENABLE, writeValue);
        if(writeResult != 0) {
            return format("write failed (%i)", writeResult);
        }

        local readResult = read(LP5521_REG_ENABLE);
        if (readResult == null) {
            return format("read failed (%i)", readError());
        }

        local readValue = readResult[0];
        if(readValue != writeValue) {
            return format("read value not equal to write value (r: 0x%02X, w: 0x%02X)", readValue, writeValue);
        }

        return null;
    }

    function init() {
        base.init();

        /* Reset enable back to default */
        write(LP5521_REG_ENABLE, 0x00);
        /* Direct control on all channels */
        write(LP5521_REG_OP_MODE, 0x3F);
        /* Enable auto-powersave, set charge pump to auto */
        write(LP5521_REG_CONFIG, LP5521_CP_MODE_AUTO | LP5521_CLK_INT);
        /* Initialize all channels PWM to zero -> leds off */
        write(LP5521_REG_R_PWM, 0);
        write(LP5521_REG_G_PWM, 0);
        write(LP5521_REG_B_PWM, 0);
        /* Set engines are set to run state when OP_MODE enables engines */
        write(LP5521_REG_ENABLE, LP5521_MASTER_ENABLE | LP5521_LOGARITHMIC_PWM | LP5521_EXEC_THEN_HOLD);

        off();
    }

    function on(color) {
        off();
        _ledColors = _toLEDs(color);
        _turnOn(_ledColors);
    }

    function onFor(color, durationInSec) {
        off();
        _ledColors = _toLEDs(color);
        _turnOn(_ledColors);
        _timer = imp.wakeup(durationInSec, off.bindenv(this));
    }

    function blink(color) {
        off();
        _ledColors = _toLEDs(color);
        _alternate();
    }

    function off() {
        _timer = cancelTimer(_timer);
        _ledState = 0;
        _ledColors = [];
        _turnOff();
    }

    function _alternate() {
        _ledState = _ledState ? 0 : 1;
        if(_ledState) { _turnOn(_ledColors); }
        else { _turnOn(_ledColors, 0); }

        _timer = imp.wakeup(BLINK_INTERVAL, _alternate.bindenv(this));
    }
    
    function _turnOn(leds, brightness = 90) {
        foreach(led in leds) { _setBrightness(led, brightness); }
    }

    function _turnOff() {
        _setBrightness(LED_R, 0);
        _setBrightness(LED_G, 0);
        _setBrightness(LED_B, 0);
    }
    
    /* LED: R = 0, G = 1, B = 2, Brightness: int 0 - 255 */
    function _setBrightness(LED, brightness) {
        write(LP5521_REG_LED_PWM_BASE + LED, brightness);
    }

    function _toLEDs(color) {
        switch(color) {
            case LED_LIGHT.RED:    return [LED_R];
            case LED_LIGHT.GREEN:  return [LED_G];
            case LED_LIGHT.BLUE:   return [LED_B];
            case LED_LIGHT.YELLOW: return [LED_R, LED_G];
            case LED_LIGHT.TEAL:   return [LED_G, LED_B];
            case LED_LIGHT.PURPLE: return [LED_R, LED_B];
            case LED_LIGHT.WHITE:  return [LED_R, LED_G, LED_B];
            default: return [];
        }
    }
}

// Services (depend on chips or buses)
enum RelayState {
    OPEN = 0,
    CLOSED  = 1
}

class RelayDriver {
    _relay = null;

    // todo should be private
    state = null;

    constructor(assignedPin) {
        _relay = assignedPin;
        state = RelayState.OPEN;
        _relay.configure(DIGITAL_OUT, 0);
    }

    function isClosed() { return state == RelayState.CLOSED; }

    function setState(relayState) {
        _relay.write(relayState);
        state = relayState;
    }
}

class RelayFactory {
    function build(relayNumber) {
        switch(relayNumber) {
            case 1: return RelayDriver(hardware.pinN);
            case 2: return RelayDriver(hardware.pinH);
            default: throw "Unsupported relay number";
        }
    }
}
class PulseInputCounterDriver {
    _pulseInput  = null;
    _pulseCounts = 0;
    _prevMillis  = 0;

    constructor(hardwarePin) {
        _pulseInput = hardwarePin;
        _pulseInput.configure(DIGITAL_IN, this._onStateChange.bindenv(this));
        _prevMillis = hardware.millis();
    }

    function _onStateChange() {
        // update pulse count on rising edge only
        if (_pulseInput.read()) {
            _pulseCounts++;
        }
    }

    // return float: pulses per second
    function pulseFrequency() {
        local nowMillis = hardware.millis();
        local duration  = millisElapsed(nowMillis, _prevMillis) * 1.0;
        local count = getPulseCount();
        local frequency = duration > 0 ? (count/duration) * 1000 : 0.0;
        _prevMillis = nowMillis;
        resetPulseCount();
        return frequency;
    }

    function pulseName() {
        switch (_pulseInput) {
            case hardware.pinT:  return "Pulse1";
            case hardware.pinL:  return "Pulse2";
            case hardware.pinXE: return "Pulse3";
            case hardware.pinY:  return "Pulse4";
            default: throw "Unsupported pulse pin";
        }
    }

    function getPulseCount() {
        return _pulseCounts;
    }

    function resetPulseCount() {
        _pulseCounts = 0;
    }

    function resetPinConfiguration() {
        _pulseInput.configure(DIGITAL_IN);
    }

    function getPinInstance() {
        return _pulseInput;
    }
}

class PulseInputIntervalDriver {
    _pulseInput             = null;
    _prevMillis             = 0;
    _onPulseIntervalHandler = null;

    constructor(hardwarePin) {
        _pulseInput = hardwarePin;
        _pulseInput.configure(DIGITAL_IN, this._onStateChange.bindenv(this));
        _prevMillis = hardware.millis();
    }

    function _onStateChange() {
        if (_pulseInput.read()) {
            local nowMillis = hardware.millis();
            local interval = millisElapsed(nowMillis ,_prevMillis);
            _prevMillis = nowMillis;
            if (_onPulseIntervalHandler) {
                _onPulseIntervalHandler(interval);
            }
        }
    }

    function setPulseIntervalHandler(handler) {
        _onPulseIntervalHandler = handler;
    }

    function resetPinConfiguration() {
        _pulseInput.configure(DIGITAL_IN);
    }
}

class RS232Driver {
    _uart            = null;
    _baudRate        = null;
    _wordSize        = null;
    _parity          = null;
    _stopBits        = null;
    _controlFlags    = null;
    _state           = 0;
    _uart            = null;
    _charRecvHandler = null;

    constructor(uart, baudRate, wordSize, parity, stopBits, controlFlags) {
        _uart         = uart;
        _baudRate     = baudRate;
        _wordSize     = wordSize;
        _parity       = parity;
        _stopBits     = stopBits;
        _controlFlags = controlFlags;
    }

    function init() {
        local baud = _uart.configure(_baudRate, _wordSize, _parity, _stopBits, _controlFlags, _recvChar.bindenv(this));
        server.log("RS232Driver:init() RS232 Baud rate: " + baud);
    }

    function resetUart() {
        _uart.disable();
        _uart = null;
    }

    function configureCallback(recvHandler) {
        _charRecvHandler = recvHandler;
    }

// -------------------- PRIVATE METHODS -------------------- //
    function _recvChar() {
        local ch;
        do {
            ch = _uart.read();
            if ( ch != -1) {
                _charRecvHandler(ch);
            }
        } while (ch != -1)
    }

    function _writeChar(ch) {
        _uart.write(ch);
    }
}
class BinaryInputDriver {
    _state = null;
    _input = null;

    constructor(hardwarePin) {
        _input = hardwarePin;
        _input.configure(DIGITAL_IN, this._onStateChange.bindenv(this));
        _onStateChange();
    }

    function _onStateChange() {
        _state = _input.read();
        server.log(_state?"HIGH":"LOW");
    }

    function getInput() {
        return _state;
    }
}

// Metering Segment
const REG_AIGAIN		= 0x4380;
const REG_AVGAIN		= 0x4381;
const REG_BIGAIN		= 0x4382;
const REG_BVGAIN		= 0x4383;
const REG_CIGAIN		= 0x4384;
const REG_CVGAIN		= 0x4385;

const REG_AIRMSOS		= 0x4387;
const REG_AVRMSOS		= 0x4388;
const REG_BIRMSOS		= 0x4389;
const REG_BVRMSOS		= 0x438a;
const REG_CIRMSOS		= 0x438b;
const REG_CVRMSOS		= 0x438c;

const REG_AVAGAIN		= 0x438e;
const REG_BVAGAIN		= 0x438f;
const REG_CVAGAIN		= 0x4390;
const REG_AWGAIN		= 0x4391;
const REG_AWATTOS		= 0x4392;
const REG_BWGAIN		= 0x4393;
const REG_BWATTOS		= 0x4394;
const REG_CWGAIN		= 0x4395;
const REG_CWATTOS		= 0x4396;

const REG_AVARGAIN		= 0x4397;
const REG_AVAROS		= 0x4398;
const REG_BVARGAIN		= 0x4399;
const REG_BVAROS		= 0x439a;
const REG_CVARGAIN		= 0x439b;
const REG_CVAROS		= 0x439c;

const REG_VATHR1		= 0x43a9;
const REG_VATHR0		= 0x43aa;
const REG_WTHR1 	    = 0x43ab;
const REG_WTHR0	    	= 0x43ac;
const REG_VARTHR1		= 0x43ad;
const REG_VARTHR0		= 0x43ae;

const REG_AIRMS		    = 0x43c0;
const REG_AVRMS		    = 0x43c1;
const REG_BIRMS		    = 0x43c2;
const REG_BVRMS		    = 0x43c3;
const REG_CIRMS		    = 0x43c4;
const REG_CVRMS		    = 0x43c5;

const REG_RUN			= 0xe228;

const REG_AWATTHR		= 0xe400;
const REG_BWATTHR		= 0xe401;
const REG_CWATTHR		= 0xe402;

const REG_AVARHR        = 0xe406;
const REG_BVARHR        = 0xe407;
const REG_CVARHR        = 0xe408;

const REG_STATUS1		= 0xe503;

const REG_AWATT		    = 0xe513;
const REG_BWATT		    = 0xe514;
const REG_CWATT		    = 0xe515;

const REG_AVAR		    = 0xe516;
const REG_BVAR		    = 0xe517;
const REG_CVAR		    = 0xe518;

const REG_PHSTATUS	    = 0xe600;
const REG_ANGLE0		= 0xe601;
const REG_ANGLE1		= 0xe602;
const REG_ANGLE2		= 0xe603;
const REG_PERIOD		= 0xe607;
const REG_CFMODE		= 0xe610;
const REG_CF1DEN		= 0xe611;
const REG_CF2DEN		= 0xe612;
const REG_CF3DEN		= 0xe613;
const REG_APHCAL		= 0xe614;
const REG_BPHCAL		= 0xe615;
const REG_CPHCAL		= 0xe616;
const REG_CONFIG		= 0xe618;

const REG_MMODE		    = 0xe700;
const REG_ACCMODE		= 0xe701;
const REG_LCYCMODE   	= 0xe702;
const REG_VERSION		= 0xe707;

const ONE_VOLT          = 1;
const CLOCK_256_KHZ     = 256000.0;
const CLOCK_256_MHZ     = 256000000.0;

const MASK_MMODE_REGISTER_SETTING = 0xFC;

const IRMS_REG_SCALING_FACTOR = 1000000.0;
const VRMS_REG_SCALING_FACTOR = 10000.0;
const WATT_REG_SCALING_FACTOR = 1000.0;

const WTHR_REG_SCALING_FACTOR = 17.0;

const PHASE_CAL_RESOLUTION = 0.0176;    // (360 * 50Hz/1.024MHz)

const ANGLE_RESOLUTION = 0.0703; // (360 * 50) / 256000

enum MModeRegisterSettings {
    PhaseA_Period = 0x00,
    PhaseB_Period = 0x01,
    PhaseC_Period = 0x02,
    Phase_Invalid = 0x03,
};

enum RamWrite {
    DISABLE = 0x00,
    ENABLE = 0x80
};

class ADE7858 extends I2cDriver {
    _aVrmsAvg    = 0;
    _bVrmsAvg    = 0;
    _cVrmsAvg    = 0;

    _aLastEnergy = 0;
    _bLastEnergy = 0;
    _cLastEnergy = 0;
    _arLastEnergy = 0;
    _brLastEnergy = 0;
    _crLastEnergy = 0;

    _aPrevMillis = 0;
    _bPrevMillis = 0;
    _cPrevMillis = 0;
    _arPrevMillis = 0;
    _brPrevMillis = 0;
    _crPrevMillis = 0;

    constructor() {
        local now = hardware.millis();
        _setPrevMillis(REG_AWATTHR, now);
        _setPrevMillis(REG_BWATTHR, now);
        _setPrevMillis(REG_CWATTHR, now);
        _setPrevMillis(REG_AVARHR, now);
        _setPrevMillis(REG_BVARHR, now);
        _setPrevMillis(REG_CVARHR, now);

        base.constructor(hardware.i2cJK, 0x70, 0x71);
    }

    function init() {
        if ("isAStub" in i2cBus) { return 0; }

        i2cBus.configure(CLOCK_SPEED_400_KHZ);

        local status = blob();
        local result = 0;
        local retries = 5;

        // Wait for hardware reset to complete
        do {
            status = _readReg (REG_STATUS1);
            if (status != null) { result = status[0] & (1<<15); }
            else { imp.sleep(0.1); }
        }
        while (!result && retries--);

        if (status == null) {
            server.log("Metering segment not detected");
            return 0;
        }
        // Perform a software reset
        _writeReg (REG_CONFIG, 1<<7);

        // Wait for software reset to complete
        result = 0;
        retries = 5;
        do {
            status = _readReg (REG_STATUS1);
            if (status != null)
                result = status[0] & (1<<15);
            else
                imp.sleep(0.1);
        }
        while (!result && retries--);

        if (status == null) {
            server.log("ADE7858 software reset failed");
            return 0;
        }

        // Start the DSP by setting Run=1
        _writeReg (REG_RUN, 0x0001);

        // Enable the data memory RAM protection by writing 0xAD to an internal 8-bit register
        //	located at Address 0xE7FE followed by a write of 0x80 to an internal 8-bit register
        //	located at Address 0xE7E3.
        _ramWriteProtection(RamWrite.ENABLE);

        // Enable the CF1, CF2 and CF3 frequency converter outputs by clearing bits 9, 10 and 11
        //	(CF1DIS, CF2DIS, and CF3DIS) to 0 in CFMODE register.
        if (_readReg(REG_CFMODE) != null) {
            _writeReg (REG_CFMODE, _readReg(REG_CFMODE)[0] & ~(0x7 << 9));
        }

        _aVrmsAvg = readAvgRms(REG_AVRMS);
        _bVrmsAvg = readAvgRms(REG_BVRMS);
        _cVrmsAvg = readAvgRms(REG_CVRMS);

        _setLastEnergy(REG_AWATTHR, readActiveEnergy(REG_AWATTHR));
        _setLastEnergy(REG_BWATTHR, readActiveEnergy(REG_BWATTHR));
        _setLastEnergy(REG_CWATTHR, readActiveEnergy(REG_CWATTHR));

        server.log("ADE7858 silicon revision " + _readReg(REG_VERSION)[0]);
        return 1;
    }

    function readActivePower(register) {
        local power = _read32BitsAvg(register);
        return power / WATT_REG_SCALING_FACTOR;
    }

    function readActiveEnergy(register) {
        local data = blob(4);
        data = _readReg(register);
        return _convertBlobTo32Bits(data);
    }

    /*
        return float: average power in Watts
     */
    function averagePower(register) {
        local now = hardware.millis();
        local durationMillis = millisElapsed(now, _getPrevMillis(register));
        local durationMilliHours = durationMillis/3600.0;
        local energy_mW = _getEnergyDiff(register);
        local avgPower = energy_mW/durationMilliHours;

        //We refer to power as an average as it considers the total energy used over the time period
        _setPrevMillis(register, now);
        return avgPower / WTHR_REG_SCALING_FACTOR;
    }

    function averageReactivePower(register) {
        local now = hardware.millis();
        local durationMillis = millisElapsed(now, _getPrevMillis(register));
        local durationMilliHours = durationMillis/3600.0;
        local energy_mVAR = _getEnergyDiff(register);
        local avgReactivePower = energy_mVAR/durationMilliHours;

        //We refer to power as an average as it considers the total energy used over the time period
        _setPrevMillis(register, now);
        return avgReactivePower / WTHR_REG_SCALING_FACTOR;
    }

    function readAvgRms(register) {
        local rms = _read32BitsAvg(register);
        local rescale = 1.0;
        if (register == REG_AIRMS || register == REG_BIRMS || register == REG_CIRMS) {
            rescale = IRMS_REG_SCALING_FACTOR;
        } else if (register == REG_AVRMS || register == REG_BVRMS || register == REG_CVRMS) {
            rescale = VRMS_REG_SCALING_FACTOR;
        }
        return rms / rescale;
    }

    function _read32BitsAvg(register) {
        local data = blob(4);
        local val = 0;
        local acc = 0;
        local cnt = 0;
        
        for (local i=0; i<50; i++) {
            try {
                data = _readReg(register);
                val = _convertBlobTo32Bits(data);
                acc += val;
                cnt += 1;
            } catch (ex) {
                server.log("WARN: "+ ex);
            }
        }

        return acc / cnt.tofloat();
    }

    function readFrequency(Phase_Period) {
        local retData = blob(2);
        local data = 0;
        local freqMillis = 0;
        local vrms = 0;
        local mode;

        retData = _readReg(REG_MMODE);
        mode = retData[0] & MASK_MMODE_REGISTER_SETTING;

        switch ( Phase_Period ) {
            case MModeRegisterSettings.PhaseB_Period:
                vrms = _bVrmsAvg;
                break;
            case MModeRegisterSettings.PhaseC_Period:
                vrms = _cVrmsAvg;
                break;
            case MModeRegisterSettings.PhaseA_Period:  // Intentional fall-thru
            default:
                vrms = _aVrmsAvg;
                break;
        }

        if (vrms > ONE_VOLT) {
            data = mode | Phase_Period;
            _writeReg(REG_MMODE, data);
            retData = _readReg(REG_PERIOD);
            freqMillis += retData[0] << 8;
            freqMillis += retData[1];
            freqMillis = (CLOCK_256_MHZ/(freqMillis + 1));
        }
        return freqMillis;
    }

    function readPF(register) {
        local retData = blob(2);
        local angleInDegrees = 0.0;
        local leadOrLag = " lag";

        retData = _readReg(register);
        angleInDegrees += retData[0] << 8;
        angleInDegrees += retData[1];
        angleInDegrees *= ANGLE_RESOLUTION;

        if (angleInDegrees > 180) {
            leadOrLag = " lead";
        } // else lagging

        local angleInRadians = (angleInDegrees * PI) / 180;
        local powerFactor = math.cos(angleInRadians);
        local powerFactorString = powerFactor.tostring() + leadOrLag;

        return powerFactorString;
    }

    function calibrate(connection, calibrationConfig) {
        _ramWriteProtection(RamWrite.DISABLE);

        _calibrateGain(connection, calibrationConfig);
        _calibrateDcOffset(connection, calibrationConfig);
        _calibratePhCal(connection, calibrationConfig);
        _calibrateTotalActiveEnergy(connection, calibrationConfig);

        _ramWriteProtection(RamWrite.ENABLE);
    }

    // -------------------- PRIVATE METHODS -------------------- //
    function _calibrateGain(connection, calibrationConfig) {
        if (calibrationConfig && "gain" in calibrationConfig) {
            local gain = calibrationConfig.gain;
            if ("current" in gain) {
                if (connection == "CT1") {_writeReg(REG_AIGAIN, gain.current);}
                if (connection == "CT2") {_writeReg(REG_BIGAIN, gain.current);}
                if (connection == "CT3") {_writeReg(REG_CIGAIN, gain.current);}
            }

            if ("voltage" in gain) {
                if (connection == "CT1") {_writeReg(REG_AVGAIN, gain.voltage);}
                if (connection == "CT2") {_writeReg(REG_BVGAIN, gain.voltage);}
                if (connection == "CT3") {_writeReg(REG_CVGAIN, gain.voltage);}
            }

            if ("watt" in gain) {
                if (connection == "CT1") {
                    _writeReg(REG_AVAGAIN, gain.watt);
                    _writeReg(REG_AWGAIN, gain.watt);
                    _writeReg(REG_AVARGAIN, gain.watt);
                }
                if (connection == "CT2") {
                    _writeReg(REG_BVAGAIN, gain.watt);
                    _writeReg(REG_BWGAIN, gain.watt);
                    _writeReg(REG_BVARGAIN, gain.watt);
                }
                if (connection == "CT3") {
                    _writeReg(REG_CVAGAIN, gain.watt);
                    _writeReg(REG_CWGAIN, gain.watt);
                    _writeReg(REG_CVARGAIN, gain.watt);
                }
            }
        }
    }

    function _calibrateDcOffset(connection, calibrationConfig) {
        if (calibrationConfig && "dcOffset" in calibrationConfig) {
            local dcOffset = calibrationConfig.dcOffset;
            if ("current" in dcOffset) {
                if (connection == "CT1") {_writeReg(REG_AIRMSOS, dcOffset.current);}
                if (connection == "CT2") {_writeReg(REG_BIRMSOS, dcOffset.current);}
                if (connection == "CT3") {_writeReg(REG_CIRMSOS, dcOffset.current);}
            }

            if ("voltage" in dcOffset) {
                if (connection == "CT1") {_writeReg(REG_AVRMSOS, dcOffset.voltage);}
                if (connection == "CT2") {_writeReg(REG_BVRMSOS, dcOffset.voltage);}
                if (connection == "CT3") {_writeReg(REG_CVRMSOS, dcOffset.voltage);}
            }
        }
    }

    function _calibratePhCal(connection, calibrationConfig) {
        local ctError = 0;
        local phaseCal = 0;

        if (calibrationConfig && "ctPhaseCalibrationErrorInDegrees" in calibrationConfig) {
            ctError = round(calibrationConfig.ctPhaseCalibrationErrorInDegrees/PHASE_CAL_RESOLUTION, 0);

            // if current lags the voltage, the result is positive and 512 is added to the result
            // inverted to allow the CT phase calibration to be entered as given
            if (calibrationConfig.ctPhaseCalibrationErrorInDegrees < 0) {
                ctError += 512;
            }
        }

        if (calibrationConfig && "phaseCal" in calibrationConfig) {
            phaseCal = calibrationConfig.phaseCal;
        }

        local totalPhaseCal = (phaseCal + ctError).tointeger();

        if (connection == "CT1") {_writeReg(REG_APHCAL, totalPhaseCal);}
        if (connection == "CT2") {_writeReg(REG_BPHCAL, totalPhaseCal);}
        if (connection == "CT3") {_writeReg(REG_CPHCAL, totalPhaseCal);}
    }

    function _calibrateTotalActiveEnergy(connection, calibrationConfig) {
        //For 10Amp CT, n=3
        // wtHr = 241069955 = 0xE5E6F83;
        if (calibrationConfig) {
            _writeReg(REG_ACCMODE, 0);
            _writeReg(REG_LCYCMODE, 0); // 1 for line cycle mode

            local low = calibrationConfig.wthr.low;
            _writeReg(REG_WTHR0, low);
            _writeReg(REG_VARTHR0, low);
            _writeReg(REG_VATHR0, low);

            local high = calibrationConfig.wthr.high;
            _writeReg(REG_WTHR1, high);
            _writeReg(REG_VARTHR1, high);
            _writeReg(REG_VATHR1, high);
        }
    }

    function _getEnergyDiff(register) {
        local energy = readActiveEnergy(register);
        // "The active energy register content can roll over to full-scale negative (0x8000000) and continue increasing
        //     in value when active power is postive. Conversely, if the active power is negative, the energy register
        //     underflows to full-scale postive (0x7FFFFFF) and continues decreasing in value."
        local energyDiff = diffRolloverSafe(energy, _getLastEnergy(register));
        _setLastEnergy(register, energy);
        return energyDiff;
    }

    function _getLastEnergy(reg) {
        if (reg == REG_AWATTHR) {return(_aLastEnergy)}
        if (reg == REG_BWATTHR) {return(_bLastEnergy)}
        if (reg == REG_CWATTHR) {return(_cLastEnergy)}
        if (reg == REG_AVARHR) {return(_arLastEnergy)}
        if (reg == REG_BVARHR) {return(_brLastEnergy)}
        if (reg == REG_CVARHR) {return(_crLastEnergy)}
    }

    function _setLastEnergy(reg, value) {
        if (reg == REG_AWATTHR) {_aLastEnergy = value}
        if (reg == REG_BWATTHR) {_bLastEnergy = value}
        if (reg == REG_CWATTHR) {_cLastEnergy = value}
        if (reg == REG_AVARHR) {_arLastEnergy = value}
        if (reg == REG_BVARHR) {_brLastEnergy = value}
        if (reg == REG_CVARHR) {_crLastEnergy = value}
    }

    function _getPrevMillis(reg) {
        if (reg == REG_AWATTHR) {return(_aPrevMillis)}
        if (reg == REG_BWATTHR) {return(_bPrevMillis)}
        if (reg == REG_CWATTHR) {return(_cPrevMillis)}
        if (reg == REG_AVARHR) {return(_arPrevMillis)}
        if (reg == REG_BVARHR) {return(_brPrevMillis)}
        if (reg == REG_CVARHR) {return(_crPrevMillis)}
    }

    function _setPrevMillis(reg, value) {
        if (reg == REG_AWATTHR) {_aPrevMillis = value}
        if (reg == REG_BWATTHR) {_bPrevMillis = value}
        if (reg == REG_CWATTHR) {_cPrevMillis = value}
        if (reg == REG_AVARHR) {_arPrevMillis = value}
        if (reg == REG_BVARHR) {_brPrevMillis = value}
        if (reg == REG_CVARHR) {_crPrevMillis = value}
    }

    /*
        TIP: we have seen - the index '0' does not exist - exception being thrown from here
        Ensure caller(s) to this function handle exception appropriately
     */
    function _convertBlobTo32Bits(value) {
        return (value[0] <<24) + (value[1] <<16) + (value[2] <<8) + value[3];
    }

    function _ramWriteProtection(writeEnable = RamWrite.DISABLE) {
        local writeEnableFlag = RamWrite.DISABLE;

        if (writeEnable) {
            writeEnableFlag = RamWrite.ENABLE;
        }

        _writeReg(0xe7fe, 0xad);
        _writeReg(0xe7e3, writeEnableFlag);
    }

    function _writeReg(reg, data) {
        local writeData;

        switch(reg.tointeger() & 0xff00) {
            case 0xe700:
            case 0xec00:
            // data in the region is 8 bits
                writeData = blob(3);
                writeData[0] = (reg & 0xff00) >> 8;
                writeData[1] = (reg & 0xff);
                writeData[2] = data & 0xff;
                i2cBus.write(writeAddr, writeData.tostring());
                break;

            case 0xe200:
            case 0xe600:
            // data in the region is 16 bits
                writeData = blob(4);
                writeData[0] = (reg & 0xff00) >> 8;
                writeData[1] = (reg & 0xff);
                writeData[2] = ((data & 0xff00) >> 8);
                writeData[3] = data & 0xff;
                i2cBus.write(writeAddr, writeData.tostring());
                break;

            case 0x4300:
            // data in the region is 24 bits - but need to write 32 bits, see ADE datasheet
            case 0xe400:
            case 0xe500:
            // data in the region is 32 bits
                writeData = blob(6);
                writeData[0] = (reg & 0xff00) >> 8;
                writeData[1] = (reg & 0xff);
                writeData[2] = ((data & 0xff000000) >> 24);
                writeData[3] = ((data & 0xff0000) >> 16);
                writeData[4] = ((data & 0xff00) >> 8);
                writeData[5] = data & 0xff;
                i2cBus.write(writeAddr, writeData.tostring());
                break;

            default:
            // Unknown region, ignore
                break;
        }
    }

    function _readReg(reg) {
        local writeData = blob(2);
        local readData;
        local len = 1;

        // Write the address we are interested in first
        writeData[0] = (reg & 0xff00) >> 8;
        writeData[1] = (reg & 0xff);
        i2cBus.write(writeAddr, writeData.tostring());

        // Then read the data
        switch(reg & 0xff00) {
            case 0xe700:
            case 0xec00:
            // data in the region is 8 bits
                len = 1;
                break;

            case 0xe200:
            case 0xe600:
            // data in the region is 16 bits
                len = 2;
                break;

            case 0x4300:
            // data in the region is 24 bits - but need to read 32 bits, see ADE datasheet
            case 0xe400:
            case 0xe500:
            // data in the region is 32 bits
                len = 4;
                break;

            default:
            // Unknown region, ignore
                break;
        }

        readData = blob(len);
        readData = i2cBus.read(readAddr, "", len);

        return readData;
    }
}

function cancelTimer(timer) {
    if(timer) {imp.cancelwakeup(timer); }
    return null;
}

function tableGet(key, table) {
    return (key in table) ? table[key] : null;
}

function tableGetOrElse(key, table, otherwise) {
    return (key in table) ? table[key] : otherwise;
}

function tableSafeDelete(key, table) {
    return (key in table) ? delete table[key] : null;
}

function isCharging(p) { return p < 0; }
function isDischarging(p) { return p > 0; }
function round(val, decimalPoints) {
    local f = math.pow(10, decimalPoints) * 1.0;
    local newVal = val * f;
    newVal = math.floor(newVal + 0.5)
    return (newVal * 1.0) / f;
}

function lessThan(a, b, precision) { return round(a, precision) < round(b, precision); }

function max(a, b) { return a > b ? a : b; }
function min(a, b) { return a > b ? b : a; }
function negate(x) { return x * -1; }

function diffRolloverSafe(current, previous) {
    if ((current <=> previous) == 0) { return 0; }
    else { return current - previous; }
}

function millisElapsed(millisNow, refMillis) {
    return math.abs(diffRolloverSafe(millisNow, refMillis));
}
/*
returns date string in yyyy-MM-ddTHH:mm:ss format
 */
function printTimestamp(date) {
    return format("%i-%02i-%02iT%02i:%02i:%02i", date.year, date.month + 1, date.day, date.hour, date.min, date.sec);
}

/*
Approximated Epoch UTC Time in millis
to be used for timestamping recorded items to send to readings service

Readings service expects UTC time in ms.
However, multipying by 1000 crosses the integer 32bit size so get() returns string instead of long.
(though, is 32bits really a problem? can we run 64bits?)

https://electricimp.com/docs/squirrel/system/time/
https://electricimp.com/docs/api/hardware/millis/
 */
class KiwiTime {
    static SAMPLING_INTERVAL = 0.1;

    _refTime = null;
    _refMillis = null;
    _initTime = null;
    _reviseInterval = null;
    _reviseTimer = null;

    constructor(reviseInterval = 0) {
        _refTime = time();
        _refMillis = hardware.millis();
        _initTime = _refTime;
        _reviseInterval = reviseInterval;
        _reviseTimer = imp.wakeup(0, _revise.bindenv(this));
    }

    function checkInvalid() {
        if(_refTime != null && _refTime < 1500000000) {
            _reinit();
        }
    }

    function get() {
        if(_refTime == null || _refMillis == null) {
            return null;
        } else {
            local diff = millisElapsed(hardware.millis(), _refMillis);
            local ms = diff % 1000;
            local s = ((diff - ms) / 1000).tointeger();

            return (_refTime + s) + format("%03d", ms);
        }
    }

    function getImpTime() {
        return time() + format("000");
    }

    function _revise() {
        local t = time();
        if(t > _initTime) {
            _refTime = t
            _refMillis = hardware.millis();

            if(_reviseInterval > 0) {

                if(_reviseTimer != null) {
                    imp.cancelwakeup(_reviseTimer);
                }

                _reviseTimer = imp.wakeup(_reviseInterval, _reinit.bindenv(this));
            }
        } else {
            if(_reviseTimer != null) {
                imp.cancelwakeup(_reviseTimer);
            }

            _reviseTimer = imp.wakeup(SAMPLING_INTERVAL, _revise.bindenv(this));
        }
    }

    function _reinit() {
        _initTime = time();

        if(_reviseTimer != null) {
            imp.cancelwakeup(_reviseTimer);
        }

        _reviseTimer = imp.wakeup(0, _revise.bindenv(this));
    }
}

/* revise time after 12hrs */
local kwTime = KiwiTime(43200);


class Response {
    // todo probably change default data to empty string instead
    function success(contextId, data = {}) {
        agent.send("respond.success", { "contextId": contextId, "payload": data });
    }

    function badRequest(contextId, errorMessage) {
        agent.send("respond.badRequest", _error(contextId, errorMessage));
    }

    function forbidden(contextId, errorMessage) {
        agent.send("respond.forbidden", _error(contextId, errorMessage));
    }

    function notFound(contextId) {
        agent.send("respond.notFound", _error(contextId, "unavailable"));
    }

    function serverError(contextId, errorMessage) {
        agent.send("respond.serverError", _error(contextId, errorMessage));
    }
    
    function serviceUnavailable(contextId, errorMessage) {
        agent.send("respond.serviceUnavailable", _error(contextId, errorMessage));
    }

    function _error(contextId, message) {
        return { "contextId": contextId, "payload": { "message": message } };
    }

    function restartAgent(contextId, data = {}){
        agent.send("respond.restart",{ "contextId": contextId, "payload": data })
    }
}
function convertTimeDateToUnixTime(seconds, minutes, hours, day, month, year) {
    const UNIX_TIME_START_JANUARY_01_1970 = 719561;
    const NUMBER_SECONDS_PER_DAY          =  86400;
    local t;

    //January and February are counted as months 13 and 14 of the previous year
    if (month <= 2)  {
        month += 12;
        year -= 1;
    }
    t = (365 * year) + (year / 4) - (year / 100) + (year / 400); //Convert years to days
    t += (30 * month) + (3 * (month + 1) / 5) + day;             //Convert months to days
    t -= UNIX_TIME_START_JANUARY_01_1970;
    t *= NUMBER_SECONDS_PER_DAY; //Convert days to seconds
    t += (3600 * hours) + (60 * minutes) + seconds; //Add hours, minutes and seconds

    return t;//Return Unix time
}
// This file contains all global constants for fruit device
const NOMINAL_GRID_FREQUENCY_mHZ = 50000;
const IDLE        =  0;
const CONSUME     = -1;
const DELIVER     =  1;
const CHARGE    = -1;
const DISCHARGE =  1;

// Vendors and Models
const VENDOR_NEC = "NEC";
const MODEL_AEROS_GBS = "AEROS GBS";

const VENDOR_MICROMAX = "MICROMAX";
const MODEL_F = "-F";

const VENDOR_TESLA = "TESLA";
const MODEL_POWERPACK = "POWERPACK";

const VENDOR_MAP = "MAP";
const MODEL_BATT  = "BATT";
const MODEL_METER = "METER";
const MODEL_POWERON = "POWERON";

const VENDOR_BYD = "BYD";
const MODEL_EPRI = "EPRI";


const VENDOR_CEWE = "CEWE";
const MODEL_PRO100 = "PRO100";

const VENDOR_CE = "CE";
const MODEL_STACKBATT = "STACKBATT";

// todo redundant
const VENDOR_ANY = "ANY";
const MODEL_ANY = "ANY";

enum CommScheme {
    MODBUS_TCP_MASTER = "MODBUS TCP/IP MASTER",
    MODBUS_SERIAL_MASTER = "MODBUS SERIAL MASTER",
    MODBUS_SERIAL_SLAVE = "MODBUS SERIAL SLAVE",
    RS232 = "RS232"
}

enum InputType {
    PULSE = "PULSE",
    DIGITAL = "DIGITAL",
    METERING_SEGMENT = "METERING SEGMENT",
    SERIAL = "SERIAL",
    MODBUS_TCP_MASTER = "MODBUS TCP/IP MASTER",
    MODBUS_SERIAL_MASTER = "MODBUS SERIAL MASTER",
    PME_PMI = "PME PMI",
    ICE = "ICE",
    ICE_GEN = "ICE GEN",
    SAPHIR = "SAPHIR",
    SAPHIR_GEN = "SAPHIR GEN"
}

enum InputMeasure {
    FREQUENCY = "FREQUENCY",
    POWER     = "POWER"
}

enum PulseInputs {
    PULSE1 = "P1",
    PULSE2 = "P2",
    PULSE3 = "P3",
    PULSE4 = "P4"
}

enum AssetType {
    RELAY = "relay",
    STATIC_FFR_RELAY = "static ffr relay",
    BATTERY = "battery",
    VARIABLE_GENERATOR = "variable generator",
    VARIABLE_LOAD = "variable load"
    // BINARY
}

enum BridgeType {
    POWERON = "poweron"
}

const POWERON_MAX_ZONES = 17;

enum StatusValue {
    IDLE,
    TORN_DOWN,
    RUNNING,
    INITIALISING,
    TEARING_DOWN,
    WARNING,
    BROKEN,
    DISPATCHING
}

class Status {
    _state = null; // StatusValue
    _since = null;
    // todo inspect memory issue
    _observer  = null;

    constructor(state, timestamp = null) {
        update(state, timestamp);
    }

    function update(state, timestamp = null) {
        _state = state;
        _since = timestamp ? timestamp : time();
        if(_observer) {
            _observer(copy());
        }
    }

    function value() { return _state; }
    function since() { return _since; }
    function isRunning() { return _state == StatusValue.RUNNING; }
    function printTimeSince() { return printTimestamp(date(_since)); }

    /* onChangeCallback(status) */
    function register(onChangeCallback) {
        _observer = onChangeCallback;
    }

    /* returns a new Status to avoid mutation further down the line */
    function add(status){
        if(status.value() == value()) {
            local time = null;
            if(status.value() > StatusValue.RUNNING) {
                time = status.since() > since() ? since() : status.since();
            } else {
                time = status.since() > since() ? status.since() : since();
            }
            return Status(value(), time);
        } else {
            return status.value() > value() ? status.copy() : this.copy();
        }
    }

    function name() {
        local state = value();
        switch (state) {
            case StatusValue.IDLE: return "idle";
                break;
            case StatusValue.TORN_DOWN: return "torn down";
                break;
            case StatusValue.RUNNING: return "running";
                break;
            case StatusValue.INITIALISING: return "initialising";
                break;
            case StatusValue.TEARING_DOWN: return "tearing down";
                break;
            case StatusValue.WARNING: return "warning";
                break;
            case StatusValue.BROKEN: return "broken";
                break;
            case StatusValue.DISPATCHING: return "dispatching";
                break;
            default: throw "unknown asset status '"+ state +"'";
        }
    }

    function copy() { return Status(_state, _since); }

    function render();
}

class SingleStatus extends Status {
    _reason = null;
    _infoFn = null;

    constructor(state, additionalInfoFn = null) {
        if(additionalInfoFn) {
            _infoFn = additionalInfoFn;
        } else {
            _infoFn = function() { return {}; };
        }

        base.constructor(state);
    }

    function New(infoFn = null) {
        return SingleStatus(StatusValue.IDLE, infoFn);
    }

    function render() {
        local info = null;
        local state = value();

        if(state == StatusValue.IDLE || state == StatusValue.TORN_DOWN) {info = {}; }
        else { info = _infoFn(); }

        info.status <- name();
        info.since  <- printTimeSince();

        if(_reason) { info.reason <- _reason; }

        return info;
    }

    function initialising() { return _update(StatusValue.INITIALISING); }
    function running() { return _update(StatusValue.RUNNING); }
    function tearingDown() { return _update(StatusValue.TEARING_DOWN); }
    function tornDown() { return _update(StatusValue.TORN_DOWN); }
    function dispatching() { return _update(StatusValue.DISPATCHING); }

    function warning(reason=null) { return _update(StatusValue.WARNING, reason); }
    function broken(reason=null) { return _update(StatusValue.BROKEN, reason); }

    function _update(state, reason=null) {
        if(_state != state || _reason != reason) {
            _reason = reason;
            base.update(state);
        }

        if(state == StatusValue.TORN_DOWN) { _infoFn = null; }
        return this;
    }
}

class AggregatedStatus extends Status {
    _accessors = null;
    _infoFn = null;

    constructor(accessors = null, additionalInfoFn = null) {
        _accessors = accessors ? accessors : {};
        if(additionalInfoFn) {
            _infoFn = additionalInfoFn;
        } else {
            _infoFn = function() { return {}; };
        }

        foreach(key, statusFn in _accessors) {
            _registerSelfTo(statusFn)
        }

        local initialStatus = _evaluateStatus();
        base.constructor(initialStatus.value(), initialStatus.since());
    }

    function append(key, statusFn) {
        _accessors[key] <- statusFn;
        _registerSelfTo(statusFn);
        statusChanged();
    }

    function statusChanged(status = null) {
        local newStatus = _evaluateStatus();
        if(_state != newStatus.value()) {
            update(newStatus.value(), newStatus.since());
        }
    }

    function _registerSelfTo(statusFn) {
        local status = statusFn();
        status.register(statusChanged.bindenv(this));
    }

    function _evaluateStatus() {
        local ag = SingleStatus.New();

        foreach(key, statusFn in _accessors) {
            local status = null;
            try {
                status = statusFn();
            } catch(exception) {
                status = SingleStatus.New().broken("Exception: "+ exception);
            }
            ag = ag.add(status);
        }

        return ag;
    }

    function render() {
        if(_accessors.len() == 0) {
            local info = {};
            info.status <- name();
            info.since  <- printTimeSince();
            return info;
        } else {
            local agStatus = SingleStatus.New();
            local agRender = {};
            foreach (key, statusFn in _accessors) {
                local status = null;
                local render = null;
                try {
                    status = statusFn();
                    render = status.render();
                } catch (exception) {
                    status = SingleStatus.New().broken("Exception: " + exception);
                    render = status.render();
                }

                agStatus = agStatus.add(status);
                agRender[key] <- render;
            }
            agRender.status <- agStatus.name();

            local state = agStatus.value();
            if(state != StatusValue.IDLE && state != StatusValue.TORN_DOWN) {
                local info = _infoFn();
                foreach(key, value in info) {
                    agRender[key] <- value;
                }
            }

            return agRender;
        }
    }
}
// todo move from driver to top level device
class StatusIndicator {
    _led = null
    _status = null;
    _connected = true;

    constructor(lp5521Driver) {
        _led = lp5521Driver;
        _status = StatusValue.IDLE;
    }

    function init() { _led.init(); }

    function onConnection(connected) {
        _connected = connected;
        _show();
    }

    function statusChanged(status) {
        _status = status.value();
        _show();
    }

    function _show() {
        if(!_connected) { _led.blink(LED_LIGHT.RED); }
        else {
            switch(_status) {
                case StatusValue.BROKEN:
                    _led.on(LED_LIGHT.RED);
                    break;
                case StatusValue.WARNING:
                    _led.on(LED_LIGHT.YELLOW);
                    break;
                case StatusValue.RUNNING:
                    _led.on(LED_LIGHT.GREEN);
                    break;
                case StatusValue.DISPATCHING:
                    _led.on(LED_LIGHT.BLUE);
                    break;
//            case StatusValue.TEARING_DOWN:
//            case StatusValue.TORN_DOWN:
//            case StatusValue.IDLE:
                default: _led.off();
            }
        }
    }
}
//const SPI_FLASH_SIZE = 40960;    // 10 * 4 * 1024

const RECORDER_SEND_LIMIT = 40; // half of ReadingsSender.RetryLimit
const RECORDER_BUFF_LIMIT = 1950; // limit no of records held in processor memory so device won't blow up

class Recorder {
    ResendLimit = null;
    BufferLimit = null;
    _batchInterval = 1;
    _reSendInterval = 1;
    _records = null;
    _sendInterval = null;
    _pqMode = null;
    _sysDebug = null;
    _sendTimer = null;
    _batchTimer = null;
    _reSendTimer = null;
    _sysTimer = null;
    _logger = null;
    _toReSend = null;
    _recoveryStart = false;
    _recoveryStartTime = 0;
    _numRecovered = 0;
    _readingsBackoff = false;

    constructor(resendLimit = RECORDER_SEND_LIMIT, bufferLimit = RECORDER_BUFF_LIMIT) {

        ResendLimit = resendLimit;
        BufferLimit = bufferLimit;
        _sendInterval = 1;
        _pqMode = false;
        _sysDebug = false;
        _records = [];
        _logger = KiwiSPIFlashLogger( 0x1000 );
        _toReSend = [];
    }

    function configure(sendIntervalInSec = 1, prequalification = false, systemDebug = false) {
        _sendInterval = sendIntervalInSec;
        _pqMode = prequalification;
        _sysDebug = systemDebug;
    }

    function start() {
        _sendTimer = imp.wakeup(0, _sendReadings.bindenv(this));
        _batchTimer = imp.wakeup(0, _batchFlashReadings.bindenv(this));
        _reSendTimer = imp.wakeup(0, _reSendReadings.bindenv(this));
        if(_sysDebug) { imp.wakeup(0, _sendSysInfo.bindenv(this)); }
        local dimensions = _logger.dimensions();
        logger.log(">>> SPI flash allocated to readings recorder: "+dimensions.len+" bytes");
        if (_logger.first() != null) { logger.log("    Readings present in SPI flash"); }
    }

    function stop() {
        _sendTimer = cancelTimer(_sendTimer);
        _batchTimer = cancelTimer(_batchTimer);
        _reSendTimer = cancelTimer(_reSendTimer);
        _sysTimer = cancelTimer(_sysTimer);
    }

    function eraseSpiFlash() {
        logger.log(">>> erasing SPI flash");
        _logger.eraseAll();
        return {};
    }

    function readingsBackoff(backoff) {
        _readingsBackoff = backoff;
    }

    function hasReadings() { return _records.len() > 0; }
    function hasReadingsToResend() { return _toReSend.len() > 0; }
    function inPrequalification() { return _pqMode; }
    function values(id, values) { _record(id, values); }

    // --- custom recording functions
    /*
        convention on units:
        frequency in mHz integer
        stateOfCharge in 100 percentage 1dp
        power in kW 3dp: targetPower, calcTargetPower, adjustedTargetPower, powerStep, offsetPower
     */

    function pulseCounting(id, frequency) { _record(id, { pulseFrequency = frequency }); }
    function pulseTiming(id, interval) { _record(id, { pulseInterval = interval }); }

    function digitalInput(connection, state) {
        switch (connection) {
            case PulseInputs.PULSE1 :
                _record(connection, {P1 = state});
                break;
            case PulseInputs.PULSE2 :
                _record(connection, {P2 = state});
                break;
            case PulseInputs.PULSE3 :
                _record(connection, {P3 = state});
                break;
            case PulseInputs.PULSE4 :
                _record(connection, {P4 = state});
                break;
            default :
                throw "Missing pin mapping for " + connection
                break;
        }
    }

    function _agendSend(toSend) { return agent.send("readings.send.records", toSend); }

    function _sendReadings() {
        if (hasReadings()) {
            local toSend = _records.slice(0, min(_records.len(), RECORDER_SEND_LIMIT));
            if (_isReadingsBackoffActive()) {
                _logger.write(toSend);
            } else {
                local sent = _agendSend(toSend);
                if (sent != 0) { _logger.write(toSend) }
            }
            _records = _records.slice(toSend.len());
        }
        _sendTimer = imp.wakeup(_sendInterval, _sendReadings.bindenv(this));
    }

    function _isReadingsBackoffActive() { return _readingsBackoff; }

    function _batchFlashReadings() {
        if (_toReSend.len() < ResendLimit) {
            _logger.read(
                function(toSend, address, next) {
                    if (_toReSend.len() < ResendLimit) {
                        _toReSend.extend(toSend);
                        _logger.erase(address);
                        imp.wakeup(0, next);
                    } else {
                        _batchTimer = imp.wakeup(_batchInterval, _batchFlashReadings.bindenv(this));
                    }
                }.bindenv(this),
                function() {
                    _batchTimer = imp.wakeup(_batchInterval, _batchFlashReadings.bindenv(this));
                    if (_recoveryStart) {
                        local elapsedTime = time() - _recoveryStartTime;
                        logger.log("   Readings recovery completed: " + _numRecovered + " readings in " + elapsedTime + "s");
                        _recoveryStart = false;
                        _numRecovered = 0;
                    }
                }.bindenv(this)
            );
        } else {
            _batchTimer = imp.wakeup(_batchInterval, _batchFlashReadings.bindenv(this));
        }
    }

    function _reSendReadings() {
        if (!_isReadingsBackoffActive()) {
            if (_toReSend.len()) {
                local toSend = clone _toReSend;
                local sent = _agendSend(toSend);
                if (sent == 0) {
                    local numberSent = _toReSend.len();
                    _numRecovered += numberSent;
                    local timestamp = _toReSend[0].timestamp;
                    _toReSend.clear();
                    if (!_recoveryStart) {
                        _recoveryStart = true;
                        _recoveryStartTime = time();
                    }
                    local ts = timestamp.slice(0, timestamp.len() - 3).tointeger();
                    local ms = timestamp.slice(timestamp.len() - 3).tointeger();
                    logger.log("   Recover "+ numberSent +" readings from "+ printTimestamp(date(ts)) + "." + format("%03i", ms) );
                }
            }
        }
        _reSendTimer = imp.wakeup(_reSendInterval, _reSendReadings.bindenv(this));
    }

    function _record(id, reading) {
        if(_sendTimer) {
            // todo send id along with each record once readings service can handle it
            if(inPrequalification()) {
                _records.append({
                    timestamp = kwTime.get(),
                    values = reading
                });
            } else {
                _records.append({
                    timestamp = kwTime.getImpTime(),
                    values = reading
                });
            }

            if(_records.len() > BufferLimit) { _records.remove(0); logger.error("Recorder buffer limit exceeded"); }
        }
    }

    function _sendSysInfo() {
        _sysTimer = imp.wakeup(1, _sendSysInfo.bindenv(this));
        local values = { memFree = imp.getmemoryfree(), deviceTime = time() };

        if(inPrequalification()) {
            agent.send("readings.send.sysinfo", { timestamp = kwTime.get(), values = values });
        } else {
            agent.send("readings.send.sysinfo", { timestamp = kwTime.getImpTime(), values = values });
        }
    }
}

class Comms {
    // todo function id()
    // todo function name()
    function stop() { throw "comms.stop() unimplemented"; }
    function status() { throw "comms.status() unimplemented"; }
}

class Input {
    _id     = null;
    _name   = null;
    _record = null;

    constructor(id, name, recorder) {
        _id = id;
        _name = name;
        _record = recorder;
    }

    function id() { return _id; }
    function name() { return _name; }
    function status() { throw "input.status() unimplemented"; }
    function stop() { throw "input.stop() unimplemented"; }
}

class FailedInput extends Input {
    _name = null;
    _status = null;

    constructor(id, name, error) {
        base.constructor(id, name, null);
        _status = SingleStatus.New().broken("Failed construction: " + error);
    }

    function status() { return _status; }
    function stop() { _status.tornDown(); }
}

class Control {
    function start(comm, detail) { throw "ctrl.start() unimplemented"; }
    function stop() { throw "ctrl.stop() unimplemented"; }
}

class Asset {
    _id = null;
    _name = null;

    constructor(id, name) {
        _id = id;
        _name = name;
    }

    function id() { return _id; }
    function name() { return _name; }
    // todo function type()
    function status() { throw "asset.status() unimplemented"; }
    function stop() { throw "asset.stop() unimplemented"; }
}

class FailedAsset extends Asset {
    _status = null;

    constructor(id, name, error) {
        base.constructor(id, name);
        _status = SingleStatus.New().broken("Failed construction: " + error);
    }

    function status() { return _status; }
    function stop() { _status.tornDown(); }
}

class Bridge {
    _id = null;
    _name = null;

    constructor(id, name) {
        _id = id;
        _name = name;
    }

    function id() { return _id; }
    function name() { return _name; }
    // todo function type()
    function status() { throw "bridge.status() unimplemented"; }
    function stop() { throw "bridge.stop() unimplemented"; }
}

class FailedBridge extends Bridge {
    _status = null;

    constructor(id, name, error) {
        base.constructor(id, name);
        _status = SingleStatus.New().broken("Failed construction: " + error);
    }

    function status() { return _status; }
    function stop() { _status.tornDown(); }
}

logger <- {
    function register() {
        local data = {
            device_group_name   = format("%s", __EI.DEVICEGROUP_NAME),
            device_group_type   = format("%s", __EI.DEVICEGROUP_TYPE),
            device_group_id     = format("%s", __EI.DEVICEGROUP_ID),
            product_name        = format("%s", __EI.PRODUCT_NAME),
            device_id           = hardware.getdeviceid()
        }
        agent.send("logger.register", data);
    },
    function log(msg) {
        agent.send("logger.log", msg);
        return msg;
    },
    function error(msg) {
        agent.send("logger.error", msg);
        return msg;
    }
}

//use logger.log(msg) rathe rthan # msg can be string or table/json
logger.register()

// Comms

class BinaryComms extends Comms {
    function isDispatching();
    function dispatch();
    function endDispatch();
}

class OnBoardRelay extends BinaryComms {
    _relayNumber = null;
    _relay = null;
    _status = null;
    constructor(relayNumber) {
        _relayNumber = relayNumber;
        _relay = RelayFactory.build(relayNumber);
        _status = SingleStatus.New(function() {
            return {
                type = "relay",
                number = _relayNumber,
                state = (isDispatching() ? "on" : "off") }
        }.bindenv(this)).running();
    }

    function isDispatching() { return _relay.isClosed(); }
    function dispatch() { _relay.setState(RelayState.CLOSED); }
    function endDispatch() { _relay.setState(RelayState.OPEN); }
    function status() { return _status; }

    function stop() {
        endDispatch();
        _status.tornDown();
        _relay = null;
    }
}


// Modbus status bits
const MODBUS_ETHERNET_CABLE_PLUG_IN     = 0x01;
const MODBUS_CONNECTION_ACTIVE          = 0x02;
const MODBUS_SERVER_NOT_AVAILABLE       = 0x04;

class ModbusTcpClientFactory {
    /*
        conf: modbus vendor, model and connection details
        wiz: Wiznet instance
    */
    function build(conf, wiz) {
        local vendorModel = conf.vendor + conf.model;
        local modbusMap = tableGet("modbusMap", conf);
        local device = null;

        // todo split between vendor+model vs mapped modbus
        switch (vendorModel.toupper())  {
            case (VENDOR_NEC + MODEL_AEROS_GBS):
                device = NecModbusTcpClient(conf, wiz);
                break;
            case (VENDOR_MAP + MODEL_BATT):
                device = BatteryMappedModbusTcpClient(conf, wiz, modbusMap);
                break;
            case (VENDOR_MAP + MODEL_METER):
                device = MeterMappedModbusTcpClient(conf, wiz, modbusMap);
                break;
            case (VENDOR_MAP + MODEL_POWERON):
                device = PoweronMappedModbusTcpClient(conf, wiz, modbusMap);
                break;
            case (VENDOR_TESLA + MODEL_POWERPACK):
                device = TeslaModbusTcpClient(conf, wiz);
                break;
            case (VENDOR_BYD + MODEL_EPRI):
                device = BydModbusTcpClient(conf, wiz);
            break;
            case (VENDOR_CE + MODEL_STACKBATT):
                device = ConnectedEnergyModbusTcpClient(conf, wiz);
            break;
            default:
                // todo should it be a 'plain' modbus instead of being fake?
                // todo or just ... null?
                device = FakeModbusTcpClient(conf, wiz);
                break;
        }
        return device;
    }
}
//
// Exceptions used by all modbus class
//
enum ModbusExceptionCodes {
    ILLEGAL_FUNCTION     = 1,
    ILLEGAL_DATA_ADDR    = 2,
    ILLEGAL_DATA_VAL     = 3,
    SLAVE_DEVICE_FAIL    = 4,
    ACKNOWLEDGE          = 5,
    SLAVE_DEVICE_BUSY    = 6,
    NEGATIVE_ACKNOWLEDGE = 7,
    MEMORY_PARITY_ERROR  = 8,
    GATEWAY_PATH_UNAVAILABLE = 10,
    RESPONSE_TIMEOUT     = 0x50,
    INVALID_CRC          = 0x51,
    INVALID_ARG_LENGTH   = 0x52,
    INVALID_DEVICE_ADDR  = 0x53,
    INVALID_TARGET_TYPE  = 0x57,
    INVALID_VALUES       = 0x58,
    INVALID_QUANTITY     = 0x59
}

class ModbusExceptions {
    static function decode(error) {
        switch (error) {
            case null: return error;
            case ModbusExceptionCodes.ILLEGAL_FUNCTION: return "Modbus error: ILLEGAL_FUNCTION";
            case ModbusExceptionCodes.ILLEGAL_DATA_ADDR: return "Modbus error: ILLEGAL_DATA_ADDR";
            case ModbusExceptionCodes.ILLEGAL_DATA_VAL: return "Modbus error: ILLEGAL_DATA_VAL";
            case ModbusExceptionCodes.SLAVE_DEVICE_FAIL: return "Modbus error: SLAVE_DEVICE_FAIL";
            case ModbusExceptionCodes.ACKNOWLEDGE: return null; /* ACKNOWLEDGE is not actually an error */
            case ModbusExceptionCodes.SLAVE_DEVICE_BUSY: return "Modbus error: SLAVE_DEVICE_BUSY";
            case ModbusExceptionCodes.NEGATIVE_ACKNOWLEDGE: return "Modbus error: NEGATIVE_ACKNOWLEDGE";
            case ModbusExceptionCodes.MEMORY_PARITY_ERROR: return "Modbus error: MEMORY_PARITY_ERROR";
            case ModbusExceptionCodes.GATEWAY_PATH_UNAVAILABLE: return "Modbus error: GATEWAY_PATH_UNAVAILABLE";
            case ModbusExceptionCodes.RESPONSE_TIMEOUT: return "Modbus error: RESPONSE_TIMEOUT";
            case ModbusExceptionCodes.INVALID_CRC: return "Modbus error: INVALID_CRC";
            case ModbusExceptionCodes.INVALID_ARG_LENGTH: return "Modbus error: INVALID_ARG_LENGTH";
            case ModbusExceptionCodes.INVALID_DEVICE_ADDR: return "Modbus error: INVALID_DEVICE_ADDR"
            case ModbusExceptionCodes.INVALID_TARGET_TYPE: return "Modbus error: INVALID_TARGET_TYPE";
            case ModbusExceptionCodes.INVALID_VALUES: return "Modbus error: INVALID_VALUES";
            case ModbusExceptionCodes.INVALID_QUANTITY: return "Modbus error: INVALID_QUANTITY";
            default: return "Modbus error: " + error;
        }
    }
}

enum ModbusActionType {
    READ_TEST  = "RT",
    WRITE_TEST = "WT",
    DIAGNOSTIC = "DN",
    DISCONNECT = "DC",
    HEARTBEAT  = "HB",
    SET_POWER  = "SP",
    READ_SOC   = "RS",
    READ_POWER = "RP",
    READ_FREQUENCY = "RF"
}

class ModbusClient extends Comms {

    function testEnabled() { return _testEnabled; }

    // ---- Abstract functions to implement by derived class ---- //
    function stop();

    /*
        return array of registers, for example:
        [
            // Name                       |  Address                                          | Quantity
            ["systemestop",               NecAerosReadAddress.SYSTEM_ESTOP,                   1],
            ["groupmode" ,                NecAerosReadAddress.GROUP_MODE,                     1]
        ]
     */
    function diagnosticRegisters();

    // -------------------- PRIVATE METHODS -------------------- //
    function readTest(registerType, address, quantity, resultCb) {
        local fn = function() {
            _modbus.read(registerType, address, quantity, _onReceive(resultCb).bindenv(this));
        }.bindenv(this);
        _appendActionQueue(ModbusActionType.READ_TEST, fn);
    }

    function writeTest(registerType, address, quantity, values, resultCb) {
        local fn = function() {
            _modbus.write(registerType, address, quantity, values, _onReceive(resultCb).bindenv(this));
        }.bindenv(this);
        _appendActionQueue(ModbusActionType.WRITE_TEST, fn);
    }

    function runDiagnostics(callback, readRegisters=null) {
        local registers = readRegisters ? readRegisters : diagnosticRegisters();

        if(registers.len() > 0) {
            registers.reverse();

            _diagnostic.readRegisters <- registers;
            _diagnostic.callback      <- callback;
            _diagnostic.result        <- [];

            imp.wakeup(0, _diagnosticRead.bindenv(this));
        } else {
            callback([]);
        }
    }

    function _diagnosticRead() {
        local fn = function() {
            local next = _diagnostic.readRegisters.pop();
            _diagnostic.result.append(next);
            _modbus.read(MODBUSRTU_TARGET_TYPE.HOLDING_REGISTER, next[1], next[2],
            _onReceive(_diagnosticReadResult).bindenv(this));
        }.bindenv(this);

        _appendActionQueue(ModbusActionType.DIAGNOSTIC, fn);
    }

    function _diagnosticReadResult(error, result) {
        _diagnostic.result.top().append(error ? error : result);

        if(_diagnostic.readRegisters.len() > 0) {
            imp.wakeup(0.1, _diagnosticRead.bindenv(this));
        } else {
            _diagnostic.callback(_diagnostic.result);
            _diagnostic.clear();
        }
    }

    function _existsInQueue(actionType) {
        foreach(a in _actionQueue) {
            if(a.type == actionType) { return true; }
        }
        return false;
    }

    function _appendActionQueue(actionType, actionFn) {
        _actionQueue.append({ type = actionType, action = actionFn });
        imp.wakeup(0, _executeNextAction.bindenv(this));
    }

    function _appendQueueIfNotExist(actionType, actionFn) {
        if(!_existsInQueue(actionType)) {
            _appendActionQueue(actionType, actionFn);
        }
    }

    function _prependQueueIfNotExist(actionType, actionFn) {
        if(!_existsInQueue(actionType)) {
            _actionQueue.insert(0, { type = actionType, action = actionFn });
            imp.wakeup(0, _executeNextAction.bindenv(this));
        }
    }

    function _executeNextAction() {
        if(!_busy && _actionQueue.len() > 0) {
            _busy = true;
            local next = _actionQueue.remove(0);
            next.action();
        }
    }

    function _onReceive(callback) {
        return function(error, result) {
            _busy = false;
            callback(ModbusExceptions.decode(error), result);
            imp.wakeup(0, _executeNextAction.bindenv(this));
        }
    }

    function _onReceiveWithAction(callback, action) {
        return function(error, result) {
            _busy = false;
            callback(action, ModbusExceptions.decode(error), result);
            imp.wakeup(0, _executeNextAction.bindenv(this));
        }
    }
}

class ModbusDoctor {
    _modbus = null;
    _testMode = null;

    constructor(modbus) {
        _modbus = modbus;
        _testMode = modbus.testEnabled();
    }

    function runDiagnostics(resultCb) {
        if("runDiagnostics" in _modbus) {
            _modbus.runDiagnostics(resultCb);
        } else {
            throw "Modbus run diagnostics not available";
        }
    }

    function readTest(registerType, address, quantity, resultCb) {
        if(!_testMode && registerType != MODBUSRTU_TARGET_TYPE.HOLDING_REGISTER) {
            throw "Forbidden register type - only HOLDING_REGISTER allowed when not in test mode"
        }
        _modbus.readTest(registerType, address, quantity, resultCb);
    }

    function writeTest(registerType, address, quantity, values, resultCb) {
        if(!_testMode && registerType != MODBUSRTU_TARGET_TYPE.HOLDING_REGISTER) {
            throw "Forbidden register type - only HOLDING_REGISTER allowed when not in test mode"
        }
        _modbus.writeTest(registerType, address, quantity, values, resultCb);
    }
}

// Modbus status bits
const MODBUS_ETHERNET_CABLE_PLUG_IN     = 0x01;
const MODBUS_CONNECTION_ACTIVE          = 0x02;
const MODBUS_SERVER_NOT_AVAILABLE       = 0x04;

class ModbusTcpClient extends ModbusClient {
    _wiz                = null;
    _modbus             = null;
    _connectivityStatus = 0;
    _statusTimer        = null;
    _connectionDetails  = null;
    _diagnostic         = null;
    _debug              = false;
    _testEnabled        = false;

    _busy               = false;
    _actionQueue        = null;
    _slave              = 0;

    constructor(commConfig, wiz) {
        _connectionDetails = {};
        _connectionDetails.serverIpAddress  <- commConfig.serverIpAddress;
        _connectionDetails.serverPort       <- commConfig.serverPort;

        if ("debug" in commConfig) {
            _debug = commConfig.debug;
        }
        if("testEnable" in commConfig) {
            _testEnabled = commConfig.testEnable;
        }
        if ("slave" in commConfig) {
            _slave = commConfig.slave;
        }
        _wiz = wiz;
        _diagnostic = {};
        _actionQueue = [];
        _connect();
    }

    function testEnabled() { return _testEnabled; }

    function deinitialise() {
        if (connectivityStatus() & MODBUS_CONNECTION_ACTIVE) {
            _queueDeInit();
            _queueDisconnect();
        } else {
            _postDisconnect();
        }
    }

    function _queueDisconnect() {
        _appendActionQueue(ModbusActionType.DISCONNECT, function() { disconnect(); }.bindenv(this));
    }

    function disconnect() {
        if(_modbus) {
            _modbus.disconnect(function() {
                _postDisconnect();
            }.bindenv(this));
        }
    }

    // ---- Abstract functions to implement by derived class ---- //
    function onConnected();
    function onReconnected();
    function onDisconnected();
    function onConnectionError(error);
    function onReconnectionError(error);
    function _queueDeInit();

    // -------------------- PRIVATE METHODS -------------------- //
    function _connect() {
        _modbus = KiwiModbusTCPMaster(_wiz, _debug, _slave);

        _modbus.connect({"destIP":_connectionDetails.serverIpAddress, "destPort":_connectionDetails.serverPort},
            _connectionCallback("Connection", onConnectionError, onConnected).bindenv(this),
            _connectionCallback("Reconnection", onReconnectionError, onReconnected).bindenv(this));

        imp.wakeup(0, _trackConnectivity.bindenv(this));
    }

    function _connectionCallback(label, onError, onSuccess) {
        return function(error, conn) {
            logger.log(format("server %s, port %d", _connectionDetails.serverIpAddress, _connectionDetails.serverPort));

            if (error) {
                logger.error(label + " error: "+ error);
                _connectivityStatus = _connectivityStatus & ~MODBUS_CONNECTION_ACTIVE;
                onError(error);
            } else {
                logger.log(label + " successful");
                _connectivityStatus = _connectivityStatus | MODBUS_CONNECTION_ACTIVE;
                onSuccess();
            }
        }
    }

    function _trackConnectivity() {
        local status = 0;
        if (_wiz.isPhysicallyConnected()) {
            status = _connectivityStatus | MODBUS_ETHERNET_CABLE_PLUG_IN;
        } else {
            status = _connectivityStatus & ~MODBUS_ETHERNET_CABLE_PLUG_IN;
        }

        if ( _connectivityStatus != status ) {
            logger.log(format("ModbusTcpClient::connectivityStatus: 0x%02X ", status) + this);
            _connectivityStatus = status;
        }

        _statusTimer = imp.wakeup(1.0, _trackConnectivity.bindenv(this));
    }

    function connectivityStatus() {
        return _connectivityStatus;
    }

    function _postDisconnect() {
        _connectivityStatus = _connectivityStatus & ~MODBUS_CONNECTION_ACTIVE;
        // NOTE: at this point, _connectivityStatus should be 1, otherwise something is not right
        logger.log(format("ModbusTcpClient::_postDisconnect connectivityStatus: 0x%02X ", _connectivityStatus) + this);
        onDisconnected();
        _statusTimer = cancelTimer(_statusTimer);
    }
}

const SIGNED_BIT_FLAG  = 0x8000;
const SIGNED_BITS_MASK = 0x7FFF;

enum MappedModbusTcpClientConversionType {
    FP = "IEEE-754 Floating Point",
    TC = "Twos Complement"
};

class MappedModbusTcpClient extends ModbusTcpClient {
    _map = null;
    _endianness = null;
    _status = null;

    constructor(commConfig, wiz, modbusMap) {
        _initMapping(modbusMap);

        _status = SingleStatus.New(function() {
            return { connectivity = connectivityStatus()};
        }.bindenv(this)).initialising();

        base.constructor(commConfig, wiz);
    }

    function configKeyToModbusAction(key);

    function _initMapping(map) {
        _map = {};
        _endianness = tableGetOrElse("endianness", map, "LITTLE").toupper();
        foreach(k, v in map) {
            local action = configKeyToModbusAction(k);
            if(action) {
                local m = {};
                m.address  <- v.registerAddress;
                m.quantity <- v.registerSizeBits / 16;
                m.scale <- tableGetOrElse("unitScaling", v, 1);
                m.conversion <- tableGetOrElse("conversion", v, MappedModbusTcpClientConversionType.TC);
                m.direction <- tableGetOrElse("powerDirection", v, 1);
                m.maxValue <- tableGet("maxRegisterValue", v);
                _map[action] <- m;
            } else {
                logger.log("WARN: Unknown modbus map: "+ k);
            }
        }
    }

    function status() { return _status; }

    function stop() {
        _status.tearingDown();
        base.deinitialise();
    }

    function onDisconnected() {
        _status.tornDown();
    }

    function onConnected() {
        _status.running();
    }

    function onConnectionError(error) {
        logger.log("MappedModbusTcpClient::onConnectionError " + error + " " + this);
        _status.broken("Connection error: " + error);
    }

    function onReconnected() {
        _status.running();
    }

    function onReconnectionError(error) {
        logger.log("MappedModbusTcpClient::onReconnectionError " + error + " " + this);
        _status.broken("Reconnection error: " + error);
    }

    function _write(action, value) {
        local props = tableGet(action, _map);

        if(props != null) {
            local scaled = value;
            local scale = tableGet("scale", props);
            // signed 32 bit value - translate to (two contiguous 16 bits values, lower register is MSB)
            scaled = (value * props.scale * props.direction).tointeger();
            if(props.maxValue != null && scaled > props.maxValue) { scaled = props.maxValue; }
            _queueAction(action, function() {
                _modbus.write(MODBUSRTU_TARGET_TYPE.HOLDING_REGISTER, props.address, props.quantity,
                    _formatWrite(scaled, props.quantity), _onReceiveWithAction(_logWriteError(), action).bindenv(this));
            }.bindenv(this));
        }
    }

    function _formatWrite(value, quantity) {
        if (typeof value == "integer" && quantity == 2) {
            local b = blob();
            b.writen((value >> 16), 'w');
            b.writen((value & 0xFFFF), 'w');
            if (_endianness == "BIG") { b.swap2(); }
            return b;
        } else {
            return value;
        }
    }

    function _logWriteError() {
        return function(action, decodedError, result) {
            if (decodedError) {
                local msg = action + " error: " + decodedError;
                logger.error(msg);
                _status.broken(msg);
            } else {
                _status.running();
            }
        }
    }

    function _fetchReading(action, cb) {
        local props = tableGet(action, _map);
        if(props) {
            _queueAction(action, function() {
                _modbus.read(MODBUSRTU_TARGET_TYPE.HOLDING_REGISTER, props.address, props.quantity, _onReceiveWithAction(_readThenCallback(cb), action).bindenv(this));
            }.bindenv(this));
        }
    }

    function _readThenCallback(valueCb) {
        return function(action, error, result) {
            if (error) { _status.broken(action + " error: " + error);
            } else {
                local props = tableGet(action, _map);
                if(props) {
                    valueCb(_readResult(result, props) * props.scale * props.direction);
                }
                _status.running();
            }
        }
    }

    function _readResult(result, props) {
        if (tableGet("scale", props)) {
            local value = result.len() == 2 ? ((result[0] << 16) + result[1]): result[0];
            switch (props.conversion) {
                case MappedModbusTcpClientConversionType.FP:
                    result = casti2f(value);
                    break;

                case MappedModbusTcpClientConversionType.TC:
                default:
                    result = (value && value & SIGNED_BIT_FLAG ? -((~value & SIGNED_BITS_MASK) + 1) : value);
                    break;
            }
        }
        return result;
    }
};


enum MeterMappedModbusyActionType {
    READ_POWER = "RP",
    READ_FREQUENCY = "RF"
}

class MeterMappedModbusTcpClient extends MappedModbusTcpClient {
    constructor(commConfig, wiz, modbusMap) {
        base.constructor(commConfig, wiz, modbusMap);
        logger.log("MeterMappedModbusTcpClient constructor" );
    }

    function readActualPower(valueCb) {
        _fetchReading(MeterMappedModbusyActionType.READ_POWER , valueCb); // we have to deducte 1 from reg address
    }

    function readFrequency(valueCb) {
        _fetchReading(MeterMappedModbusyActionType.READ_FREQUENCY , valueCb); // we have to deducte 1 from reg address
    }

    function configKeyToModbusAction(key) {
        switch(key) {
            case "readActualPower": return MeterMappedModbusyActionType.READ_POWER;
            case "readFrequency": return MeterMappedModbusyActionType.READ_FREQUENCY;
            default: return null;
        }
    }

    function onConnected() {
        base.onConnected()
    }

    function stop() {
        base.stop();
    }

    function _queueAction(actionType, fn) {
        switch (actionType) {
            default: _appendQueueIfNotExist(actionType, fn);
        }
    }
};

//
// class FakeModbusTcpClient
//
// handles default condition when vendor is not supplied
// during instantiation of a class.
//
class FakeModbusTcpClient extends ModbusTcpClient {
    _toggleHeartbeat   = 0;
    _heartbeatTimer    = null;
    _commsStatus       = null;

    constructor(commConfig, wiz) {
        // todo additional info to say what type of ModbusTcpClient it is???
        _commsStatus = SingleStatus.New(function() {
            return { connectivity = connectivityStatus()};
        }.bindenv(this)).initialising();
        base.constructor(commConfig, wiz);
    }

    function status() {
        return _commsStatus;
    }

    function stop() {
        base.disconnect();
    }

    function onConnected() {
        _commsStatus.running();
    }

    function onDisconnected() {
        _commsStatus.tornDown();
    }

    function onReconnected();
    function onConnectionError(error);
    function onReconnectionError(error);

    function getStateOfCharge() {
        return 50;
    }

    function realPowerSetPoint(power) {
//        logger.log("Setting power to: " + power);
    }
};


/*
   BYD EPRI class implementation
 */

// REGISTER ADDRESS MAP
enum BydRegisterAddress {
    SYSTEM_STATE_CONTROL = 1280,
    SYSTEM_ACTIVE_POWER = 1281,
    SYSTEM_WORK_STATE  = 14496,
    SYSTEM_AVAILABLE = 14504       // aka STATE_OF_CHARGE
}

// SYSTEM STATE CONTROL VALUE
enum  BydStateControlValue {
    STATE_CONTROL_STOP = 2,
    STATE_CONTROL_RUN  = 6,
}

enum BydModbusBatteryActionType {
    WORK_STATE,
    STATE_CONTROL,
    SET_POWER,
    SOC
}


class BydModbusTcpClient extends ModbusTcpClient {
    _commsStatus            = null;
    _inOperation            = false;
    _soc                    = null;
    _power                  = 0;
    _readSoCTimer           = null;
    _stateControlTimer      = null;

    constructor(commConfig, wiz) {
        _commsStatus = SingleStatus.New(function() {
            return { connectivity = connectivityStatus()};
        }.bindenv(this)).initialising();

        base.constructor(commConfig, wiz);
    }

    function status() {
        return _commsStatus;
    }

    function stop() {
        _commsStatus.tearingDown();
        _stopTimers();
        _inOperation = false;

        base.deinitialise();
    }


    function onConnected() {
        _initSequence();
    }

    function onConnectionError(error) {
        logger.error("BydModbusTcpClient::onConnectionError "+ error + " "+ this);
        _commsStatus.broken("Connection error: "+ error);
    }

    function onDisconnected() {
        _commsStatus.tornDown();
    }

    function onReconnected() {
        _commsStatus.running();
        // todo check whether to restart heartbeat after regaining connection as it was stopped onReconnectionError
    }

    function onReconnectionError(error) {
        logger.error("BydModbusTcpClient::onReconnectionError " + error + " "+ this);
        _commsStatus.broken("Reconnection error: "+ error);
        _stopTimers();
    }

    function getStateOfCharge() {
        return _soc;
    }

    function realPowerSetPoint(power) {
        _power = power;
        _queueAction(BydModbusBatteryActionType.SET_POWER, function() {
            _modbus.write(MODBUSRTU_TARGET_TYPE.HOLDING_REGISTER, BydRegisterAddress.SYSTEM_ACTIVE_POWER, 1,
                _nextTargetPowerValue(), _onReceive(_logWriteError("SYSTEM_ACTIVE_POWER")).bindenv(this));
        }.bindenv(this));
    }

// -------------------- PRIVATE METHODS -------------------- //
    function _stopTimers() {
        _readSoCTimer = cancelTimer(_readSoCTimer);
    }

    function _nextTargetPowerValue() {
        return _power.tointeger();
    }

    function _logWriteError(cmd) {
        return function(decodedError, result) {
            if (decodedError) {
                local msg = cmd + " write error: " + decodedError;
                logger.error(msg);
                _commsStatus.broken(msg);
            } else {
                if(_inOperation) _commsStatus.running();
            }
        }
    }

    function _queueDeInit() {
        _resetPower();
        _systemStateControl(BydStateControlValue.STATE_CONTROL_STOP);
        _systemWorkState();
    }

    function _initSequence() {
        _systemStateControl(BydStateControlValue.STATE_CONTROL_RUN);
        _systemWorkState();
    }

    function _resetPower() {
        _queueAction(BydModbusBatteryActionType.SET_POWER, function() {
            _modbus.write(MODBUSRTU_TARGET_TYPE.HOLDING_REGISTER, BydRegisterAddress.SYSTEM_ACTIVE_POWER, 1,
                0, _onReceive(_logWriteError("SET_POWER")).bindenv(this));
        }.bindenv(this));
    }


    function _systemStateControl(state) {
        _queueAction(BydModbusBatteryActionType.STATE_CONTROL, function() {
        _modbus.write(MODBUSRTU_TARGET_TYPE.HOLDING_REGISTER, BydRegisterAddress.SYSTEM_STATE_CONTROL, 1,
                      state, _onReceive(_logWriteError("SYSTEM_STATE_CONTROL")).bindenv(this));
        }.bindenv(this));
    }

    function _systemWorkState() {
        _queueAction(BydModbusBatteryActionType.WORK_STATE, function() {
            _modbus.read(MODBUSRTU_TARGET_TYPE.HOLDING_REGISTER, BydRegisterAddress.SYSTEM_WORK_STATE, 1,
                _onReceive(_initialisationCompleteCb).bindenv(this));
        }.bindenv(this));
    }

    function _readStateOfCharge() {
        _readSoCTimer = imp.wakeup(1, _readStateOfCharge.bindenv(this));
        _queueAction(BydModbusBatteryActionType.SOC, function() {
            _modbus.read(MODBUSRTU_TARGET_TYPE.HOLDING_REGISTER, BydRegisterAddress.SYSTEM_AVAILABLE, 1,
                _onReceive(_readStateOfChargeCallback).bindenv(this));
        }.bindenv(this));
    }

    function _readStateOfChargeCallback(decodedError, result) {
        if (decodedError) {
            _commsStatus.broken("SoC read error: " + decodedError);
        } else {
            _soc = result[0]/10;
            _commsStatus.running();
            logger.log("_readStateOfChargeCallback: " + _soc);
        }
    }

    function _initialisationCompleteCb(decodedError, result) {
        if (decodedError) {
            logger.error("BydModbusTcpClient::_initialisationCompleteCb: error " + decodedError);
            _commsStatus.broken("initialisation error: " + decodedError);
        } else {
            switch (result[0]){
                case BydStateControlValue.STATE_CONTROL_RUN:
//                    logger.log("BydStateControlValue.STATE_CONTROL_RUN");
                    imp.wakeup(0, _readStateOfCharge.bindenv(this));
                    _commsStatus.running();
                    _inOperation = true;
                    break;
                case BydStateControlValue.STATE_CONTROL_STOP:
//                    logger.log("BydStateControlValue.STATE_CONTROL_STOP");
                    _inOperation = false;
                    break;
                default:
//                    logger.log("BydStateControlValue.STATE_CONTROL_DEFAULT");
                    imp.wakeup(5, _systemWorkState.bindenv(this));
                    break;
            }

        }
    }

    /*
        Set power  always have priority
      */
    function _queueAction(actionType, fn) {
        switch (actionType) {
            case BydModbusBatteryActionType.SET_POWER:
                _prependQueueIfNotExist(actionType, fn);
                break;
            case BydModbusBatteryActionType.WORK_STATE:
            case BydModbusBatteryActionType.STATE_CONTROL:
            case BydModbusBatteryActionType.SOC:
            default: _appendQueueIfNotExist(actionType, fn);
        }
    }

    function diagnosticRegisters() {
          return [
              /*
            // Name                                |  Address | Quantity
            ["RTUAddressTable1",                    1280,       3],
            ["RTUAddressTable2",                   14496,      10],
            ["PCAddressTable",                      4097,      84],
            ["BECUAddress",                        24577,      66],
            ["ADASAddressTable",                   13328,      67]
            */
        ];
        return 0;
    }
}



enum NecAerosReadAddress {
    GROUP_MODE            = 1,
    CHARGE_CAPACITY       = 2,
    DISCHARGE_CAPACITY    = 3,
    REAL_POWER_DEMAND     = 4,
    KVAR_POWER_DEMAND     = 5,
    REAL_POWER_OUTPUT     = 6,
    REACITVE_POWER_OUTPUT = 7,
    AVERAGE_SOC           = 10,
    REAL_POWER_CAPACITY   = 11,
    AVAILABLE_ENERGY      = 12,
    SYSTEM_ESTOP          = 14010,
    ENABLED_DISPATCH_MODE = 14030
    //REAL_POWER_COMMAND   = 14031  // TBD
}

enum NecAerosWriteAddress {
    REAL_POWER_SETPOINT            = 14007,
    REACTIVE_POWER_SETPOINT        = 14009,
    SOCREF                         = 14011,
    MODE_CONTROL                   = 14012,
    POWER_BLOCK_ENABLE_CONTROL_12  = 14013,   // Register is 32bits
    POWER_BLOCK_ENABLE_CONTROL_34  = 14015,
    HEARTBEAT_FROM_PGM             = 14017,
    DISPATCH_MODE                  = 14020,
    POWER_SLEW_RATE                = 14028,
    REACTIVE_POWER_SLEW_RATE       = 14029,
    ACK_ALARMS                     = 14050,
    MAX_POWER_DISCHARGE            = 14060,
    MIN_POWER_DISCHARGE            = 14061
}

enum NecAerosDefaultValue {
    REAL_POWER_SETPOINT           =  0,
    REACTIVE_POWER_SETPOINT       =  0,
    SOCREF                        =  50,
    POWER_BLOCK_ENABLE_CONTROL    =  3,
    ACK_ALARMS                    =  1,
    DISPATCH_MODE_DISPATCH        =  1,
    MANUAL_MODE                   =  4,
    PSLEW_RECOMMENDED_INIT_VALUE  =  32767,
    QSLEW_RECOMMENDED_INIT_VALUE  =  600,
}

class NecModbusTcpClient extends ModbusTcpClient {
    _commsStatus     = null;
    _initialiseState = 0;

    _soc             = 0.0;
    _power           = 0;
    _toggleHeartbeat = 0;
    _heartbeatTimer  = null;

    constructor(commConfig, wiz) {
        _commsStatus = SingleStatus.New(function() {
            return { connectivity = connectivityStatus()};
        }.bindenv(this)).initialising();

        base.constructor(commConfig, wiz);
    }

    function status() {
        return _commsStatus;
    }

    function stop() {
        _initialiseState = 0;
        _commsStatus.tearingDown();
        _stopHeartbeat();
        base.deinitialise();
    }

    function _queueDeInit() {}

    function onConnected() {
        _initialiseState = 0;
        _writeInitCommand();
    }

    function onConnectionError(error) {
        logger.log("NecModbusTcpClient::onConnectionError "+ error + " "+ this);
        _commsStatus.broken("Connection error: "+ error);
    }

    function onDisconnected() {
        _commsStatus.tornDown();
    }

    function onReconnected() {
        _commsStatus.running();
        // todo check whether to restart heartbeat after regaining connection as it was stopped onReconnectionError
    }

    function onReconnectionError(error) {
        logger.log("NecModbusTcpClient::onReconnectionError " + error + " "+ this);
        _commsStatus.broken("Reconnection error: "+ error);
        _stopHeartbeat();
    }

    function getStateOfCharge() {
        return _soc;
    }

    function realPowerSetPoint(power) {
        _power = round(power, 0).tointeger();
        _queueAction(ModbusActionType.SET_POWER, function() {
            _modbus.readWriteMultipleRegisters(NecAerosReadAddress.AVERAGE_SOC, 1,
                NecAerosWriteAddress.REAL_POWER_SETPOINT, 1, _nextTargetPowerValue(),
                _onReceive(_readSocCallback).bindenv(this));
        }.bindenv(this));
    }

    // -------------------- PRIVATE METHODS -------------------- //
    function _stopHeartbeat() {
        _heartbeatTimer = cancelTimer(_heartbeatTimer);
    }

    function _writeInitCommandCb(error, result) {
        if (error) {
            local errorString = ModbusExceptions.decode(error);
            logger.error("NecModbusTcpClient::_writeInitCommandCb: error " + errorString);
            _commsStatus.broken("initialisation error: "+ errorString);
        } else {
            imp.wakeup(0.01, this._writeInitCommand.bindenv(this));
        }
    }

    function _writeInitCommand() {
        local address;
        local value;
        local quantity = 1;

        switch (_initialiseState) {
            case 0:
                address = NecAerosWriteAddress.ACK_ALARMS;
                value   = NecAerosDefaultValue.ACK_ALARMS;
                break;
            case 1:
                value = blob();
                value.writen(NecAerosDefaultValue.POWER_BLOCK_ENABLE_CONTROL, 'w');
                value.writen(0, 'w');
                value.swap2();
                address = NecAerosWriteAddress.POWER_BLOCK_ENABLE_CONTROL_12;
                quantity = 2;
                break;
            case 2:
                address = NecAerosWriteAddress.DISPATCH_MODE;
                value   = NecAerosDefaultValue.DISPATCH_MODE_DISPATCH;
                break;
            case 3:
                address = NecAerosWriteAddress.SOCREF;
                value   = NecAerosDefaultValue.SOCREF;
                break;
            case 4:
                address = NecAerosWriteAddress.REAL_POWER_SETPOINT;
                value   = NecAerosDefaultValue.REAL_POWER_SETPOINT;
                break;
            case 5:
                address = NecAerosWriteAddress.REACTIVE_POWER_SETPOINT;
                value   = NecAerosDefaultValue.REACTIVE_POWER_SETPOINT;
                break;
            case 6:
                address = NecAerosWriteAddress.MODE_CONTROL;
                value   = NecAerosDefaultValue.MANUAL_MODE;
                break;
            case 7:
                address = NecAerosWriteAddress.POWER_SLEW_RATE;
                value   = NecAerosDefaultValue.PSLEW_RECOMMENDED_INIT_VALUE;
                break;
            case 8:
                address = NecAerosWriteAddress.REACTIVE_POWER_SLEW_RATE;
                value   = NecAerosDefaultValue.QSLEW_RECOMMENDED_INIT_VALUE;
                break;
            case 9:
                imp.wakeup(0, _heartbeat.bindenv(this));
                _commsStatus.running();
                return;
            default:
                logger.log("NecModbusTcpClient::_writeInitCommand(): Unknown initialiseState "+ _initialiseState);
                return;
        }
         _initialiseState++;
         _modbus.write(MODBUSRTU_TARGET_TYPE.HOLDING_REGISTER, address, quantity, value, _writeInitCommandCb.bindenv(this));
    }

    function _nextHeartbeatValue() {
        _toggleHeartbeat = (_toggleHeartbeat == 1) ? 0 : 1;
        return _toggleHeartbeat;
    }

    function _heartbeat() {
        _heartbeatTimer = imp.wakeup(1, _heartbeat.bindenv(this));
        _queueAction(ModbusActionType.HEARTBEAT, function() {
            _modbus.readWriteMultipleRegisters(NecAerosReadAddress.AVERAGE_SOC, 1,
                NecAerosWriteAddress.HEARTBEAT_FROM_PGM, 1, _nextHeartbeatValue(),
                _onReceive(_readSocCallback).bindenv(this));
        }.bindenv(this));
    }

    function _nextTargetPowerValue() {
        return _power;
    }

    function _readSocCallback(error, result) {
        if (error) {
            _commsStatus.broken("SOC read error: " + error);
        } else {
            _soc = (result[0])/10.0;
            _commsStatus.running();
        }
    }

    /*
        0..1 realPowerSetPoint always jump ahead of queue
        0..1 heartbeat append the queue
     */
    function _queueAction(actionType, fn) {
        switch (actionType) {
            case ModbusActionType.SET_POWER:
                _prependQueueIfNotExist(actionType, fn);
                break;
            case ModbusActionType.HEARTBEAT:
                _appendQueueIfNotExist(actionType, fn);
                break;
            default:
        }
    }

    function diagnosticRegisters() {
        return [
            // Name                       |  Address                                          | Quantity
            ["systemestop",               NecAerosReadAddress.SYSTEM_ESTOP,                   1],
            ["groupmode" ,                NecAerosReadAddress.GROUP_MODE,                     1],
            ["averagesoc",                NecAerosReadAddress.SYSTEM_ESTOP,                   1],
            ["realpowercapacity" ,        NecAerosReadAddress.REAL_POWER_CAPACITY,            1],
            ["chargecapacity",            NecAerosReadAddress.CHARGE_CAPACITY,                1],
            ["dischargecapacity",         NecAerosReadAddress.DISCHARGE_CAPACITY,             1],
            ["realpowerdemand",           NecAerosReadAddress.REAL_POWER_DEMAND,              1],
            ["kvarpowerdemand",           NecAerosReadAddress.KVAR_POWER_DEMAND,              1],
            ["realpoweroutput",           NecAerosReadAddress.REAL_POWER_OUTPUT,              1],
            ["reactivepoweroutput",       NecAerosReadAddress.REACITVE_POWER_OUTPUT,          1],
            ["averagesoc",                NecAerosReadAddress.AVERAGE_SOC,                    1],
            ["availableenergy",           NecAerosReadAddress.AVAILABLE_ENERGY,               1],
            ["enabled",                   NecAerosReadAddress.ENABLED_DISPATCH_MODE,          1],
            ["modecontrol",               NecAerosWriteAddress.MODE_CONTROL,                  1],
            ["powerblockenablecontrol12", NecAerosWriteAddress.POWER_BLOCK_ENABLE_CONTROL_12, 2],
            ["powerblockenablecontrol34", NecAerosWriteAddress.POWER_BLOCK_ENABLE_CONTROL_34, 2],
            ["ackalarms",                 NecAerosWriteAddress.ACK_ALARMS,                    1],
            ["heartbeat",                 NecAerosWriteAddress.HEARTBEAT_FROM_PGM,            1],
            ["dispatchmode",              NecAerosWriteAddress.DISPATCH_MODE,                 1],
            ["realpowersetpoint",         NecAerosWriteAddress.REAL_POWER_SETPOINT,           1],
            ["reactivepowersetpoint",     NecAerosWriteAddress.REACTIVE_POWER_SETPOINT,       1],
            ["socref",                    NecAerosWriteAddress.SOCREF,                        1],
            ["psocmax",                   NecAerosWriteAddress.MAX_POWER_DISCHARGE,           1],
            ["psocmin",                   NecAerosWriteAddress.MIN_POWER_DISCHARGE,           1],
            ["pslew",                     NecAerosWriteAddress.POWER_SLEW_RATE,               1],
            ["qslew",                     NecAerosWriteAddress.REACTIVE_POWER_SLEW_RATE,      1]
        ];
    }
}

enum BatteryModbusBatteryActionType {
    SET_POWER  = "SP",
    READ_SOC   = "RS",
    READ_POWER = "RP",
}

class BatteryMappedModbusTcpClient extends MappedModbusTcpClient {
    _readTimer = null;

    // todo remove once soc becomes an input source
    _soc = null;

    constructor(commConfig, wiz, modbusMap) {
        base.constructor(commConfig, wiz, modbusMap);
    }

    // todo change to fetch-callback mechanism once SoC becomes an input source, see readActualPower(cb)
    function getStateOfCharge() {
        return _soc;
    }

    function readActualPower(valueCb) {
        _fetchReading(BatteryModbusBatteryActionType.READ_POWER, valueCb);
    }

    /*
       TIP: target power is calculated from TO THE GRID point-of-view. However,
            Jules views power from TO THE BATTERY point.
    */
    function realPowerSetPoint(kW) { _write(BatteryModbusBatteryActionType.SET_POWER, kW); }

    function configKeyToModbusAction(key) {
        switch(key) {
            case "writeRealPowerSetpoint": return BatteryModbusBatteryActionType.SET_POWER;
            case "readStateOfCharge": return BatteryModbusBatteryActionType.READ_SOC;
            case "readActualPower": return BatteryModbusBatteryActionType.READ_POWER;
            default: return null;
        }
    }

    function setPower() {
        _queueAction(BatteryModbusBatteryActionType.SET_POWER, function() {
            _modbus.write(MODBUSRTU_TARGET_TYPE.HOLDING_REGISTER, TeslaPowerpackRealAddress.MODE, 1,
                value, _onReceive(_logWriteError("MODE")).bindenv(this));
        }.bindenv(this));
    }

    function onConnected() {
        base.onConnected();
        _pollSoC();
    }

    function stop() {
        _readTimer = cancelTimer(_readTimer);
        base.stop();
    }

    // todo remove once SoC becomes an input source
    function _pollSoC() {
        _readTimer = imp.wakeup(1, _pollSoC.bindenv(this));
        _fetchReading(BatteryModbusBatteryActionType.READ_SOC, function(value) {_soc = value;}.bindenv(this));
    }

    function _queueDeInit() {
        realPowerSetPoint(0);
    }

    function _queueAction(actionType, fn) {
        switch (actionType) {
            case BatteryModbusBatteryActionType.SET_POWER:
                _prependQueueIfNotExist(actionType, fn);
                break;
            default: _appendQueueIfNotExist(actionType, fn);
        }
    }
};


// Inputs
class FrequencyProfile {
    _profile = null
    _startMillis = null
    _endMillis = null
    _completeCb = null

    constructor(profile) {
        _profile = []
        _startMillis = hardware.millis()

        foreach (point in profile) {
            local time = (point.time * 1000).tointeger()
            local frequency = (point.freq * 1000).tointeger()
            _profile.append({"time": time, "frequency": frequency})
        }

        _endMillis = _profile[_profile.len() - 1].time
    }

    function getFreq() {
        local millis = millisElapsed(hardware.millis(), _startMillis)
        if (millis > _endMillis && _completeCb) { _completeCb() }
        return frequency(millis)
    }

    function onCompleted(callback) { _completeCb = callback }

    function frequency(timeInMillisSinceProfileStarted) {
        local frequency = 50000
        foreach (i, point in _profile) {
            if (point.time == timeInMillisSinceProfileStarted) {
                frequency = point.frequency
                break
            } else if (point.time > timeInMillisSinceProfileStarted) {
                frequency = calculate(i, timeInMillisSinceProfileStarted)
                break
            }
        }
        return frequency
    }

    function calculate(index, elapsed) {
        local prev = _profile[index - 1]
        local next = _profile[index]

        local timeDelta = (1.0*elapsed - 1.0*prev.time) / (1.0*next.time - 1.0*prev.time)
        local verticalChange = 1.0*next.frequency - 1.0*prev.frequency

        return round((timeDelta * verticalChange) + prev.frequency,0).tointeger()
    }
}

class FrequencySource extends Input {
    _frequency = null;
    _injected  = null;
    _recordOn = null;

    constructor(id, name, recorder, recordOn) {
        base.constructor(id, name, recorder);
        _recordOn = recordOn;
    }

    function getFrequency() { return _injected ? _injected.getFreq() : _frequency; }

    function runWithProfile(frequencyProfile) {
        frequencyProfile.onCompleted(endProfile.bindenv(this))
        _injected = frequencyProfile;
    }

    function endProfile() {
        if(_injected) {
            logger.log("---- Stop frequency profile: "+ printTimestamp(date()));
            _injected = null;
        }
    }

    function isRunningProfile() {
        return _injected != null;
    }

    function _recordReading(mHz) {
        if(mHz == null) {
            _frequency = null;
        } else {
            _frequency = round(mHz, 0).tointeger();
        }

        if(_recordOn) {
            if (_injected) {
                _record.values(id(), { frequency = _injected.getFreq() });
            } else {
                _record.values(id(), { frequency = _frequency });
            }
        }
    }
}

class PowerSource extends Input {
    _power = null;
    _recordOn = null;

    constructor(id, name, recorder, recordOn) {
        base.constructor(id, name, recorder);
        _recordOn = recordOn;
    }

    function getPower() { return _power; }

    function _recordReading(kW=null, pulse=null, VA=null) {
        _power = kW;
        if(_recordOn) {
            local values = {};
            if(kW != null) {
                if ( pulse != null )
                    values[pulse] <- round(kW, 3);
                else
                    values.power <- round(kW, 3);
            }
            if(VA != null) {
                values.reactivePower <- round(VA, 3);
            }
            if(values.len() > 0) {_record.values(id(), values) };
        }
    }
}

/*
GNSS ... Time Provider
should it be an input???
feel like it should be something that is automatically detected and provided as utility
*/
const BUFFER_MAX = 512;

enum gnssState {
    SYNC_RECV =  0,
    SYNC_G    =  1,
    SYNC_N    =  2,
    SYNC_R    =  3,
    SYNC_M    =  4,
    SYNC_C    =  5,
    END_RECV  =  6,
    DATA_RECV = -1,
}

class GnssTime extends Input {
    _gnssInput      = null;
    _status         = null;
    _bufferSentence = null;
    _bufferPosition = 0;
    _state          = gnssState.SYNC_RECV;
    _driver         = null;
    _time           = null;
    _timeBuffer     = null;
    _dateBuffer     = null;

    constructor(id, name, gnssInput) {
        base.constructor(id, name, null);
        _gnssInput = gnssInput;
        _gnssInput.init();
        _gnssInput.configureCallback(decode.bindenv(this));
        _bufferSentence = array(BUFFER_MAX);

        _status = SingleStatus.New().running();
    }

    function time() {
        // time
        local hours   = _timeBuffer.slice(0,2).tointeger();
        local minutes = _timeBuffer.slice(2,4).tointeger();
        local seconds = _timeBuffer.slice(4).tointeger();
        // date
        local day   = _dateBuffer.slice(0,2).tointeger();
        local month = _dateBuffer.slice(2,4).tointeger();
        local year  = ("20" + _dateBuffer.slice(4)).tointeger();
        // convert now
        _time = convertTimeDateToUnixTime(seconds,minutes,hours,day,month,year);
        return _time;
    }

    function timeAsString() {
        return _timeBuffer;
    }

    function dateAsString() {
        return _dateBuffer;
    }

    function status() { return _status; }

    function stop() {
        _gnssInput.resetUart();
        _gnssInput = null;

        _status.tornDown();
    }

    function decode (input) {
        switch (_state) {
            case gnssState.SYNC_RECV:
                if ( input == '$') { _state = gnssState.SYNC_G; }
                break;

            case gnssState.SYNC_G:
                if ( input == 'G') { _state = gnssState.SYNC_N;  } else { _state = gnssState.SYNC_RECV; }
                break;

            case gnssState.SYNC_N:
                if ( input == 'N') { _state = gnssState.SYNC_R; } else { _state = gnssState.SYNC_RECV; }
                break;

            case gnssState.SYNC_R:
                if ( input == 'R') { _state = gnssState.SYNC_M; } else { _state = gnssState.SYNC_RECV; }
                break;

            case gnssState.SYNC_M:
                if ( input == 'M') { _state = gnssState.SYNC_C; } else { _state = gnssState.SYNC_RECV; }
                break;

            case gnssState.SYNC_C:
                if ( input == 'C') { _state = gnssState.DATA_RECV; } else { _state = gnssState.SYNC_RECV; }
                break;

            case gnssState.END_RECV:
                if ( input == '\n') {
                    _bufferSentence[ _bufferPosition ] = 0;
                    _parseGnrmcSentence(_bufferSentence);
                    _bufferPosition = 0;
                } else { _state = gnssState.SYNC_RECV; }
                break;

            default://  gnssState.DATA_RECV
                switch (input) {
                    case '\r': _state = gnssState.END_RECV;
                    break;

                    case '\n':
                        _bufferPosition = 0;
                        _state = gnssState.SYNC_RECV;
                    break;

                    case '$':
                        _bufferPosition = 0;
                        _state = gnssState.SYNC_G;
                    break;

                    default: _bufferSentence[_bufferPosition++] = input;
                    break;
                }
            break;
        }
    }

    function _writeByte(dataByte) {
        _port.write(dataByte);
    }

    function _parseDelimiterItem(delimiter, startIndex, buf) {
        // GNRMC sentence: ,145424.00,V,,,,,,,080318,,,N*63
        local item = "";
        local i = 0;

        for ( local j = 0; j < startIndex; j++ ) {
            while (buf[i++] != delimiter);
        }

        while (buf[i] != delimiter) {
            item = item + buf[i].tochar();
            i++;
        }
        return item;
    }

    function _parseGnrmcSentence(buf) {
        _timeBuffer = _parseDelimiterItem(',', 1, buf);
        _timeBuffer = _parseDelimiterItem('.', 0, _timeBuffer); // removes the .00 at end of time
        _dateBuffer = _parseDelimiterItem(',', 9, buf);
     }
}
/* Input: Onboard */
class PulseMetering extends Input {
    _pulseInput      = null;
    _readIntervalSec = null;
    _timer           = null;
    _status          = null;
    _recordOn        = null;

    constructor(id, name, pulseInput, measurementPeriodSec, recorder, recordOn) {
        base.constructor(id, name, recorder);
        _pulseInput = pulseInput;
        _recordOn = recordOn;

        if (measurementPeriodSec) {
            _readIntervalSec = measurementPeriodSec;
            imp.wakeup(0, recordPulseFrequency.bindenv(this))
        } else {
            pulseInput.setPulseIntervalHandler(onPulseInterval.bindenv(this));
        }

        _status = SingleStatus.New().running();
    }

    function recordPulseFrequency() {
        local pulseFrequency = _pulseInput.pulseFrequency();
        if(_record) {
            _record.pulseCounting(id(), pulseFrequency);
        }
        _timer = imp.wakeup(_readIntervalSec, recordPulseFrequency.bindenv(this));
    }

    function onPulseInterval(pulseInterval) {
        if(_recordOn) _record.pulseTiming(id(), pulseInterval);
    }

    function status() { return _status; }

    function stop() {
        _timer = cancelTimer(_timer);
        _pulseInput.resetPinConfiguration();
        _pulseInput = null;

        _status.tornDown();
    }
}
class PowerSource.PulseCounting extends PowerSource {
    static SECS_IN_1HR = 3600;

    _driver = null;
    _status = null;
    _timer  = null;
    _interval = null;
    _kWhPerPulse = null;

    /*
        readInterval: how often to read pulse frequency from the driver in sec
        kWhPerPulse: multiplier in kWh per pulse
     */
    constructor(id, name, driver, readInterval, kWhPerPulse, recorder, recordOn) {
        base.constructor(id, name, recorder, recordOn);

        _driver = driver;
        _kWhPerPulse = kWhPerPulse;
        _interval = readInterval;
        _status = SingleStatus.New(function() {
            return { kW = _power};
        }.bindenv(this)).running();
        imp.wakeup(0, _read.bindenv(this));
    }

    function _read() {
        local f = _driver.pulseFrequency();
        local kW = f * SECS_IN_1HR * _kWhPerPulse;
        _recordReading(kW, _driver.pulseName());
        _timer = imp.wakeup(_interval, _read.bindenv(this));
    }

    function status() { return _status; }

    function stop() {
        _timer = cancelTimer(_timer);
        _driver.resetPinConfiguration();
        _status.tornDown();
    }
}
class DigitalInputMonitoring extends Input {
    _input           = null;
    _readIntervalSec = null;
    _timer           = null;
    _status          = null;
    _recordOn        = null;
    _connector       = null;

    constructor(id, connector, name, input, measurementPeriodSec, recorder, recordOn) {
        base.constructor(id, name, recorder);
        _readIntervalSec = measurementPeriodSec;
        _input = input;
        _connector = connector;
        _recordOn = recordOn;
        _status = SingleStatus.New().running();
        imp.wakeup(_readIntervalSec, recordDigitalInput.bindenv(this));
    }

    function recordDigitalInput() {
        if(_record) {
            _record.digitalInput(_connector, _input.getInput());
        }
        _timer = imp.wakeup(_readIntervalSec, recordDigitalInput.bindenv(this));
    }

    function status() { return _status; }

    function stop() {
        _timer = cancelTimer(_timer);
        _input = null;

        _status.tornDown();
    }
}

/* Input: Metering Segment */
class FrequencySource.MeteringSegment extends FrequencySource {
    connectionToPhase = {
        P1 = MModeRegisterSettings.PhaseA_Period,
        P2 = MModeRegisterSettings.PhaseB_Period,
        P3 = MModeRegisterSettings.PhaseC_Period
    }

    _driver = null;
    _phase  = null;
    _status = null;
    _timer  = null;

    _interval_us = 0;
    _sampleInterval_us = 0;
    _numberOfSamples = 0;
    _currentSample = 0;
    _sampleAccumulator = 0;
    _lastSampleTime_us = 0;
    _lastIntervalTime_us = 0;
    _firstRun = false;

    constructor(id, name, connection, driver, readInterval_s, samplesOverInterval, recorder, recordOn) {
        if(!(connection in connectionToPhase)) { throw "Unknown metering segment connection"; }
        base.constructor(id, name, recorder, recordOn);
        _driver = driver;
        _phase  = connectionToPhase[connection];
        _interval_us = (readInterval_s * 1000000).tointeger();
        _sampleInterval_us = (_interval_us / samplesOverInterval).tointeger();
        _numberOfSamples = samplesOverInterval;

        _status = SingleStatus.New(function() {
            return {
                mHz = getFrequency(),
                runningProfile = isRunningProfile()
            };
        }.bindenv(this)).initialising();

        _alignClocksToSecondBoundary();
        imp.wakeup(0, _fetch.bindenv(this));
    }

    function _fetch() {
        // Increment and wrap the sample count
        _currentSample = (_currentSample + 1) % _numberOfSamples;

        // Sample the phase
        _sampleAccumulator += _driver.readFrequency(_phase);

        // If this was the last sample in the sequence, record the average and reset
        if(_currentSample == 0) {
            _recordReading(_sampleAccumulator/_numberOfSamples);
            _sampleAccumulator = 0;
            if( _status.isRunning() == false ) { _status.running(); }
        }

        // Sleep until the next sample, correcting for drift, if this is the first sample, correct for
        // rounding errors as well using the interval time instead of the sample time
        if(_firstRun == false) {
            // This is our first sample, so set the initial last times and perform no correction
            _firstRun = true;
            _lastIntervalTime_us = _lastSampleTime_us = hardware.micros() & 0x7FFFFFFF;
            _timer = imp.wakeup( 1.0 * _sampleInterval_us / 1000000, _fetch.bindenv(this));
        } else {
            local currentTime_us = hardware.micros() & 0x7FFFFFFF;
            local difference_us = ((currentTime_us - _lastSampleTime_us) & 0x7FFFFFFF) - _sampleInterval_us;

            if( difference_us < _sampleInterval_us ) {
                _timer = imp.wakeup(1.0 * (_sampleInterval_us - difference_us) / 1000000, _fetch.bindenv(this));
            } else {
                _timer = imp.wakeup(0, _fetch.bindenv(this));
            }

            if(_currentSample == 1) {
                _lastIntervalTime_us = (_lastIntervalTime_us + _interval_us) & 0x7FFFFFFF;
                _lastSampleTime_us = _lastIntervalTime_us;
            } else {
                _lastSampleTime_us = (_lastSampleTime_us + _sampleInterval_us) & 0x7FFFFFFF;
            }
        }
    }

    function stop() {
        endProfile();
        _timer = cancelTimer(_timer);
        _status.tornDown();
        _frequency = null;
    }

    function status() { return _status; }
}

local StaticFrequencyDriver = {
    function readFrequency(phase) {
        return 50000;
    }
};

/// \note Could be ineffective if booting with no valid clock.
function _alignClocksToSecondBoundary() {

    if( "nutkin" in getroottable() ) {
        return;
    }

    local x = time();
    while (time() == x) {};
}
const METERING_SEGMENT_DEFAULT_CT_RATING_IN_AMPS = 5.0;

class MeteringSegmentPowerReader {
    _driver = null;
    _pqOn = null;

    _regWattHr = null;
    _regWatt = null;
    _regVARHr = null;
    _regVrms = null;
    _regIrms = null;
    _regAngle = null;

    _ctRatingScalingFactor = 1.0;
    _voltageTransformerRatio = 1.0;

    constructor(driver, connection, prequalification, ctRatingInAmps, calibration, voltageTransformerRatio) {
        _driver = driver;
        _pqOn = prequalification;

        if (connection == "CT1") {
            _regWattHr = REG_AWATTHR;
            _regWatt   = REG_AWATT;
            _regVARHr  = REG_AVARHR;
            _regVrms   = REG_AVRMS;
            _regIrms   = REG_AIRMS;
            _regAngle  = REG_ANGLE0;
        } else if (connection == "CT2") {
            _regWattHr = REG_BWATTHR;
            _regWatt   = REG_BWATT;
            _regVARHr  = REG_BVARHR;
            _regVrms   = REG_BVRMS;
            _regIrms   = REG_BIRMS;
            _regAngle  = REG_ANGLE1;
        } else if (connection == "CT3") {
            _regWattHr = REG_CWATTHR;
            _regWatt   = REG_CWATT;
            _regVARHr  = REG_CVARHR;
            _regVrms   = REG_CVRMS;
            _regIrms   = REG_CIRMS;
            _regAngle  = REG_ANGLE2;
        } else {
            throw ("Input connection unknown");
        }

        _ctRatingScalingFactor = ctRatingInAmps / METERING_SEGMENT_DEFAULT_CT_RATING_IN_AMPS;

        if (calibration && (_driver instanceof ADE7858)) {
            _driver.calibrate(connection, calibration);
        }

        _voltageTransformerRatio = voltageTransformerRatio;
    }

    function read() {
        if (_pqOn) {
            return { averagePowerInWatts = _driver.averagePower(_regWattHr) * _voltageTransformerRatio * _ctRatingScalingFactor }
        } else {
            return {
                averagePowerInWatts = _driver.averagePower(_regWattHr) * _voltageTransformerRatio * _ctRatingScalingFactor,
                averageReactivePowerInVoltAmpsR = _driver.averageReactivePower(_regVARHr) * _voltageTransformerRatio * _ctRatingScalingFactor,
                activePowerInWatts = _driver.readActivePower(_regWatt) * _voltageTransformerRatio * _ctRatingScalingFactor,                
                vrms = _driver.readAvgRms(_regVrms) * _voltageTransformerRatio,
                irms = _driver.readAvgRms(_regIrms) * _ctRatingScalingFactor,
                pf = _driver.readPF(_regAngle)
            }
        }
    }
}

local StaticPowerDriver = {
    function readActivePower(x) {
        return 2500;
    },
    function readReactivePower(x) {
        return 2500;
    },
    function readAvgRms(reg) {
        local rms = ((reg == REG_AVRMS) || (reg == REG_BVRMS) || (reg == REG_CVRMS)) ? 250 : 10;
        return rms;
    },
    function readPF(x) {
        return 0.5;
    },
    function averagePower(x) {
        return 2500;
    }
    function averageReactivePower(x) {
        return 2500;
    }
};

class PowerSource.SinglePhaseMeteringSegment extends PowerSource {
    _status = null;
    _connection = null;
    _reader = null;
    _readings = null;
    _timer  = null;
    _interval = null;
    _measureReactivePower = false;

    constructor(id, name, driver, connection, ctRatingInAmps, calibration, voltageTransformerRatio, readIntervalSec, measureReactivePower, recorder, recordOn) {
        base.constructor(id, name, recorder, recordOn);
        local prequalification = recorder.inPrequalification();

        _interval = readIntervalSec;
        _measureReactivePower = measureReactivePower;
        _connection = connection;

        _reader = MeteringSegmentPowerReader(driver, connection, prequalification, ctRatingInAmps, calibration, voltageTransformerRatio);
        _readings = {};

        _status = SingleStatus.New(function() {
            // todo relook at this
            local info = {};
            info.kW <- _power;
            info[_connection] <- clone _readings;
            return info;
        }.bindenv(this)).initialising();

        imp.wakeup(1, _delayReading.bindenv(this));
    }

    function _delayReading() {
        _readings = _reader.read();
        imp.wakeup(0, _read.bindenv(this));
    }

// todo if multiple single-phase, how can we differentiate values sent to readings service?
    // this, again, back to the question of how we want fruit/asset naming convention to be as currently it's limited to station id
    function _read() {
        try {
            _readings = _reader.read();
            _recordReading(_readings.averagePowerInWatts / 1000, null, _measureReactivePower && !_reader._pqOn ? _readings.averageReactivePowerInVoltAmpsR / 1000 : null );
            _status.running();
        } catch (ex) {
            _status.broken(ex);
            logger.log("WARN: "+ ex);
        }
        _timer = imp.wakeup(_interval, _read.bindenv(this));
    }

    function stop() {
        _timer = cancelTimer(_timer);
        _status.tornDown();
    }

    function status() { return _status; }
}

class PowerSource.ThreePhaseMeteringSegment extends PowerSource {
    _status = null;
    _readers = null;
    _readings = null;
    _timer  = null;
    _interval = null;
    _measureReactivePower = false;

    constructor(id, name, driver, ctRatingInAmps, calibration, voltageTransformerRatio, readIntervalSec, measureReactivePower, recorder, recordOn) {
        base.constructor(id, name, recorder, recordOn);
        local pqOn = recorder.inPrequalification();
        _interval = readIntervalSec;
        _measureReactivePower = measureReactivePower;

        // todo CT1|CT2|CT3 strings should probably be grouped into an enum somewhere ... constants.device.nut?
        local function calibrationFor(connection) {
            return (calibration && connection in calibration) ? calibration[connection] : null;
        }

        _readers = {};
        _readers["CT1"] <- MeteringSegmentPowerReader(driver, "CT1", pqOn, ctRatingInAmps, calibrationFor("CT1"), voltageTransformerRatio);
        _readers["CT2"] <- MeteringSegmentPowerReader(driver, "CT2", pqOn, ctRatingInAmps, calibrationFor("CT2"), voltageTransformerRatio);
        _readers["CT3"] <- MeteringSegmentPowerReader(driver, "CT3", pqOn, ctRatingInAmps, calibrationFor("CT3"), voltageTransformerRatio);

        _readings = {};

        _status = SingleStatus.New(function() {
            local info = clone _readings;
            info.kW <- _power;
            return info;
        }.bindenv(this)).initialising();

        imp.wakeup(1, _delayReading.bindenv(this));
    }

    function _delayReading() {
        foreach (ct, r in _readers) {
            _readings[ct] <- r.read();
        }
        imp.wakeup(0, _read.bindenv(this));
    }

    function _read() {
        try {
            local _totalPower = 0.0;
            local _totalReactivePower = 0.0;
            foreach (ct, r in _readers) {
                _readings[ct] <- r.read();
                _totalPower += _readings[ct].averagePowerInWatts;
                if( !r._pqOn ) { _totalReactivePower += _readings[ct].averageReactivePowerInVoltAmpsR; }
            }
            _recordReading(_totalPower / 1000, null, _measureReactivePower && !_reader._pqOn ? _totalReactivePower / 1000 : null );
            
            _status.running();
        } catch (ex) {
            _status.broken(ex);
            logger.log("WARN: "+ ex);
        }
        _timer = imp.wakeup(_interval, _read.bindenv(this));
    }

    function stop() {
        _timer = cancelTimer(_timer);
        _status.tornDown();
    }

    function status() { return _status; }
}


class FrequencySource.Modbus extends FrequencySource {
    _modbus = null;
    _status = null;
    _timer  = null;
    _interval = null;

    constructor(id, name, modbus, readIntervalSec, recorder, recordOn) {
        base.constructor(id, name, recorder, recordOn);
        _modbus = modbus;
        _interval = readIntervalSec;

        _status = AggregatedStatus(
            { comm = _modbus.status.bindenv(_modbus) },
            function() {
                return {
                    mHz = getFrequency(),
                    runningProfile = isRunningProfile()
            };
        }.bindenv(this));

        imp.wakeup(0, _fetch.bindenv(this));
    }

    function _fetch() {
        _modbus.readFrequency(_retrieve.bindenv(this));
        _timer = imp.wakeup(_interval, _fetch.bindenv(this));
    }

    function _retrieve(mHz) { _recordReading(mHz); }

    function stop() {
        _timer = cancelTimer(_timer);
        _frequency = null;
    }

    function status() { return _status; }
}
class PowerSource.Modbus extends PowerSource {
    _status = null;
    _modbus = null;
    _timer  = null;
    _interval = null;
    _initTimer = null;

    constructor(id, name, modbus, readIntervalSec, recorder, recordOn) {
        base.constructor(id, name, recorder, recordOn);
        _modbus = modbus;
        _interval = readIntervalSec;

        _status = SingleStatus.New(function() {
            return { kW = getPower() };
        }.bindenv(this)).initialising();

        imp.wakeup(0, _checkReady.bindenv(this));
    }

    function _checkReady() {
        if(_modbus.status().isRunning()) {
            _status.running();
            imp.wakeup(0, _fetch.bindenv(this));
        } else { _initTimer = imp.wakeup(0.5, _checkReady.bindenv(this)); }
    }

    function _fetch() {
        _modbus.readActualPower(_retrieve.bindenv(this));
        _timer = imp.wakeup(_interval, _fetch.bindenv(this));
    }

    function _retrieve(kW) { _recordReading(kW); }

    function stop() {
        _timer = cancelTimer(_timer);
        _initTimer = cancelTimer(_initTimer);
        _status.tornDown();
    }

    function status() { return _status; }
}

// Assets

class BinaryAsset extends Asset {
    _comm = null;
    _dispatchTimer = null;

    constructor(id, name, binaryComm) {
        _comm = binaryComm;
        _comm.endDispatch();
        base.constructor(id, name);
    }

    function isDispatching() {
        return _comm.isDispatching();
    }

    function beginDispatch(duration) {
        if(duration == 0) {
            endDispatch();
        } else {
            _comm.dispatch();
            _dispatchTimer = cancelTimer(_dispatchTimer);
            _dispatchTimer = imp.wakeup(duration, endDispatch.bindenv(this));
            logger.log("--- "+ name() +": begin dispatch for "+ duration +"s");
            _status.dispatching();
        }
    }

    function endDispatch() {
        _comm.endDispatch();
        _dispatchTimer = cancelTimer(_dispatchTimer);
        logger.log("--- "+ name() +": end dispatch");
        _status.running();
    }

    function action( action )
    {
        switch( action.mode )
        {
            case "targetPower":
                if( action.params.kW == 0 )
                {
                    _comm.endDispatch();
                }
                else if( action.params.kW > 0 )
                {
                    _comm.dispatch();
                }
                break;

            case "default":
                _comm.endDispatch();
                break;

            default: return logger.error( "Mode not supported by BinaryAsset type" );
        }

        return null;
    }
}

class RelayAsset extends BinaryAsset {
    _relayNumber = null;
    _status = null;

    constructor(conf) {
        _relayNumber = conf.relayNumber;
        base.constructor(conf.id, conf.name, OnBoardRelay(_relayNumber));

        _status = SingleStatus.New(function() {
            return {
                name = name(),
                relayNumber = _relayNumber,
                dispatching = isDispatching() }
        }.bindenv(this)).running();
    }

    function status() { return _status; }

    function stop() {
        endDispatch();
        _status.tornDown();
    }

    function cancel() { endDispatch(); }
}

// todo OtherBinaryAsset i.e. non-relay and requires comm as constructor arg

class TargetPowerControl extends Control {
    _comm = null;
    _timer = null;
    _step = null;
    _targetPower = 0;
    _baseline = 0;

    function currentTargetPower_kW() { return _targetPower }
    function currentBaseline() { return _baseline }
    function setBaseline(kW) { _baseline = kW }

    function _adjustTowards(target, current) {
        if(_step) return (target > current) ? min(target, current + _step) : max(target, current - _step);
        else return target;
    }

    function stop() {
        _timer = cancelTimer(_timer);
        _comm = null;
    }
}

// TODO review whether behaviour should be changed to read-only, i.e. not setting target power
class TargetPowerControl.Idle extends TargetPowerControl {

    constructor(powerStepPerSec) {
        _step = powerStepPerSec;
    }

    function start(comm, initialTargetPower) {
        _comm = comm;
        _targetPower = initialTargetPower;
        imp.wakeup(0, _setTargetPower.bindenv(this));
    }

    function _setTargetPower() {
        if(_comm) {
            _timer = imp.wakeup(1, _setTargetPower.bindenv(this));
            _targetPower = _adjustTowards(0, _targetPower);
            _comm.realPowerSetPoint(_targetPower);
        }
    }
}

class TargetPowerControl.ManualPower extends TargetPowerControl {
    _initial = null;
    _requested = null;
    _lastTime_ms = null;
    _elapsed_ms = null;
    _kW_ms = null;

    constructor(powerStepPerSec) {
        _step = powerStepPerSec;
    }

    function start(comm, params) {
        _comm = comm;
        _targetPower = params.initTargetPower;
        _requested = params.requestedPower;

        if( "_kW_s" in params ) {
            _kW_ms = 1.0 * params.kW_S / 1000.0;
            _initial = _requested;
            _lastTime_ms = hardware.millis();
            _elapsed_ms = (time() - params.start) * 1000;
        }

        imp.wakeup(0, _setTargetPower.bindenv(this));
    }

    function _setTargetPower() {
        if(_comm) {
            _timer = imp.wakeup(1, _setTargetPower.bindenv(this));

            if( _kW_ms ) {
                local time_ms = hardware.millis();
                _elapsed_ms += time_ms - _lastTime_ms;
                _lastTime_ms = time_ms;
                _requested = _elapsed_ms < 0 ? _initial : _initial + (_kW_ms * _elapsed_ms);
            }

            _targetPower = _calcTargetPower();
            _comm.realPowerSetPoint(_targetPower);
        }
    }

    function _calcTargetPower() { return _adjustTowards(_requested, _targetPower); }
}

/// 
class DFFRControl extends Control
{
    _assetId                = null;
    _dataRecorder           = null;
    _commModule             = null;
    _frequencySource        = null;
    _setTargetPowerTimer    = null;
    
    _gridFrequency_Hz       = null;

    _power                  = null;
    _bands                  = null;
    _soc                    = null;
    _socDeadbandManagement  = null;
    _direction              = "both";
    _offset_kW              = 0;
    _lastAppliedPower_kW    = 0;

    constructor( assetId, frequencySource, dataRecorder, powerSettings, controlSettings, socSettings = null )
    {
        _assetId            = assetId;
        _frequencySource    = frequencySource;
        _dataRecorder       = dataRecorder;

        // Configure the asset power settings/limits
        _power = { max_kW = powerSettings.maxPower_kW };
        if( "step_kW" in powerSettings ) { _power.step_kW <- powerSettings.step_kW; }

        // Configure the asset state of charge settings/limits (if available)
        _soc = socSettings;

        // Configure the initial control settings
        _gridFrequency_Hz = ("gridFrequency_Hz" in controlSettings) ? controlSettings.gridFrequency_Hz : 50.0;
        if( "socDeadbandManagement" in controlSettings ) { _socDeadbandManagement  = controlSettings.socDeadbandManagement; }

        _reconfigure( controlSettings );
    }

    // add code to show broken if no frequency source?

    function setBaseline( baseline ) {}
    function currentBaseline() { return 0; }
    function predictBaseline() { return 0; }

    function currentTargetPower_kW()
    {
        return _lastAppliedPower_kW;
    }

    function start( commModule, lastAppliedPower_kW, params = {} )
    {
        _commModule             = commModule;
        _lastAppliedPower_kW    = lastAppliedPower_kW;

        _reconfigure( params );

        if( _setTargetPowerTimer != null )
        {
            imp.cancelwakeup( _setTargetPowerTimer );
        }

        _setTargetPowerTimer = imp.wakeup( 0, _setTargetPower.bindenv(this) );
    }

    function stop()
    {
        if( _setTargetPowerTimer != null )
        {
            imp.cancelwakeup( _setTargetPowerTimer );
            _setTargetPowerTimer = null;
        }

        _commModule = null;
    }

    function _reconfigure( params )
    {
        if( "socDeadbandManagement" in params ) { _socDeadbandManagement = params.socDeadbandManagement; }
        if( "direction" in params ) { _direction = params.direction; }
        if( "offset_kW" in params ) { _offset_kW = params.offset_kW; }

        if( "bands" in params )
        {
            local bands = params.bands;

            if( "lower" in bands && "upper" in bands )
            {
                _bands =
                {
                    lower =
                    {
                        deadband_kW = bands.lower.min_kW,
                        flatband_kW = bands.lower.max_kW,
                        deadband_Hz = _gridFrequency_Hz - bands.lower.min_Hz,
                        flatband_Hz = _gridFrequency_Hz - bands.lower.max_Hz,
                    },
                    upper =
                    {
                        deadband_kW = -bands.upper.min_kW,
                        flatband_kW = -bands.upper.max_kW,
                        deadband_Hz = _gridFrequency_Hz + bands.upper.min_Hz,
                        flatband_Hz = _gridFrequency_Hz + bands.upper.max_Hz
                    }
                };
            }
            else
            {
                _bands =
                {
                    lower =
                    {
                        deadband_kW = bands.min_kW,
                        flatband_kW = bands.max_kW,
                        deadband_Hz = _gridFrequency_Hz - bands.min_Hz,
                        flatband_Hz = _gridFrequency_Hz - bands.max_Hz,
                    },
                    upper =
                    {
                        deadband_kW = -bands.min_kW,
                        flatband_kW = -bands.max_kW,
                        deadband_Hz = _gridFrequency_Hz + bands.min_Hz,
                        flatband_Hz = _gridFrequency_Hz + bands.max_Hz,
                    }
                };
            }

            bands = _bands;

            bands.lower.gradient <- 1.0 * (bands.lower.flatband_kW - bands.lower.deadband_kW) / (bands.lower.deadband_Hz - bands.lower.flatband_Hz);
            bands.upper.gradient <- 1.0 * (bands.upper.flatband_kW - bands.upper.deadband_kW) / (bands.upper.flatband_Hz - bands.upper.deadband_Hz);
        }        
    }

    function _setTargetPower()
    {
        // Reset the timer
        _setTargetPowerTimer = imp.wakeup( 0.1, _setTargetPower.bindenv(this) );

        // Obtain the current frequency
        local frequency_Hz = _frequencySource.getFrequency().tofloat() / 1000.0;

        // Obtain the current state of charge (if available)
        local soc_percent = ("getStateOfCharge" in _commModule) ? _commModule.getStateOfCharge() : null;

        // Calculate the target power
        local targetPower_kW = _calculateTargetPower_kW( frequency_Hz, soc_percent );

        // Constrain the direction to comply with the programme
        if( (_direction == "high" && targetPower_kW > 0) ||
            (_direction == "low" && targetPower_kW < 0) )
        {
            targetPower_kW = 0;
        }

        // Apply asset power limits
        if( targetPower_kW > _power.max_kW )
        {
            targetPower_kW = _power.max_kW;
        }
        else if( targetPower_kW < (-_power.max_kW) )
        {
            targetPower_kW = -_power.max_kW;
        }

        // Apply state of charge limits (if available)
        if( soc_percent != null && _soc != null )
        {
            if( (soc_percent >= _soc.max_percent && targetPower_kW < 0) ||
                (soc_percent <= _soc.min_percent && targetPower_kW > 0) )
            {
                targetPower_kW = 0;
            }
        }

        // Round and set the target power on the asset
        targetPower_kW = round( targetPower_kW, 3 );

        if( targetPower_kW != _lastAppliedPower_kW )
        {
            _lastAppliedPower_kW = _adjustPowerTowards_kW( targetPower_kW, _lastAppliedPower_kW );
            _commModule.realPowerSetPoint( _lastAppliedPower_kW );
        }

        // Provide additional logging if in prequalification mode
        if( _dataRecorder.inPrequalification() )
        {
            local values = { frequency = (frequency_Hz * 1000).tointeger(), targetPower = _lastAppliedPower_kW };
            if( soc_percent != null ) { values.stateOfCharge <- soc_percent; }
            _dataRecorder.values( _id, values );
        }
    }

    function _calculateTargetPower_kW( frequency_Hz, soc_percent = null )
    {
        local lowerBand             = _bands.lower;
        local upperBand             = _bands.upper;
        local socDeadbandManagement = _socDeadbandManagement;

        // Deadband check
        if( frequency_Hz > lowerBand.deadband_Hz &&
            frequency_Hz < upperBand.deadband_Hz )
        {
            // We're within the DFFR deadband and so we shouldn't do anything, but we can manage the SoC
            if( socDeadbandManagement != null && soc_percent != null )
            {
                if( soc_percent < socDeadbandManagement.min_percent )
                {
                    return -socDeadbandManagement.power_kW;
                }
                else if( soc_percent > socDeadbandManagement.max_percent )
                {
                    return socDeadbandManagement.power_kW;
                }
            }

            return 0;
        }

        // Target Power Direction
        if( frequency_Hz < _gridFrequency_Hz )
        {
            // Target Power Calculation
            if( frequency_Hz < lowerBand.flatband_Hz )
            {
                return lowerBand.flatband_kW + _offset_kW;
            }
            else
            {
                return (lowerBand.deadband_kW + ((lowerBand.deadband_Hz - frequency_Hz) * lowerBand.gradient)) + _offset_kW;
            }
        }
        else
        {
            // Target Power Calculation
            if( frequency_Hz > upperBand.flatband_Hz )
            {
                return upperBand.flatband_kW + _offset_kW;
            }
            else
            {
                return (upperBand.deadband_kW + ((frequency_Hz - upperBand.deadband_Hz) * upperBand.gradient)) + _offset_kW;
            }
        }
    }

    function _adjustPowerTowards_kW( target, current )
    {
        if( "step_kW" in _power ) { return (target > current) ? min( target, current + _power.step_kW ) : max( target, current - _power.step_kW ); }
        else { return target; }
    }
}

class DynamicAsset extends Asset {
    _status = null;

    _ctrls = null;
    _ctrlDefault = null;
    _ctrlCurrent = null;
    _ctrlNext = null;

    _initTimer = null;
    _statusTimer = null;
    _startTimer = null;
    _endTimer   = null;

    constructor(id, name) {
        base.constructor(id, name);
    }

    function _buildStandardControls();
    function _buildDynamicControl(assetConf);
    function _startControl(detail=null);

    function _initControls(assetConf, defaultMode) {
        _ctrls = {};
        _ctrlDefault = defaultMode;
        _buildStandardControls();
        _buildDynamicControl(assetConf);

        _ctrlCurrent = {};
        _ctrlCurrent.mode  <- null;
        _ctrlCurrent.ctrl  <- null;
        _ctrlCurrent.begin <- null;
        _ctrlCurrent.end   <- null;

        _ctrlNext = {};
        _ctrlNext.mode  <- null;
        _ctrlNext.begin <- null;
        _ctrlNext.end   <- null;
    }

    function stop() {
        _startTimer = cancelTimer(_startTimer);
        _endTimer = cancelTimer(_endTimer);
        _statusTimer = cancelTimer(_statusTimer);
        _initTimer = cancelTimer(_initTimer);

        if(_ctrlCurrent.ctrl) { _ctrlCurrent.ctrl.stop(); }
    }

    function status() { return _status; }

    function operationInfo() {
        return {
            defaultMode = _ctrlDefault,
            current = {
                mode = _ctrlCurrent.mode,
                start = (_ctrlCurrent.begin ? printTimestamp(date(_ctrlCurrent.begin)) : null),
                end = (_ctrlCurrent.end ? printTimestamp(date(_ctrlCurrent.end)) : null)
            },
            next = {
                mode = _ctrlNext.mode,
                start = (_ctrlNext.begin ? printTimestamp(date(_ctrlNext.begin)) : null),
                end = (_ctrlNext.end ? printTimestamp(date(_ctrlNext.end)) : null)
            }
        }
    }

    function run(mode, detail, startTime=null, duration=null) {
        if(!(mode in _ctrls)) { throw mode + " unavailable"; }

        if(startTime) {
            // scheduled start
            _startTimer = cancelTimer(_startTimer);

            _ctrlNext.mode = mode;
            _ctrlNext.begin = startTime;
            _ctrlNext.end   = duration == null ? null : startTime + duration;

            _startTimer = imp.wakeup(startTime - time(), function() {
                run(mode, detail, null, duration);
            }.bindenv(this));
            logger.log(format("%s | scheduled '%s' for %s", name(), mode, printTimestamp(date(startTime))));
        } else {
            // immediate start
            _nullifyNext();
            _setCurrent(mode, duration);
            _startControl(detail);
            if(duration) {
                _endTimer = imp.wakeup(duration, _runDefault.bindenv(this));
                logger.log(format("%s | run '%s' for %is", name(), mode, duration));
            } else {
                logger.log(format("%s | run '%s'", name(), mode));
            }
        }
    }

    function cancelNext() {
        if(_ctrlNext.begin == null) {
        } else if(_ctrlNext.begin > time()) {
            logger.log(format("%s | cancel scheduled '%s'", name(), _ctrlNext.mode));
            _nullifyNext();
        } else { throw "scheduled control starting"; }
    }

    function _nullifyNext() {
        _startTimer = cancelTimer(_startTimer);
        _ctrlNext.mode  = null;
        _ctrlNext.begin = null;
        _ctrlNext.end   = null;
    }

    function _setCurrent(mode, duration) {
        if(_ctrlCurrent.ctrl) { _ctrlCurrent.ctrl.stop(); }
        _endTimer = cancelTimer(_endTimer);
        _ctrlCurrent.mode  = mode;
        _ctrlCurrent.ctrl  = _ctrls[mode];
        _ctrlCurrent.begin = time();
        _ctrlCurrent.end   = duration == null ? null : _ctrlCurrent.begin + duration;
    }

    function _runDefault() {
        if(_ctrlNext.begin == null || _ctrlNext.begin > time()) {
            _setCurrent(_ctrlDefault, null);
            _startControl();
            logger.log(format("%s | run '%s'", name(), _ctrlDefault));
        }
    }
}

class TargetPowerCalc {}

enum VariableAssetControlMode {
    IDLE = "idle",
    MANUAL_POWER = "manualPower",
    DFFR = "dynamicFFR"
}

/*
Variable asset is a directional asset (can either consume or deliver) with variable output, i.e. more than simply be on or off.
 */
class VariableAsset extends DynamicAsset {
    _comm = null;
    _fsrc = null;
    _record = null;
    _powerSettings = null;

    _assetType = null;
    _assetStatus = null;
    _currentFreq = null;
    _currentTargetPower_kW = null;

    _baseliner = null;
    _baseline = null;
    _blBuffer = null;
    _blMin = null;
    _blMax = null;

    constructor(assetConf, targetPowerComm, frequencySource, recorder, baseliner=null) {
        base.constructor(assetConf.id, assetConf.name);
        _assetType = assetConf.type;
        _comm = targetPowerComm;
        _fsrc = frequencySource;
        _record = recorder;
        _baseliner = baseliner;

        if(!_baseliner) { _blBuffer = array(1, 0.0) }
        else { _blBuffer = array(5, 0.0) }
        _baseline = 0.0;

        local blLimits = tableGet("baseline", assetConf);
        if(blLimits) {
            local direction = _assetType == AssetType.VARIABLE_LOAD ? CONSUME : DELIVER;
            local minLimit = tableGet("min", blLimits) * direction;
            local maxLimit = tableGet("max", blLimits) * direction;
            _blMin = min(minLimit, maxLimit);
            _blMax = max(minLimit, maxLimit);
        }

        _powerSettings = clone assetConf.powerSettings;

        _initControls(assetConf, VariableAssetControlMode.IDLE);

        _currentTargetPower_kW = 0.0;

        _assetStatus = SingleStatus.New().initialising();
        _status = AggregatedStatus(
            {
                comm = _comm.status.bindenv(_comm),
                lifecycle = (@() _assetStatus).bindenv(this)
            },
            function() {
                return {
                    name = name(),
                    readings = {
                        frequency = { mHz = _currentFreq }
                        targetPower = { kW = _currentTargetPower_kW },
                        baseline = { kW = _baseline }
                    },
                    operation = operationInfo()                }
            }.bindenv(this));

        imp.wakeup(0, _checkReady.bindenv(this));
    }

    function stop() {
        base.stop();
        _assetStatus.tornDown();
    }

    function currentBaseline() { return _blBuffer[0] }

    function _checkReady() {
        // todo probably need to rethink aggregated status ...
        if(_comm.status().isRunning() && _fsrc.status().isRunning() && (_baseliner == null || _baseliner.status().isRunning())) {
            _initTimer = cancelTimer(_initTimer);
            _assetStatus.running();
            _runDefault();
            imp.wakeup(0, _trackAndRecord.bindenv(this));
        } else {
            _initTimer = imp.wakeup(0.5, _checkReady.bindenv(this));
        }
    }

    function _trackAndRecord() {
        _statusTimer = imp.wakeup(0.99, _trackAndRecord.bindenv(this));

        local values = {};

        _currentFreq = _fsrc.getFrequency();
        _currentTargetPower_kW = _ctrlCurrent.ctrl.currentTargetPower_kW();

        local inPqDffr = _record.inPrequalification() && _ctrlCurrent.mode == VariableAssetControlMode.DFFR;
        if(!inPqDffr) {
            values.frequency <- _currentFreq;
            values.targetPower <- _currentTargetPower_kW;
            if(_baseliner) { values.baseline <- _ctrlCurrent.ctrl.currentBaseline() }
        }

        if(_baseliner) {
            local prediction = _baseliner.getPower();

            if(_blMin != null && prediction < _blMin) { prediction = _blMin }
            else if(_blMax != null && prediction > _blMax) { prediction = _blMax }

            _blBuffer.append(prediction);
            values.baselinePrediction <- prediction;

            if(inPqDffr) {
                _setBaselineToControl();
                _blBuffer.remove(0);
            } else {
                _blBuffer.remove(0);
                imp.wakeup(0.8, _setBaselineToControl.bindenv(this));
            }
        }

        if(values.len() > 0) { _record.values(id(), values) }
    }

    function _setBaselineToControl() {
        _baseline = _blBuffer[0];
        _ctrlCurrent.ctrl.setBaseline(_baseline);
    }

    function _startControl(params=null) {
        _ctrlCurrent.ctrl.setBaseline(_baseline);

        switch(_ctrlCurrent.mode) {
            case VariableAssetControlMode.MANUAL_POWER:
                _ctrlCurrent.ctrl.start(_comm, {
                    initTargetPower = _currentTargetPower_kW,
                    requestedPower = params.kW
                });
                break;
            case VariableAssetControlMode.DFFR:
                _ctrlCurrent.ctrl.start(_comm, _currentTargetPower_kW, params);
                break;
            case VariableAssetControlMode.IDLE:
            default: _ctrlCurrent.ctrl.start(_comm, _currentTargetPower_kW);
        }
    }

    function _buildStandardControls() {
        local stepChange = tableGet("staticStepChange_kW", _powerSettings);
        _ctrls[VariableAssetControlMode.IDLE] <- TargetPowerControl.Idle(stepChange);
        _ctrls[VariableAssetControlMode.MANUAL_POWER] <- TargetPowerControl.ManualPower(stepChange);
    }

    function _buildDynamicControl(assetConf) {
        foreach(mode, conf in assetConf.controlAlgorithms) {
            local ctrl = _buildConfiguredControl(mode, conf);
            if(ctrl) {
                _ctrls[VariableAssetControlMode.DFFR] <- ctrl;
            }
        }
    }

    function _buildConfiguredControl(mode, ctrlConf) {
        switch(mode) {
            case "dffr":
                return DFFRControl(id(), _fsrc, _record, _powerSettings, ctrlConf, _assetType);
            default: return null;
        }
    }
}

enum BatteryControlMode {
    IDLE = "idle",
    MANUAL_POWER = "manualPower",
    MANUAL_SOC = "manualStateOfCharge",
    DFFR = "dynamicFFR",
    CHARGE_MAX_RATE = "chargeMaxRate",
    DISCHARGE_MAX_RATE = "dischargeMaxRate"
}

class BatteryControl extends TargetPowerControl {}

class BatteryControl.ManualPower extends TargetPowerControl.ManualPower {
    _socSettings = null;

    constructor(socSettings, powerStepPerSec) {
        base.constructor(powerStepPerSec);
        _socSettings = socSettings;
    }

    function _calcTargetPower() {
        local soc = _comm.getStateOfCharge();
        if(soc >= _socSettings.max_percent && isCharging(_requested)) {
            return _adjustTowards(IDLE, _targetPower);
        } else if(soc <= _socSettings.min_percent && isDischarging(_requested)) {
            return _adjustTowards(IDLE, _targetPower);
        } else {
            return _adjustTowards(_requested, _targetPower);
        }
    }
}

class BatteryControl.ManualStateOfCharge extends BatteryControl {
    _maxPower = null;
    _requested = null;
    _comm = null;
    _timer = null;

    constructor(maxPower, powerStepPerSec) {
        _maxPower = maxPower;
        _step = powerStepPerSec;
    }

    function start(comm, params) {
        _comm = comm;
        _targetPower = params.initTargetPower;
        _requested = params.requestedSoc;
        imp.wakeup(0, _setTargetPower.bindenv(this));
    }

    function _setTargetPower() {
        if(_comm) {
            _timer = imp.wakeup(1, _setTargetPower.bindenv(this));
            local soc = _comm.getStateOfCharge().tointeger();

            if(soc < _requested) {
                _targetPower = _adjustTowards(_maxPower * CHARGE, _targetPower);
            } else if (soc > _requested) {
                _targetPower = _adjustTowards(_maxPower * DISCHARGE, _targetPower);
            } else {
                _targetPower = _adjustTowards(IDLE, _targetPower);
            }
            _comm.realPowerSetPoint(_targetPower);
        }
    }
}

class BatteryAsset extends DynamicAsset {
    _comm = null;
    _fsrc = null;
    _record = null;
    _socSettings = null;
    _powerSettings = null;

    _socStatus = null;
    _currentSoc = null;
    _currentFreq = null;
    _currentTargetPower_kW = null;
    _currentBaseline = null;

    _blBuffer = null;

    constructor(assetConf, batteryComm, frequencySource, recorder) {
        base.constructor(assetConf.id, assetConf.name);
        _comm = batteryComm;
        _fsrc = frequencySource;
        _record = recorder;

        _socSettings = clone assetConf.socSettings;
        _powerSettings = clone assetConf.powerSettings;

        _initControls(assetConf, BatteryControlMode.IDLE);

        _currentTargetPower_kW = 0;

        _blBuffer = array(5, null);

        _socStatus  = SingleStatus.New(function() {
            return { min = _socSettings.min_percent, max = _socSettings.max_percent };
        }.bindenv(this)).initialising();

        _status = AggregatedStatus({
                comm = _comm.status.bindenv(_comm),
                socThresholds = (@() _socStatus).bindenv(this)
            },
            function() {
                return {
                    name = name(),
                    readings = {
                        targetPower = { kW = _currentTargetPower_kW },
                        baseline = { kW = _currentBaseline },
                        frequency = { mHz = _currentFreq },
                        stateOfCharge = { percent = _currentSoc }
                    },
                    operation = operationInfo()
                }
            }.bindenv(this));

        imp.wakeup(0, _checkReady.bindenv(this));
    }

    function stop() {
        base.stop();
        _socStatus.tornDown();
    }

    function _checkReady() {
        if(_comm.status().isRunning() && _fsrc.status().isRunning()) {
            _initTimer = cancelTimer(_initTimer);
            _socStatus.running();
            _runDefault();
            imp.wakeup(0, _trackAndRecord.bindenv(this));
        } else {
            _initTimer = imp.wakeup(0.5, _checkReady.bindenv(this));
        }
    }

    function _trackAndRecord() {
        _statusTimer = imp.wakeup(0.99, _trackAndRecord.bindenv(this));

        local inPqDffr = _record.inPrequalification() && _ctrlCurrent.mode == BatteryControlMode.DFFR;
        local values = {};
        local prediction = null;
        local withBaselinePrediction = "predictBaseline" in _ctrlCurrent.ctrl;

        _currentSoc  = _comm.getStateOfCharge();
        _currentFreq = _fsrc.getFrequency();
        _currentTargetPower_kW = _ctrlCurrent.ctrl.currentTargetPower_kW();
        _currentBaseline = _ctrlCurrent.ctrl.currentBaseline();

        if(withBaselinePrediction) {
            prediction = _ctrlCurrent.ctrl.predictBaseline();
            values.baselinePrediction <- prediction;
        }
        _manageBaseline(prediction);

        if(_currentSoc < _socSettings.min_percent || _currentSoc > _socSettings.max_percent) {
            _socStatus.broken("outside operational thresholds "+_currentSoc);
        } else {
            _socStatus.running();
        }

        if(!inPqDffr) {
            values.frequency <- _currentFreq;
            values.stateOfCharge <- _currentSoc;
            values.targetPower <- _currentTargetPower_kW;
            if(withBaselinePrediction) { values.baseline <- _currentBaseline; }
        }

        if(values.len() > 0) { _record.values(id(), values) }
    }

    function _manageBaseline(prediction) {
        local _baseline = prediction == null ? 0 : _blBuffer[0];
        _ctrlCurrent.ctrl.setBaseline(_baseline == null ? 0 : _baseline);
        _blBuffer.append(prediction);
        _blBuffer.remove(0);
    }

    function _startControl(params=null) {
        switch(_ctrlCurrent.mode) {
            case BatteryControlMode.MANUAL_POWER:
                _ctrlCurrent.ctrl.start(_comm, {
                    initTargetPower = _currentTargetPower_kW,
                    requestedPower = params.kW
                });
                break;
            case BatteryControlMode.CHARGE_MAX_RATE:
                _ctrlCurrent.ctrl.start(_comm, {
                    initTargetPower = _currentTargetPower_kW,
                    requestedPower = (_powerSettings.maxPower_kW * -1)
                });
                break;
            case BatteryControlMode.DISCHARGE_MAX_RATE:
                _ctrlCurrent.ctrl.start(_comm, {
                    initTargetPower = _currentTargetPower_kW,
                    requestedPower = _powerSettings.maxPower_kW
                });
                break;
            case BatteryControlMode.MANUAL_SOC:
                _ctrlCurrent.ctrl.start(_comm, {
                    initTargetPower = _currentTargetPower_kW,
                    requestedSoc = params.percent
                });
                break;
            case BatteryControlMode.DFFR:
                _ctrlCurrent.ctrl.start(_comm, _currentTargetPower_kW, params);
                break;
            case BatteryControlMode.IDLE:
            default: _ctrlCurrent.ctrl.start(_comm, _currentTargetPower_kW);
        }
    }

    function _buildStandardControls() {
        local stepChange = tableGet("staticStepChange_kW", _powerSettings);
        _ctrls[BatteryControlMode.IDLE] <- TargetPowerControl.Idle(stepChange);
        _ctrls[BatteryControlMode.MANUAL_POWER] <- BatteryControl.ManualPower(_socSettings, stepChange);
        _ctrls[BatteryControlMode.CHARGE_MAX_RATE] <- BatteryControl.ManualPower(_socSettings, stepChange);
        _ctrls[BatteryControlMode.DISCHARGE_MAX_RATE] <- BatteryControl.ManualPower(_socSettings, stepChange);
        _ctrls[BatteryControlMode.MANUAL_SOC] <- BatteryControl.ManualStateOfCharge(_powerSettings.maxPower_kW, stepChange);
    }

    function _buildDynamicControl(assetConf) {
        foreach(mode, conf in assetConf.controlAlgorithms) {
            local ctrl = _buildConfiguredControl(mode, conf);
            if(ctrl) {
                _ctrls[BatteryControlMode.DFFR] <- ctrl;
                _ctrlDefault = BatteryControlMode.IDLE;
            }
        }
    }

    function _buildConfiguredControl(mode, ctrlConf) {
        switch(mode) {
            case "dffr" :
                return DFFRControl(id(), _fsrc, _record, _powerSettings, ctrlConf, _socSettings);
            default: return null;
        }
    }
}
// todo: asset
// "static ffr" is not an asset but a type of control which can be applied to an asset
// "static ffr" should be applicable to all BinaryAsset, not just relay
// keeping to the unit convention on Fruit: frequency should be in mHz, including in config
// is a cooloff time property of an asset? i.e. applies during ffr as well as STOR dispatch
// todo: manual <-> ffr edge cases
// there are possibilities that relay asset can be scheduled to run different program during different time of day
// - edge case around moving from ffr to manual, for example, frequency is tripped near the end of ffr period
//   - with the PiP, the trip frequencies get altered but relay doesn't get forced open ... so may be cool off duration is a property of an asset also?
// todo: readings
// for FFR program
// - ~10 times a second during pq
// - one a second during normal operation
// - the moment the frequency is tripped should also be recorded during normal operation
// bare in mind that during STOR readings only required to be one per minute ...
// we can go at 1s when outside FFR period for the time being and deal with that separately ... or may be inject a power source in so recording interval can be controlled by the asset?
// todo: other things
// currently it's exposed to manual dispatch: should it be possible to manual dispatch while in ffr mode???
// dispatch/cool off period in relation to device restart/reconfiguration?
class StaticFfrAsset extends RelayAsset {
    _name = null;
    _ctrl = null;
    _record = null;
    _fsrc = null;
    _timer = null;
    _readingsTimer = null;
    _coolOffTimer = null;
    _dispatchRequested = false;

    constructor(id, name, type, ctrl, fs, record) {
        _name = name;
        _ctrl = ctrl;
        _fsrc = fs;
        _record = record;
        base.constructor({"id": id, "name": name, "relayNumber": ctrl.relayNumber});

        local low = tableGetOrElse("low", _ctrl.tripPoint, 0);
        _ctrl.tripPoint.low <- low;
        local high = tableGetOrElse("high", _ctrl.tripPoint, 100);
        _ctrl.tripPoint.high <- high;

        imp.wakeup(0, _doStaticFfr.bindenv(this));
        imp.wakeup(0, _recordReading.bindenv(this));
    }

    function stop() {
        base.stop();
        _timer = cancelTimer(_timer);
    }

    // todo do we need to do anything special here?
    function cancel() {
        base.cancel();
    }

    function _recordReading() {
        local f = _fsrc.getFrequency();
        _record.values(id(), { frequency = f, dispatch = isDispatching().tointeger()});
        _readingsTimer = imp.wakeup(1, _recordReading.bindenv(this));
    }

    function _doStaticFfr() {
        local f = _fsrc.getFrequency();

        if (!isDispatching()) {
            // todo _dispatchRequested looks redundant: it is a combination of dispatching + cooldown
            if (_dispatchRequested && _coolOffTimer == null) {
                logger.log("start cool off: "+ clock);
                _coolOffTimer = imp.wakeup(_ctrl.coolOffDuration, _coolOffCompelete.bindenv(this));
            } else {
                if ((_dispatchRequested == false) && f && ((f <= _ctrl.tripPoint.low) || (f >= _ctrl.tripPoint.high))) {
                    logger.log("start dispatch: "+ clock);
                    beginDispatch(_ctrl.tripDuration);
                    _dispatchRequested = true;
                }
            }
        }
//        _record.values(id(), { frequency = f, dispatch = isDispatching().tointeger()});
        _timer = imp.wakeup(0.1, _doStaticFfr.bindenv(this));
    }

    function _coolOffCompelete() {
        logger.log("end cool off: "+ clock);
        _coolOffTimer = cancelTimer(_coolOffTimer);
        _dispatchRequested = false;
    }
}

// Common



class CommsConfig {
    _nwSettings = null;
    _commsConf = null;
    _wiz = null;
    _wizStopWatch = null;
    _completeCb = null;

    constructor(networkSettings, commsConf) {
        _nwSettings = networkSettings;
        _commsConf = commsConf;
    }

    // completeCb = function(comms)
    function build(completeCb) {
        _completeCb = completeCb;
        if((Build.modbusTcp && _nwSettings) || (Build.modbusTcpPowerOn && _nwSettings)) {
            imp.wakeup(0, _initWiz.bindenv(this));
        } else if (_commsConf) {
            _initComms();
        } else {
            logger.log("   No network to initialise");
            completeCb({});
        }
    }
    
    function _initWiz() {
        _wizStopWatch = hardware.millis();

        hardware.spi0.configure(CLOCK_IDLE_LOW | MSB_FIRST | USE_CS_L, 4000);
        _wiz = W5500(hardware.pinXA, hardware.spi0, null, hardware.pinM);

        _wiz.configureNetworkSettings(_nwSettings.address, _nwSettings.netmask, _nwSettings.gateway);
        _wiz.onReady(_initComms.bindenv(this));
    }
    
    function _initComms() {
        if (_wiz) { logger.log("   Init Wiznet took " + (hardware.millis() - _wizStopWatch) + "ms"); }
        local comms = {};
        if(_commsConf && _commsConf.len() > 0) {
            logger.log("-- Configuring "+ _commsConf.len() + " comms");
            foreach(conf in _commsConf) {
                local id = conf.id;
                try {
                    local type = conf.type.toupper();
                    switch(type) {
                        case CommScheme.MODBUS_TCP_MASTER:
                            local comm = ModbusTcpClientFactory.build(conf, _wiz);
                            comms[id] <- comm;
                            break;
                        case CommScheme.MODBUS_SERIAL_MASTER:
                        case CommScheme.MODBUS_SERIAL_SLAVE:
                            local comm = ModbusSerialFactory.build(conf);
                            comms[id] <- comm;
                            break;
                        case CommScheme.RS232:
                            // Note: For frenchMeter builds, UART 1 used for RS232
                            local comm = SerialRs232(hardware.uart1, conf);
                            comms[id] <- comm;
                            break;
                        default: logger.error("  Comm id:"+ id +" unknown comm type:"+ type);
                    }
                } catch(ex) {
                    logger.error("  Comm id:"+ id +" build exception:"+ ex);
                }
            }
        } else {
            logger.log("-- No comms to configure");
        }
        _completeCb(comms);
    }
}
class InputsConfig {
    _config = null;
    _comms = null;
    _segmentDriver = null;
    _recorder = null;

    constructor(inputConfig, meteringSegmentDriver, comms, recorder) {
        _config = inputConfig;
        _segmentDriver = meteringSegmentDriver;
        _comms = comms;
        _recorder = recorder;
    }

    /* returns array of inputs */
    function build() {
        local inputs = [];
        if(_config && _config.len() > 0) {
            logger.log("-- Configuring " + _config.len() + " inputs");
            foreach (conf in _config) {
                local i = _buildInput(conf);
                logger.log("    " + i.id() + ": " + i.name());
                inputs.append(i);
            }
        } else {
            logger.log("-- No inputs to configure");
        }
        return inputs;
    }

    function _buildInput(conf) {
        local id = conf.id;
        local name = conf.name;
        local pa1mn = "PA1MN ";
        local meterType = {"pa1mn": pa1mn, "date": "DATE ", "dateLen": 22, "ea": "EA_s ", "timeSeparator": ":"};

        try {
            local type = conf.type.toupper();
            switch(type) {
                case InputType.PULSE:
                    return _buildPulseInput(id, name, conf);
                case InputType.DIGITAL:
                    return _buildDigitalInput(id, name, conf);
                case InputType.METERING_SEGMENT:
                    if(Build.meteringSegment) { return _buildSegmentInput(id, name, conf) }
                    else { return FailedInput(id, name, "unsupported on this build") }
                case InputType.MODBUS_TCP_MASTER:
                case InputType.MODBUS_SERIAL_MASTER:
                    if(Build.modbusSerial || Build.modbusTcp) { return _buildModbusInput(id, name, conf); }
                    else { return FailedInput(id, name, "unsupported on this build") }
                case InputType.SERIAL:
                    if (conf.connection.toupper() == "RS232" && conf.measure.toupper() == "GNSS TIME") {
                        return _buildGnssTimeInput(id, name, conf);
                    } else {
                        throw "Unsupported serial input type";
                    }
                case InputType.PME_PMI:
                case InputType.ICE_GEN:
                case InputType.ICE:
                case InputType.SAPHIR:
                case InputType.SAPHIR_GEN:
                    if (type == InputType.ICE || type == InputType.ICE_GEN) {
                        if (type == InputType.ICE_GEN) { pa1mn = "IPA1MN "; }
                        meterType = {"pa1mn": pa1mn, "date": "DATECOUR ", "dateLen": 26, "ea": "EA ", "timeSeparator": "/"};
                    }
                    if (type == InputType.SAPHIR || type == InputType.SAPHIR_GEN) {
                        local ea = "EAS ";
                        if (type == InputType.SAPHIR_GEN) { ea = "EAI "; }
                        meterType = {"date": "DATE ", "dateLen": 22, "ea": ea, "timeSeparator": "/"};
                    }
                    if(Build.frenchMeter) { return _buildFrenchMeterRs232Input(id, name, conf, meterType); }
                    else { return FailedInput(id, name, "unsupported on this build") }
                default: return FailedInput(id, name, "unknown input type: "+ type);
            }
        } catch(ex) {
            logger.log(ex);
            return FailedInput(id, name, ex);
        }
    }

    function _buildPulseInput(id, name, conf) {
        local connector = tableGet("connection", conf);
        local recordOn = tableGetOrElse("record", conf, false);
        local measurementPeriod = tableGet("measurementPeriodSec", conf);

        local function buildPulseCounter() {
            local driver = PulseInputFactory.pulseCounter(connector);
            if("measure" in conf && conf.measure.toupper() == InputMeasure.POWER) {
                // todo should do tofloat() inside the constructor, not the caller?
                return PowerSource.PulseCounting(id, name, driver, measurementPeriod, conf.kWhPerPulse.tofloat(), _recorder, recordOn);
            } else {
                return PulseMetering(id, name, driver, conf.measurementPeriodSec, _recorder, recordOn);
            }
        }

        local function buildPulseTimer() {
            local driver = PulseInputFactory.pulseInterval(connector);
            return PulseMetering(id, name, driver, null, _recorder, recordOn);
        }

        return (measurementPeriod) ? buildPulseCounter() : buildPulseTimer();
    }

    function _buildDigitalInput(id, name, conf) {
        local connector = tableGet("connection", conf);
        local recordOn = tableGetOrElse("record", conf, false);
        local measurementPeriod = tableGet("measurementPeriodSec", conf);
        local driver = BinaryInputDriver(InputConnectors.toPin(connector));
        return DigitalInputMonitoring(id, connector, name, driver, conf.measurementPeriodSec, _recorder, recordOn);
    }

    function _buildSegmentInput(id, name, conf) {
        local recordOn = tableGetOrElse("record", conf, false);
        local readInterval = tableGetOrElse("readingIntervalInSec", conf, 1);
        local samplesOverInterval = tableGetOrElse("samplesOverInterval", conf, 1);
        local measureReactivePower = tableGetOrElse("measureReactivePower", conf, false);

        local measure = tableGet("measure", conf);
        if(!measure) { throw "Input measure not configured"; }
        else { measure = measure.toupper(); }

        local connection = tableGet("connection", conf);

        if(measure == InputMeasure.FREQUENCY) {
            if(!connection) { throw "Input connection not configured"; }

            // HACK!!! StaticFrequencyDriver for testing against fruit with no metering segment
            local driver = _segmentDriver ? _segmentDriver : StaticFrequencyDriver;

            return FrequencySource.MeteringSegment(id, name, connection.toupper(), driver, readInterval, samplesOverInterval, _recorder, recordOn);
        }

        if(measure == InputMeasure.POWER) {
            local voltageTransformerRatio = 1.0;
            local calibration = tableGet("calibration",  conf);
            local ctRating = tableGet("ctRatingInAmps", conf);
            if(!ctRating) { throw "Input ctRatingInAmps not configured"; }

            if ("voltageTransformerRatio" in conf) {
                local primary = tableGet("primary", conf.voltageTransformerRatio);
                local secondary = tableGet("secondary", conf.voltageTransformerRatio);

                if(primary && secondary) { voltageTransformerRatio = primary.tofloat()/secondary.tofloat(); }
                else { throw "Input voltageTransformerRatio must specify primary and secondary voltages"; }
            }

            // HACK!!! StaticPowerDriver for testing against fruit with no metering segment
            local driver = _segmentDriver ? _segmentDriver : StaticPowerDriver;

            if(connection) {
                return PowerSource.SinglePhaseMeteringSegment(id, name, driver, connection.toupper(),
                    ctRating, calibration, voltageTransformerRatio, readInterval, measureReactivePower, _recorder, recordOn);
            } else {
                return PowerSource.ThreePhaseMeteringSegment(id, name, driver,
                    ctRating, calibration, voltageTransformerRatio, readInterval, measureReactivePower, _recorder, recordOn);
            }
        }

        throw "Unsupported metering segment measure: "+ conf.measure;
    }

    function _buildModbusInput(id, name, conf) {
        local recordOn = tableGetOrElse("record", conf, false);
        local readInterval = tableGetOrElse("readingIntervalInSec", conf, 0.5);
        local comm = _comms[conf.connection];

        local measure = tableGet("measure", conf).toupper();
        if (measure == InputMeasure.FREQUENCY) {
            // todo validate comm interface
            return FrequencySource.Modbus(id, name, comm, readInterval, _recorder, recordOn);
        }

        if (measure == InputMeasure.POWER) {
            return PowerSource.Modbus(id, name, comm, readInterval, _recorder, recordOn);
        }

        return FailedInput(id, name, "unsupported input measure");
    }

    function _buildGnssTimeInput(id, name, conf) {
        local driver = RS232Driver(hardware.uart0, conf.baudRate, conf.wordSize, conf.parity, conf.stopBits, conf.controlFlags);
        return GnssTime(id, name, driver);
    }

    function _buildFrenchMeterRs232Input(id, name, conf, pmePmi) {
        local recordOn = tableGetOrElse("record", conf, false);
        local comm = _comms[conf.connection];
        return PowerSource.FrenchMeter(id, name, comm, _recorder, recordOn, pmePmi);
    }
}

class InputConnectors {
    static function toPin(connection) {
        switch (connection) {
            case PulseInputs.PULSE1: return hardware.pinT;
            case PulseInputs.PULSE2: return hardware.pinL;
            case PulseInputs.PULSE3: return hardware.pinXE;
            case PulseInputs.PULSE4: return hardware.pinY;
            default: throw "Unsupported Pulse Input";
        }
    }
}

/*
     Input P1  = hardware.pinT;
     Input P2  = hardware.pinL;
     Input P3  = hardware.pinXE;
     Input P4  = hardware.pinY;
*/
class PulseInputFactory {
    function pulseCounter(connection) {
        return PulseInputCounterDriver(InputConnectors.toPin(connection));
    }

    function pulseInterval(connection) {
        return PulseInputIntervalDriver(InputConnectors.toPin(connection));
    }
}
class AssetsConfig {
    _config = null;
    _inputs = null;
    _comms = null;
    _recorder = null;

    constructor(assetsConfig, inputs, comms, recorder) {
        _config = assetsConfig;
        _inputs = inputs;
        _comms = comms;
        _recorder = recorder;
    }

    function build() {
        local assets = [];
        if(_config && _config.len() > 0) {
            logger.log("-- Configuring "+ _config.len() + " assets");
            foreach (conf in _config) {
                local a = _buildAsset(conf);
                logger.log("    "+ a.id() + ": " + a.name());
                assets.append(a);
            }
        } else {
            logger.log("-- No assets to configure");
        }
        return assets;
    }

    function _buildAsset(conf) {
        local id = conf.id;
        local name = conf.name;
        local type = tableGet("type", conf);

        try {
            if(type) {
                type = type.tolower();
                switch(type) {
                    case AssetType.RELAY:
                        return _buildRelay(conf);
                    case AssetType.STATIC_FFR_RELAY:
                        return _buildStaticFfrRelay(conf);
                    case AssetType.BATTERY:
                        if(Build.dynamicAsset) { return _buildBattery(conf) }
                        else { return _unsupportedBuild(id, name) }
                    case AssetType.VARIABLE_GENERATOR:
                    case AssetType.VARIABLE_LOAD:
                        if(Build.dynamicAsset) { return _buildVariable(conf) }
                        else { return _unsupportedBuild(id, name) }
                    default:
                        return FailedAsset(id, name, "unknown asset type: "+ type);
                }
            } else {
                return FailedAsset(id, name, "unknown configuration");
            }
        } catch(ex) {
            return FailedAsset(id, name, ex);
        }
    }

    function _buildRelay(conf) {
        return RelayAsset(conf);
    }

    function _buildStaticFfrRelay(conf) {
        local dependencies = tableGet("dependencies", conf);
        local fsrc = _findInput(dependencies.frequency, FrequencySource);
        //todo clean up parameters passed to StaticFfrAsset
        return StaticFfrAsset(conf.id, conf.name, conf.type, conf.controlAlgorithms.staticFfr, fsrc, _recorder);
    }

    function _buildBattery(conf) {
        local dependencies = tableGet("dependencies", conf);
        local comm = _findComm(dependencies.comm);
        local fsrc = _findInput(dependencies.frequency, FrequencySource);
        return BatteryAsset(conf, comm, fsrc, _recorder);
    }

    function _buildVariable(conf) {
        local dependencies = tableGet("dependencies", conf);
        local comm = _findComm(dependencies.comm);
        local fsrc = _findInput(dependencies.frequency, FrequencySource);
        local baseliner = ("baseline" in dependencies) ? _findInput(dependencies.baseline, PowerSource) : null;
        return VariableAsset(conf, comm, fsrc, _recorder, baseliner)
    }

    function _unsupportedBuild(id, name) { return FailedAsset(id, name, "unsupported on this build"); }

    // todo deprecate
    function _controlType(ctrl) {
        local name = null;
        if(ctrl.len() == 1) { foreach(k, v in ctrl) { name = k; } }
        return name;
    }

    function _findComm(id) {
        local comm = tableGet(id, _comms);
        if(comm) return comm;
        else throw "comm "+ id +" not found";
    }

    function _findInput(id, clazz) {
        foreach(i in _inputs) {
            if(i.id() == id) {
                if(i instanceof clazz) return i;
                else throw "input "+ id +" incorrect type";
            };
        }
        throw "input "+ id +" not found";
    }
}
class BridgeConfig {
    _config = null;
    _comms = null;

    constructor(bridgeConfig, comms) {
        _config = bridgeConfig;
        _comms = comms;
    }

    function build() {
        local bridges = [];
        if(_config && _config.len() > 0) {
            logger.log("-- Configuring "+ _config.len() + " bridge");
            foreach (conf in _config) {
                local b = _buildBridge(conf);
                logger.log("    "+ b.id() + ": " + b.name());
                bridges.append(b);
            }
        } else {
            logger.log("-- No bridge to configure");
        }
        return bridges;
    }

    function _buildBridge(conf) {
        local id = conf.id;
        local name = conf.name;
        local type = tableGet("type", conf);

        try {
            switch(type.tolower()) {
                case BridgeType.POWERON:
                    if(Build.modbusTcpPowerOn) { return _buildPoweronBridge(conf) }
                    else { return _unsupportedBuild(id, name) }
                default:
                    return FailedBridge(id, name, "unknown bridge type: "+ type);
            }
        } catch(ex) {
            return FailedBridge(id, name, ex);
        }
    }

    function _buildPoweronBridge(conf) {
        local dependencies = tableGet("dependencies", conf);
        local comm = _findComm(dependencies.comm);
        return PoweronBridge(conf.id, conf.name, comm, POWERON_MAX_ZONES);
    }

    function _unsupportedBuild(id, name) { return FailedBridge(id, name, "unsupported on this build"); }

    function _findComm(id) {
        local comm = tableGet(id, _comms);
        if(comm) return comm;
        else throw "comm "+ id +" not found";
    }
}

class FruitConfig {
    static NOOP = { kompUrl = "", stationId = "none" };

    _config          = null;
    _kompUrl         = null;
    _meteringSegment = null;
    _indicator = null;
    _record        = null;

    _comms       = null;
    _inputs      = null;
    _assets      = null;
    _bridges     = null;
    _reqHandlers = null;

    _singleStatus    = null;
    _combinedStatus  = null;
    _initCompleted   = null;
    _tearDownCount   = null;
    _tearDownCompleted = null;

    constructor(config, requestHandlers, meteringSegment, statusIndicator, recorder) {
        _config = config;
        _kompUrl = config.kompUrl;

        _meteringSegment = meteringSegment;
        _indicator = statusIndicator;
        _record = recorder;
        _reqHandlers = requestHandlers ? requestHandlers : [];

        _comms  = {};
        _inputs = [];
        _assets = [];
        _bridges = [];
        _singleStatus = SingleStatus.New();
        _combinedStatus = AggregatedStatus();
    }

    static function NoOp(recorder) { return FruitConfig(NOOP, null, null, null, recorder); }
    function specified() { return _config; }

    function status() {
        try {
            return _combinedStatus ? _combinedStatus : _singleStatus;
        } catch (exception) {
            return _singleStatus.broken(exception);
        }
    }

    function init(onComplete) {
        logger.log("---------------- Configure ----------------");
        _initCompleted = onComplete;
        _setupRecorder();
        _combinedStatus.register(_onStatusChanged.bindenv(this));
        CommsConfig(tableGet("networkSettings", _config), tableGet("comms", _config))
            .build(_continueInitialisation.bindenv(this));
    }
    
    function tearDown(onComplete) {
        _tearDownCount     = 0;
        _tearDownCompleted = onComplete;
        _record.stop();

        foreach (h in _reqHandlers) { h.unapply(); }
        _reqHandlers = null;
        
        foreach (a in _assets) { a.stop(); }
        foreach (i in _inputs) { i.stop(); }
        foreach (c in _comms)  { c.stop(); }
        foreach (b in _bridges) { b.stop(); }

        imp.wakeup(0, _checkTearDownCompleted.bindenv(this));
    }

    function instanceName(id, requestedClass) {
        return (_comms[id] instanceof requestedClass);
    }

    function _continueInitialisation(comms) {
        _comms = comms;

        foreach(id, comm in _comms) {
            _combinedStatus.append(id, comm.status.bindenv(comm));
            _registerToHandlers(comm);
        }

        _inputs = InputsConfig(tableGet("inputs", _config), _meteringSegment, _comms, _record).build();
        foreach(i in _inputs) {
            _combinedStatus.append(i.name(), i.status.bindenv(i));
            _registerToHandlers(i);
        }

        _assets = AssetsConfig(tableGet("assets", _config), _inputs, _comms, _record).build();
        foreach(a in _assets) {
            _combinedStatus.append(a.name(), a.status.bindenv(a));
            _registerToHandlers(a);
        }

        _bridges = BridgeConfig(tableGet("bridges", _config), _comms).build();
        foreach(b in _bridges) {
            agent.on("wpdPowerOn.dispatch", b.dispatch.bindenv(b));
            agent.on("wpdPowerOn.dispatchStatus", b.dispatchStatus.bindenv(b));
            _combinedStatus.append(b.name(), b.status.bindenv(b));
            _registerToHandlers(b);
        }

        if(_inputs.len() > 0 || _assets.len() > 0) { _record.start(); }
        if(_bridges.len() > 0) {
            agent.send("wpdPowerOn.init", { stationId = _config.stationId, kompUrl = _kompUrl })
        };

        _initCompleted();
        if ("scheduledSettings" in _config) {
            imp.wakeup(5, _runScheduledSettings.bindenv(this));
        }
    }

    function _runScheduledSettings() {
        batteryHandler.process(_config.scheduledSettings);
    }

    function _checkTearDownCompleted() {
        local status = status().value();

        if(status == StatusValue.TORN_DOWN || status == StatusValue.IDLE) {
            _tearDownCompleted();

            _singleStatus.tornDown();
            _combinedStatus = null;
            _assets.clear();
            _inputs.clear();
            _comms.clear();
            _bridges.clear();
        } else {
            _tearDownCount = _tearDownCount + 1;
            logger.log("Tear down count: "+ _tearDownCount);
            imp.wakeup(0.5, _checkTearDownCompleted.bindenv(this));
        }
    }

    function _onStatusChanged(status) {
        agent.send("status", {"name":status.name(),"since":status.since()});
        if(_indicator) { _indicator.statusChanged(status); }
    }

    function _registerToHandlers(entity) { foreach(h in _reqHandlers) { h.applyTo(entity); } }

    function _setupRecorder() {
        local sendInSec   = 1;
        local prequalMode = false;
        local systemDebug = false;

        local readings = tableGet("readings", _config);
        if(readings) {
            if("sendIntervalInSec" in readings) { sendInSec = readings.sendIntervalInSec; }
            if("prequalification" in readings) { prequalMode = readings.prequalification; }
            if("systemDebug" in readings) { systemDebug = readings.systemDebug; }
        }

        agent.send("readings.init", { stationId = _config.stationId, readingsUrl = _kompUrl, prequalification = prequalMode });
        _record.configure(sendInSec, prequalMode, systemDebug);
    }
}
class Fruit {
    _previous = null;
    _current = null;
    _cfgLock = false;

    _segment = null;
    _indicator = null;
    _record = null;
    _requestHandlers = null;
    _shutdownRequested = null;

    constructor(recorder, statusIndicator = null, meteringSegment = null) {
        _requestHandlers = [];
        _indicator = statusIndicator;
        _segment = meteringSegment;
        _record = recorder;

        _previous = FruitConfig.NoOp(recorder);
        _current = FruitConfig.NoOp(recorder);
    }

    function updateConfig(config) {
        if(_cfgLock) throw "reconfiguration in-progress";
        _cfgLock = true;
        _previous = _current;
        _current  = FruitConfig(config, _requestHandlers, _segment, _indicator, _record);
        _previous.tearDown(_onPreviousTornDown.bindenv(this));
    }

    function _onPreviousTornDown() { _current.init(_onCurrentInitialised.bindenv(this)); }
    function _onCurrentInitialised() { _cfgLock = false; dispatcher.scheduleNextDispatch(); }
    function registerHandler(handler) { _requestHandlers.append(handler); }
    function currentConfiguration() { return _current.specified(); }
    function previousConfiguration() { return _previous.specified(); }
    function eraseSpiFlash() { return _record.eraseSpiFlash(); }

    function status() {
        return {
            "currentConfiguration": _current.status().render(),
            "previousConfiguration": _previous.status().render(),
            "systemInfo": {
                "meteringSegmentDetected": _segment == null ? false : true,
                "memoryFree": imp.getmemoryfree(),
                "deviceTime": printTimestamp(date()),
                "shutdownRequested": _shutdownRequested
            }
        }
    }

    function shutdownRequestReceived(reason) {
        _shutdownRequested = {
            "reason": reason,
            "at"    : printTimestamp(date())
        };
    }
}
class DeviceConnection {
    _connected = true;
    _callbacks = null;
    _goneAt = null;

    constructor() {
        _callbacks = [];
    }

    function onChange(reason) {
        if (reason == SERVER_CONNECTED) {
            kwTime.checkInvalid();
            if(_goneAt != null) { logger.log("Device disconnected for "+ (time() - _goneAt) +"s") }
            _connected = true;
            _goneAt = null;
        } else {
            _connected = false;
            if(_goneAt == null) { _goneAt = time() }
            server.connect(onChange.bindenv(this), 60);
        }
        foreach(cb in _callbacks) { cb(_connected); }
    }

    /*
        cb(isConnected: boolean)
        ensure callbacks are from instances that remains throughout the lifetime of the device while powered on
        such as Fruit and handlers
    */
    function register(cb) {
        _callbacks.append(cb);
        cb(_connected);
    }
}

local fruitConnection = DeviceConnection();

// Set the timeout policy to RETURN_ON_ERROR, ie. to continue running squirrel on network disconnect
server.setsendtimeoutpolicy(RETURN_ON_ERROR, WAIT_TIL_SENT, 10);
// Register the network disconnection handler
server.onunexpecteddisconnect(fruitConnection.onChange.bindenv(fruitConnection));

local fruitRecorder = Recorder();
agent.on("recorder.backoff", fruitRecorder.readingsBackoff.bindenv(fruitRecorder));

function buildFruit(deviceConnection, recorder) {
    local statusIndicator = null;
    local meteringSegment = null;

    local lp5521 = LP5521(hardware.i2c0)
    statusIndicator = StatusIndicator(lp5521);
    statusIndicator.init();
    if(deviceConnection) { deviceConnection.register(statusIndicator.onConnection.bindenv(statusIndicator)); }

    if(Build.meteringSegment) {
        local ade7858 = ADE7858();
        if(ade7858.init()) {
            logger.log("-- Metering segment detected");
            meteringSegment = ade7858;
        }
    }

    return Fruit(recorder, statusIndicator, meteringSegment);
}

logger.log ("---> Device Start <---");
local fruit = buildFruit(fruitConnection, fruitRecorder);

agent.send("device.impversion", imp.getsoftwareversion())

server.onshutdown(function(reason) {
    local desc = null;
    switch(reason) {
        case SHUTDOWN_NEWSQUIRREL :
            desc = "New Squirrel is available";
            break;
        case SHUTDOWN_NEWFIRMWARE :
            desc = "New impOS firmware is available";
            break;
        case SHUTDOWN_OTHER :
            desc = "A restart has been requested for other reason";
            break;
        default: desc = "A restart has been requested for unknown reason";
    }
    logger.log(">>> Shut down request received: "+ reason + ":"+ desc);
    fruit.shutdownRequestReceived(desc);
});

//====================================================================
// Handlers to be instantiated and setup after fruit instance existed
//====================================================================

class RequestHandler {
    function applyTo(entity);
    function unapply();
}

class BinaryDispatchHandler extends RequestHandler {
    _asset = null;

    constructor(theFruit) {
        theFruit.registerHandler(this);
    }

    // todo make _asset a table once kompv2 can deal with asset id
    function applyTo(asset) {
        if(asset instanceof BinaryAsset) { _asset = asset; }
    }

    function unapply() { _asset = null; }

    function dispatch(req) {
        local contextId = req.contextId;
        if(_asset == null) {
            Response.notFound(contextId);
        } else {
            try {
                _asset.beginDispatch(req.durationInSec);
                Response.success(contextId);
            } catch (exception) {
                Response.serverError(contextId, exception);
            }
        }
    }

    function dispatchStatus(contextId) {
        if(_asset == null) { Response.notFound(contextId); }
        else { Response.success(contextId, { dispatching = _asset.isDispatching() }); }
    }

    function restartDevice(contextId) {
        if (_asset != null && _asset.isDispatching()) Response.serverError(contextId, "Asset currently dispatching");
        else {
            Response.restartAgent(contextId, {message="Device Restarting"})
            logger.log("Device Restarting...")
            server.restart();
        }
    }
}
local dispatchHandler = BinaryDispatchHandler(fruit);
agent.on("binary.dispatch", dispatchHandler.dispatch.bindenv(dispatchHandler));
agent.on("binary.dispatch.status", dispatchHandler.dispatchStatus.bindenv(dispatchHandler));
agent.on("fruit.restart",dispatchHandler.restartDevice.bindenv(dispatchHandler));

class FruitHandler {
    _fruit = null;
    _timer = null;
    _cfg = false;

    constructor(theFruit) {
        _fruit = theFruit;
    }
    
    function setConfig(req) {
        try {
            _configure(req.payload);
            Response.success(req.contextId, req.payload);
        } catch(error) {
            Response.serviceUnavailable(req.contextId, error);
        }
    }

    function onConnection(connected) {
        if(!connected) { _timer = cancelTimer(_timer); }
        else if(connected && !_timer) { imp.wakeup(0, _retrieve.bindenv(this)); }
    }

    function _retrieve() {
        if(_cfg) { _timer = cancelTimer(_timer); }
        else {
            agent.send("fruit.retrieveConfig", "");
            _timer = imp.wakeup(10, _retrieve.bindenv(this));
        }
    }

    function revive(config) {
        try { _configure(config); }
        catch(ex) {}
    }

    function getConfig(contextId) {
        Response.success(contextId, _fruit.currentConfiguration());
    }

    function getStatus(contextId) {
        safely(contextId, @() _fruit.status())
    }

    function eraseSpiFlash(contextId) {
        Response.success(contextId, _fruit.eraseSpiFlash());
    }

    function _configure(cfg) {
        _cfg = true;
        _timer = cancelTimer(_timer);
        if(cfg) { _fruit.updateConfig(cfg); }
    }

    function safely(contextId, doThing) {
        try {
            Response.success(contextId, doThing());
        } catch(message) {
            logger.error(message);
            Response.serverError(contextId, message);
        }
    }
}
local fruitHandler = FruitHandler(fruit);
agent.on("fruit.setConfig", fruitHandler.setConfig.bindenv(fruitHandler));
agent.on("fruit.getConfig", fruitHandler.getConfig.bindenv(fruitHandler));
agent.on("fruit.getStatus", fruitHandler.getStatus.bindenv(fruitHandler));
agent.on("fruit.reviveConfig", fruitHandler.revive.bindenv(fruitHandler));
agent.on("fruit.eraseSpiFlash", fruitHandler.eraseSpiFlash.bindenv(fruitHandler));
fruitConnection.register(fruitHandler.onConnection.bindenv(fruitHandler));

class FrequencyInjectionHandler extends RequestHandler {
    _frequencySource = null;
    _startTimer = null;

    constructor(theFruit) {
        theFruit.registerHandler(this);
    }

    function applyTo(input) {
        if(input instanceof FrequencySource) _frequencySource = input;
    }

    function unapply() { _frequencySource = null; }

    function start(request) {
        if(_frequencySource == null) {
            Response.notFound(request.contextId);
            return;
        }

        if("payload" in request && "profile" in request.payload && request.payload.len() > 0) {

            // Clear any existing start timer
            if(_startTimer != null) {
                imp.cancelwakeup(_startTimer);
                _startTimer = null;
            }

            // Determine if we should run the profile right away or schedule it
            if("startTime" in request.payload && request.payload.startTime != 0) {

                // Validate the start time
                local startTime = request.payload.startTime;
                local currentTime = time();

                if(startTime <= currentTime) {
                    Response.badRequest(request.contextId, "start time has already passed");
                    return;
                }

                // Schedule the profile to run at startTime
                _startTimer = imp.wakeup(startTime - currentTime, function() { _run(request.payload.profile); }.bindenv(this));

                logger.log("Profile Scheduled to run in " + (startTime - currentTime) + "s");

                Response.success(request.contextId);
            } else {

                // Immediately run the profile
                _run(request.payload.profile);
                Response.success(request.contextId);
            }
        } else {
            Response.badRequest(request.contextId, "profile not provided");
        }
    }

    function stop(request) {
        if(_frequencySource == null) {
            Response.notFound(request.contextId);
            return;
        }

        // Clear any existing start timer
        if(_startTimer != null) {
            imp.cancelwakeup(_startTimer);
            _startTimer = null;
        }

        _frequencySource.endProfile();
        Response.success(request.contextId);
    }

    function _run( profile ) {
        logger.log("---- Start frequency profile: "+ printTimestamp(date()));
        _frequencySource.runWithProfile(FrequencyProfile(profile));
    }
}
local frequencyInjectionHandler = FrequencyInjectionHandler(fruit);
agent.on("frequencyInjection.start", frequencyInjectionHandler.start.bindenv(frequencyInjectionHandler));
agent.on("frequencyInjection.stop", frequencyInjectionHandler.stop.bindenv(frequencyInjectionHandler));


class ModbusRequestHandler extends RequestHandler {
    _doctor = null;
    _contextId = null;

    constructor(theFruit) {
        theFruit.registerHandler(this);
    }

    function applyTo(entity) {
        if(Build.modbusTcp && entity instanceof ModbusTcpClient) {
            _doctor = ModbusDoctor(entity);
        }
    }

    function unapply() { _doctor = null; }

    function runDiagnostics(req) {
        _perform(req, function(req) {
            _doctor.runDiagnostics(_diagnosticResults.bindenv(this));
        });
    }

    function runReadTest(req) {
        _perform(req, function(req) {
            local data = req.payload;
            local registerType = _mapRegisterType(data.targetType);
            _doctor.readTest(registerType, data.address, data.quantity, _actionResult.bindenv(this));
        });
    }

    function runWriteTest(req) {
        _perform(req, function(req) {
            local data = req.payload;
            local registerType = _mapRegisterType(data.targetType);
            _doctor.writeTest(registerType, data.address, data.quantity, data.values, _actionResult.bindenv(this));
        });
    }

    function _perform(req, fn) {
        local contextId = req.contextId;
        if (_doctor == null) {
            Response.notFound(contextId);
        } else if(_contextId) {
            Response.serverError(contextId, "dianostics in-progress");
        } else {
            try {
                _contextId = contextId;
                fn(req);
            } catch (ex) {
                _contextId = null;
                Response.serverError(contextId, ex);
            }
        }
    }

    function _diagnosticResults(data) {
        Response.success(_contextId, data);
        _contextId = null;
    }

    function _actionResult(error, results) {
        Response.success(_contextId, { "result": (error ? error : results) });
        _contextId = null;
    }

    function _mapRegisterType(targetType) {
        switch (targetType) {
            case "HOLDING_REGISTER": return MODBUSRTU_TARGET_TYPE.HOLDING_REGISTER;
            case "COIL": return MODBUSRTU_TARGET_TYPE.COIL;
            case "DISCRETE_INPUT": return MODBUSRTU_TARGET_TYPE.DISCRETE_INPUT;
            case "INPUT_REGISTER": return MODBUSRTU_TARGET_TYPE.INPUT_REGISTER;
            default: throw "Invalid target register type: "+  targetType; // should already been caught by agent
        }
    }
}
local modbusRequestHandler = ModbusRequestHandler(fruit);
agent.on("modbus.diagnostics", modbusRequestHandler.runDiagnostics.bindenv(modbusRequestHandler));
agent.on("modbus.readTest", modbusRequestHandler.runReadTest.bindenv(modbusRequestHandler));
agent.on("modbus.writeTest", modbusRequestHandler.runWriteTest.bindenv(modbusRequestHandler));

/*
local ledDriver = LP5521(hardware.i2c0);
ledDriver.init();

function setLED(req) {
    logger.log("Got LED request for color: "+ req.color);
    local color = req.color;

    if(color == 0) {
        ledDriver.off();
    } else {
        ledDriver.blink(color);
    }
}

agent.on("led.light", setLED);
*/

/// Handles the receipt of a dispatch schedule from the DispatcherAgent; storing, scheduling and actioning each dispatch.
/// Is a singleton - including this file creates the global object 'dispatcher'.
class DispatcherDevice
{
    // Static Attributes
    //------------------
    static VERSION      = "0.1.0";  ///< The Major.Minor.Patch version of the class.
    static FLASH_SECTOR = 0x0000;   ///< The 4KB flash sector to persist the schedule in (must be a multiple of 4096/0x1000).
    
    // Attributes
    //-----------
    _flash                  = hardware.spiflash;    ///< The hardware SPI flash peripheral.
    _refreshScheduleTime_s  = 3600;                 ///< The time in seconds between schedule refreshes \see _refreshSchedule().
    _fruit                  = null;                 ///< A reference to the fruit object dispatches are to be actioned against.
    _schedule               = null;                 ///< The currently active schedule.
    _revision               = 0;                    ///< The revision number of the currently active schedule (used for synchronisation).
    _nextDispatchTimer      = null;                 ///< A reference to the timer that will trigger the next scheduled dispatch.
    _refreshScheduleTimer   = null;                 ///< A reference to the timer that will trigger the next schedule refresh.
    _lastAssetAction        = null;                 ///< A table containing the last action (mode and params) issued to each asset by the Dispatcher.
    
    // Constructor
    //------------
    /// Initialises the Dispatcher by:
    /// - Registering webhook, device and device connection handlers.
    /// - Refreshing the current schedule (arranging to wakeup on the next dispatch).
    constructor( fruit )
    {
        _fruit = fruit;
        _lastAssetAction = {};

        // Assign agent handlers
        agent.on( "dispatcher.synchronise.request", _onSynchroniseRequest.bindenv(this) );

        // Currently we don't load the stored schedule at initialisation time as we don't have a wall clock,
        // we instead wait for the agent to sync (flash storage of the schedule is currently redundant).
        _schedule = [];

        // Refresh the schedule
        _refreshSchedule();
    }

    // Handlers
    //---------
    /// Handles agent synchonisation requests (issued by the agent when a new schedule is available).
    /// Calls the local function synchroniseSchedule().
    /// \param newSchedule the new schedule to by synchronised.
    function _onSynchroniseRequest( newSchedule )
    {
        synchroniseSchedule( newSchedule );

        agent.send( "dispatcher.synchronise.response", newSchedule.revision );
    }

    // Methods
    //--------
    /// Clears asset action history.
    /// Can be used to prevent increasing memory consumption when assets are added / removed. 
    function clearAssetHistory()
    {
        _lastAssetAction = {};
    }

    /// Synchronises a new schedule (typically received from the agent)
    /// by storing it locally and scheduling the next dispatch based upon the new schedule.
    /// \param newSchedule the new schedule to be synchronised.
    function synchroniseSchedule( newSchedule )
    {
        // Store the Schedule
        storeSchedule( newSchedule );

        // Schedule the next dispatch
        scheduleNextDispatch();
    }

    /// Stores a new schedule (with revision) both internally and in persistent flash storage.
    /// \param  newSchedule the schedule to be stored (must be serializable).
    /// \note   flash storage is currently configured to support a maximum size of 4KB (one page).
    function storeSchedule( newSchedule )
    {
        _schedule = newSchedule.schedule;
        _revision = newSchedule.revision;

        local serializedSchedule = Serializer.serialize( newSchedule );

        _flash.enable();
        _flash.erasesector( FLASH_SECTOR );
        _flash.write( FLASH_SECTOR, serializedSchedule );
        _flash.disable();      
    }

    /// Schedules the next dispatch based upon the current schedule.
    /// \note Will do nothing if there's no config currently present, relying upon a manual trigger by the fruit once a config is present.
    function scheduleNextDispatch()
    {
        if( _compareContainers( _fruit.currentConfiguration(), FruitConfig.NOOP ) == true || _fruit._cfgLock == true ) { return; }

        if( _schedule.len() > 0 )
        {
            local wakeupTime = _schedule[0].start - time();
            
            if( wakeupTime < 0 )
            {
                wakeupTime = 0;
            }
            
            if( _nextDispatchTimer != null )
            {
                imp.cancelwakeup( _nextDispatchTimer );
            }

            _nextDispatchTimer = imp.wakeup( wakeupTime, dispatch.bindenv(this) );
        }
    }

    /// Actions the latest scheduled dispatches for each asset that are now ready to run, actions them then schedules the next dispatch.
    /// Will requeue actions that fail due assets to not being ready will and reschedule them immediately until they're no-longer relevant.
    function dispatch()
    {
        local currentDispatches = {};
        local currentTime = time();

        // Dispatch all current actions
        while( _schedule.len() > 0 )
        {
            if( _schedule[0].start > currentTime )
            {
                break;
            }

            local dispatch = _schedule.remove(0);
            currentDispatches[dispatch.assetId] <- dispatch;
        }

        foreach( assetId, dispatch in currentDispatches )
        {
            dispatch.action.params.start <- dispatch.start;

            if( action( assetId, dispatch.action ) == false )
            {
                dispatch.action.failedPreviously <- true;
                _schedule.insert( 0, dispatch );
            }
        }

        // Schedule a wake-up for the next dispatch action
        scheduleNextDispatch();
    }

    /// Retrieves the targeted asset from the current fruit config then translates and executes the passed action based on asset type.
    /// \param  assetID the ID string of the asset to perform the action on.
    /// \param  action a table containing the 'mode' string and a table of 'params' of the action to be performed.
    /// \return 'null' on success, 'false' if the asset wasn't ready, otherwise an error string.
    function action( assetID, action )
    {
        // Do nothing if the action matches the last action issued to the asset
        /// \warning Actioning assets outside of the dispatcher will cause incorrect behaviour.
        if( assetID in _lastAssetAction && _compareContainers( action, _lastAssetAction[assetID] ) == true )
        {
            return null;
        }

        // Fetch a reference to the asset to be actioned
        local asset = fetchAsset( assetID );

        if( asset == null )
        {
            return logger.error( "The asset to be actioned could not be found" );
        }

        if( asset instanceof StaticFfrAsset )
        {
            return logger.error( "StaticFFrAsset not supported" );
        }

        // Determine if the asset is initialised or not
        if( asset.status()._state == StatusValue.INITIALISING || asset.status()._state == StatusValue.TEARING_DOWN || asset.status()._state == StatusValue.TORN_DOWN )
        {
            if( !("failedPreviously" in action) ) { logger.log( "The asset was not available, will automatically reattempt when the asset becomes available" ); }
            return false;
        }

        if( asset instanceof RelayAsset )
        {
            local errorMessage = asset.action( action );
            if( errorMessage != null ) { return errorMessage; }

            _lastAssetAction[assetID] <- action;

            return null;
        }

        if( asset instanceof BatteryAsset )
        {
            switch( action.mode )
            {
                case "targetPower":

                    /// \note Taken from DynamicAsset.run()
                    asset._nullifyNext();
                    asset._setCurrent( "manualPower", null );
                    asset._startControl( { "kW" : action.params.kW, } );
                    logger.log( format( "%s | run '%s'", asset.name(), asset._ctrlCurrent.mode ) );
                    break;

                case "targetSoC":
                    /// \note Taken from DynamicAsset.run()
                    asset._nullifyNext();
                    asset._setCurrent( "manualStateOfCharge", null );
                    asset._startControl( { "percent" : action.params.percent, } );
                    logger.log( format( "%s | run '%s'", asset.name(), asset._ctrlCurrent.mode ) );
                    break;

                case "dffr":

                    /// \note Taken from DynamicAsset.run()
                    asset._nullifyNext();
                    asset._setCurrent( "dynamicFFR", null );
                    asset._startControl( action.params );
                    logger.log( format( "%s | run '%s'", asset.name(), asset._ctrlCurrent.mode ) );
                    break;

                case "default":

                    asset._nullifyNext();
                    asset._setCurrent( "manualPower", null );
                    asset._startControl( { "kW" : 0, } );
                    logger.log( format( "%s | run '%s'", asset.name(), asset._ctrlCurrent.mode ) );
                    break;

                default: return logger.error( "Mode not supported by asset type" );
            }

            _lastAssetAction[assetID] <- action;

            return null;
        }
        
        if( asset instanceof DynamicAsset )
        {
            switch( action.mode )
            {
                case "targetPower":

                    /// \note Taken from DynamicAsset.run()
                    asset._nullifyNext();
                    asset._setCurrent( "manualPower", null );
                    asset._startControl( { "kW" : action.params.kW, } );
                    logger.log( format( "%s | run '%s'", asset.name(), asset._ctrlCurrent.mode ) );
                    break;

                case "dffr":

                    /// \note Taken from DynamicAsset.run()
                    asset._nullifyNext();
                    asset._setCurrent( "dynamicFFR", null );
                    asset._startControl( action.params );
                    logger.log( format( "%s | run '%s'", asset.name(), asset._ctrlCurrent.mode ) );
                    break;

                case "default":

                    asset._nullifyNext();
                    asset._setCurrent( "manualPower", null );
                    asset._startControl( { "kW" : 0, } );
                    logger.log( format( "%s | run '%s'", asset.name(), asset._ctrlCurrent.mode ) );
                    break;

                default: return logger.error( "Mode not supported by asset type" );
            }

            _lastAssetAction[assetID] <- action;

            return null;
        }
        
        return logger.error( "Asset type not supported" );
    }

    /// Fetches the desired asset from the curent fruit config.
    /// \param  assetID the id of the asset to fetch.
    /// \return the desired asset (or 'null' if not found).
    function fetchAsset( assetID )
    {
        local assets = _fruit._current._assets;

        if( assets.len() == 0 )
        {
            return null;
        }

        foreach( asset in assets )
        {
            if( asset.id().tostring() == assetID )
            {
                return asset;
            }
        }

        return null;
    }

    /// Periodically refreshes the scheduled wakeup for the next dispatch.
    /// Helps to reduce millisecond timer drift over long periods.
    function _refreshSchedule()
    {
        if( _refreshScheduleTimer != null )
        {
            imp.cancelwakeup( _refreshScheduleTimer );
        }
        _refreshScheduleTimer = imp.wakeup( _refreshScheduleTime_s, _refreshSchedule.bindenv(this) );

        scheduleNextDispatch();
    }
    
    /// Compares all nested values within two containers for equality.
    /// \param  containerA the first container to be compaired.
    /// \param  containerB the second container to be compaired.
    /// \param  maxDepth the maximum nested depth to compare (default 5).
    /// \param  currentDepth the current nested depth (internal use only).
    /// \return 'true' if all nested value are equal (up to the maxDepth), otherwise 'false'.
    function _compareContainers( containerA, containerB, maxDepth = 5, _currentDepth = 0 )
    {
        if( _currentDepth > maxDepth ) { return true; }
        if( typeof containerA != typeof containerB ) { return false; }

        switch( typeof containerA )
        {
            case "table":

                foreach( key, value in containerA )
                {
                    if( !(key in containerB) ) { return false; }
                    if( _compareContainers( containerA[key], containerB[key], maxDepth, _currentDepth+1 ) == false ) { return false; }
                }

                break;

            case "array":

                if( containerA.len() != containerB.len() ) { return false; }

                for( local i = 0; i < containerA.len(); ++i )
                {
                    if( _compareContainers( containerA[i], containerB[i], maxDepth, _currentDepth+1 ) == false ) { return false; }
                }

                break;

            default:

                if( containerA != containerB ) { return false; }

                break;
        }

        return true;
    }
}

dispatcher <- DispatcherDevice( fruit );
